﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Web.Datacontext;

namespace Web.Tests
{
    [TestClass]
    public class ConfigExtensionsTests
    {
        private Dictionary<TimeSpan, TimeSpan> breaks = new Dictionary<TimeSpan, TimeSpan>();

        [TestInitialize]
        public void Init()
        {
            breaks = new Dictionary<TimeSpan, TimeSpan>();
            breaks.Add(TimeSpan.FromHours(9.25), TimeSpan.FromHours(9.5));
            breaks.Add(TimeSpan.FromHours(11.5), TimeSpan.FromHours(12));
        }

        [TestMethod]
        public void GetDuration_BeforeBreak()
        {   
            TestBreakTimeDeduction(7, 8, 60);
        }

        [TestMethod]
        public void GetDuration_OneBreak()
        {
            TestBreakTimeDeduction(9, 10, 45);
        }

        [TestMethod]
        public void GetDuration_BetweenBreaks()
        {
            TestBreakTimeDeduction(10, 11, 60);
        }

        [TestMethod]
        public void GetDuration_BothBreaks()
        {
            TestBreakTimeDeduction(9, 13, 195);
        }

        [TestMethod]
        public void GetDuration_InBreak()
        {
            TestBreakTimeDeduction(11.75, 12, 0);
        }

        [TestMethod]
        public void GetDuration_HalfBreakBefore()
        {
            TestBreakTimeDeduction(11.25, 11.75, 15);
        }

        [TestMethod]
        public void GetDuration_HalfBreakAfter()
        {
            TestBreakTimeDeduction(11.75, 12.50, 30);
        }

        private void TestBreakTimeDeduction(double fromHour, double toHour, double expecteBreakMinutes)
        {
            var from = DateTime.Now.Date.AddHours(fromHour);
            var to = DateTime.Now.Date.AddHours(toHour);
            var assigment = new Assignment() { StartRun = from };
            var duration = assigment.GetDuration(breaks, to);
            Assert.AreEqual(TimeSpan.FromMinutes(expecteBreakMinutes), duration);
        }
    }
}
