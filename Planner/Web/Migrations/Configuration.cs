﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Web.Datacontext.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Web.Datacontext.ApplicationDbContext";
        }

        protected override void Seed(Web.Datacontext.ApplicationDbContext context)
        {
        }
    }
}
