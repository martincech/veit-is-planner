﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddUserAttendance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "PersonId", c => c.String());
            AddColumn("dbo.Users", "PersonStartDate", c => c.DateTime());
        }

        public override void Down()
        {
            DropColumn("dbo.Users", "PersonStartDate");
            DropColumn("dbo.Users", "PersonId");
        }
    }
}
