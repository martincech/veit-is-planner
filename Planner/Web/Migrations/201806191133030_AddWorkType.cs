﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    using Web.Datacontext.Enums;

    public partial class AddWorkType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assignments", nameof(WorkType), c => c.Int(nullable: false, defaultValue: (int)WorkType.Assembly));
            AddColumn("dbo.Users", nameof(WorkType), c => c.Int(nullable: false, defaultValue: (int)WorkType.Assembly));
            AddColumn("dbo.Templates", nameof(WorkType), c => c.Int(nullable: false, defaultValue: (int)WorkType.Assembly));
        }

        public override void Down()
        {
            DropColumn("dbo.Templates", nameof(WorkType));
            DropColumn("dbo.Users", nameof(WorkType));
            DropColumn("dbo.Assignments", nameof(WorkType));
        }
    }
}
