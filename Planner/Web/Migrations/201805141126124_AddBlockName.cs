﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddBlockName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assignments", "BlockName", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.Assignments", "BlockName");
        }
    }
}
