﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddProjectInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "CloseDate", c => c.DateTime());
            AddColumn("dbo.Projects", "Note", c => c.String());
            AddColumn("dbo.Projects", "RoleId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Projects", "RoleId");
            AddForeignKey("dbo.Projects", "RoleId", "dbo.Roles", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Projects", "RoleId", "dbo.Roles");
            DropIndex("dbo.Projects", new[] { "RoleId" });
            DropColumn("dbo.Projects", "RoleId");
            DropColumn("dbo.Projects", "Note");
            DropColumn("dbo.Projects", "CloseDate");
        }
    }
}
