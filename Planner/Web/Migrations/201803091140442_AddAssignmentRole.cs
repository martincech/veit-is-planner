namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddAssignmentRole : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assignments", "RoleId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Assignments", "RoleId");
            AddForeignKey("dbo.Assignments", "RoleId", "dbo.Roles", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Assignments", "RoleId", "dbo.Roles");
            DropIndex("dbo.Assignments", new[] { "RoleId" });
            DropColumn("dbo.Assignments", "RoleId");
        }
    }
}
