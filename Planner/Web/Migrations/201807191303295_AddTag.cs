﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddTag : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tags",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Code = c.String(nullable: false, maxLength: 10),
                    Created = c.DateTime(nullable: false),
                    Disabled = c.DateTime(),
                    UserId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.Code, unique: true, name: "CodeIndex")
                .Index(t => t.UserId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Tags", "UserId", "dbo.Users");
            DropIndex("dbo.Tags", new[] { "UserId" });
            DropIndex("dbo.Tags", "CodeIndex");
            DropTable("dbo.Tags");
        }
    }
}
