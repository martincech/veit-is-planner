﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddProjectPlanned : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "Planned", c => c.Boolean(nullable: false, defaultValue: false));
        }

        public override void Down()
        {
            DropColumn("dbo.Projects", "Planned");
        }
    }
}
