﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class UpdateWorkTimeTrigger : DbMigration
    {
        public override void Up()
        {
            Sql(@"ALTER TRIGGER dbo.UpdateDuration
                   ON dbo.WorkTimes
                   AFTER UPDATE, DELETE
                AS
                BEGIN
	                Declare @assignmentId int
                    Select @assignmentId = [AssignmentId] from DELETED
	                BEGIN
	                 UPDATE dbo.Assignments SET Duration=(SELECT ISNULL(CAST(SUM(DATEDIFF(SECOND, [From], [To])) AS FLOAT) * 1000,0) FROM dbo.WorkTimes WHERE AssignmentId=@assignmentId)
					  WHERE [Id]=@assignmentId
	                END;
                END
                GO");
        }

        public override void Down()
        {
            Sql(@"ALTER TRIGGER dbo.UpdateDuration
                   ON dbo.WorkTimes
                   AFTER UPDATE
                AS
                BEGIN
	                Declare @assignmentId int
	                Select @assignmentId = [AssignmentId] from inserted
	                BEGIN
	                 UPDATE dbo.Assignments SET Duration=(SELECT ISNULL(CAST(SUM(DATEDIFF(SECOND, [From], [To])) AS FLOAT) * 1000,0) FROM dbo.WorkTimes WHERE AssignmentId=@assignmentId)
					  WHERE [Id]=@assignmentId
	                END;
                END
                GO");
        }
    }
}
