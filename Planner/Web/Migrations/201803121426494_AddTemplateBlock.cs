﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddTemplateBlock : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Blocks",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    Description = c.String(),
                })
                .PrimaryKey(t => t.Id);

            AddColumn("dbo.Templates", "BlockId", c => c.Int());
            CreateIndex("dbo.Templates", "BlockId");
            AddForeignKey("dbo.Templates", "BlockId", "dbo.Blocks", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Templates", "BlockId", "dbo.Blocks");
            DropIndex("dbo.Templates", new[] { "BlockId" });
            DropColumn("dbo.Templates", "BlockId");
            DropTable("dbo.Blocks");
        }
    }
}
