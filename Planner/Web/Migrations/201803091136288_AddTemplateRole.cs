namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddTemplateRole : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Templates", "RoleId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Templates", "RoleId");
            AddForeignKey("dbo.Templates", "RoleId", "dbo.Roles", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.Templates", "RoleId", "dbo.Roles");
            DropIndex("dbo.Templates", new[] { "RoleId" });
            DropColumn("dbo.Templates", "RoleId");
        }
    }
}
