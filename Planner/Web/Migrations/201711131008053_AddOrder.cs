namespace Web.Migrations
{
   using System;
   using System.Data.Entity.Migrations;

   public partial class AddOrder : DbMigration
   {
      public override void Up()
      {
         AddColumn("dbo.Assignments", "Order", c => c.Int(nullable: true));
         Sql("UPDATE [Planner].[dbo].[Assignments] SET [Order] = [Id]");
      }

      public override void Down()
      {
         DropColumn("dbo.Assignments", "Order");
      }
   }
}
