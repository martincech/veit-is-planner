﻿namespace Web.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddProject : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Projects",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.ProjectTemplates",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.TemplateProjectTemplates",
                c => new
                {
                    Template_Id = c.Int(nullable: false),
                    ProjectTemplate_Id = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.Template_Id, t.ProjectTemplate_Id })
                .ForeignKey("dbo.Templates", t => t.Template_Id, cascadeDelete: true)
                .ForeignKey("dbo.ProjectTemplates", t => t.ProjectTemplate_Id, cascadeDelete: true)
                .Index(t => t.Template_Id)
                .Index(t => t.ProjectTemplate_Id);

            AddColumn("dbo.Assignments", "ProjectId", c => c.Int());
            CreateIndex("dbo.Assignments", "ProjectId");
            AddForeignKey("dbo.Assignments", "ProjectId", "dbo.Projects", "Id");
        }

        public override void Down()
        {
            DropForeignKey("dbo.TemplateProjectTemplates", "ProjectTemplate_Id", "dbo.ProjectTemplates");
            DropForeignKey("dbo.TemplateProjectTemplates", "Template_Id", "dbo.Templates");
            DropForeignKey("dbo.Assignments", "ProjectId", "dbo.Projects");
            DropIndex("dbo.TemplateProjectTemplates", new[] { "ProjectTemplate_Id" });
            DropIndex("dbo.TemplateProjectTemplates", new[] { "Template_Id" });
            DropIndex("dbo.Assignments", new[] { "ProjectId" });
            DropColumn("dbo.Assignments", "ProjectId");
            DropTable("dbo.TemplateProjectTemplates");
            DropTable("dbo.ProjectTemplates");
            DropTable("dbo.Projects");
        }
    }
}
