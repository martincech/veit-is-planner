﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddProjectWorkers : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "WorkersEl", c => c.Int(nullable: false));
            AddColumn("dbo.Projects", "WorkersAs", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.Projects", "WorkersAs");
            DropColumn("dbo.Projects", "WorkersEl");
        }
    }
}
