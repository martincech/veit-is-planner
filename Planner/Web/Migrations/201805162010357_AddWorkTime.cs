﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddWorkTime : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.WorkTimes",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    AssignmentId = c.Int(nullable: false),
                    UserId = c.String(maxLength: 128),
                    From = c.DateTime(nullable: false),
                    To = c.DateTime(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Assignments", t => t.AssignmentId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.AssignmentId)
                .Index(t => t.UserId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.WorkTimes", "UserId", "dbo.Users");
            DropForeignKey("dbo.WorkTimes", "AssignmentId", "dbo.Assignments");
            DropIndex("dbo.WorkTimes", new[] { "UserId" });
            DropIndex("dbo.WorkTimes", new[] { "AssignmentId" });
            DropTable("dbo.WorkTimes");
        }
    }
}
