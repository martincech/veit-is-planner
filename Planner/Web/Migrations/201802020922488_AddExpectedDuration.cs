namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddExpectedDuration : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assignments", "ExpectedDuration", c => c.Double(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.Assignments", "ExpectedDuration");
        }
    }
}
