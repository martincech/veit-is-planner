﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assignments", "Code", c => c.String());
            AddColumn("dbo.Templates", "Code", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.Templates", "Code");
            DropColumn("dbo.Assignments", "Code");
        }
    }
}
