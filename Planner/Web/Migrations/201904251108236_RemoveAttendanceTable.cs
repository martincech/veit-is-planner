﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class RemoveAttendanceTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Attendances", "UserId", "dbo.Users");
            DropIndex("dbo.Attendances", new[] { "UserId" });
            DropTable("dbo.Attendances");
        }

        public override void Down()
        {
            CreateTable(
                "dbo.Attendances",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Year = c.Int(nullable: false),
                    Month = c.Int(nullable: false),
                    Hours = c.Double(nullable: false),
                    UserId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => t.Id);

            CreateIndex("dbo.Attendances", "UserId");
            AddForeignKey("dbo.Attendances", "UserId", "dbo.Users", "Id", cascadeDelete: true);
        }
    }
}
