// <auto-generated />
namespace Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class AddProjectPlanned : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddProjectPlanned));
        
        string IMigrationMetadata.Id
        {
            get { return "201805282006588_AddProjectPlanned"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
