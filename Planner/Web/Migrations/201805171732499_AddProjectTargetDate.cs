﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddProjectTargetDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Projects", "TargetDate", c => c.DateTime());
            AddColumn("dbo.Projects", "View", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            DropColumn("dbo.Projects", "View");
            DropColumn("dbo.Projects", "TargetDate");
        }
    }
}
