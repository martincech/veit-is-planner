namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddAssignmentName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Assignments", "Name", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.Assignments", "Name");
        }
    }
}
