﻿namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddAttendance : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Attendances",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Year = c.Int(nullable: false),
                    Month = c.Int(nullable: false),
                    Hours = c.Double(nullable: false),
                    UserId = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Attendances", "UserId", "dbo.Users");
            DropIndex("dbo.Attendances", new[] { "UserId" });
            DropTable("dbo.Attendances");
        }
    }
}
