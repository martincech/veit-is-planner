﻿using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using FluentScheduler;
using Web.Datacontext;

namespace Web
{
    public class MvcApplication : HttpApplication
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            logger.Info("Starting APP");
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ApplicationDbContext.CreateAndSeedIfNotExist();

            if (AppSettings.AssignmentAutoPause)
            {
                logger.Info($"Assignemnt auto-stop is ENABLED.");
                JobManager.AddJob(AssigmentAutoPause.Run, (s) => s.ToRunEvery(60).Seconds());
            }
            else
            {
                logger.Info($"Assignemnt auto-stop is DISABLED.");
            }
        }


        protected void Application_Error()
        {
            var lastException = Server.GetLastError();
            logger.Error(lastException);
        }
    }
}
