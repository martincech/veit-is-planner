﻿using System;

namespace Web.Extensions
{
    public static class DateTimeExtensions
    {
        public static (DateTime from, DateTime to) GetMonthDuration(this DateTime baseMonth, int months, int lenght = 1)
        {
            var targetMonth = new DateTime(baseMonth.Year, baseMonth.Month, 1).AddMonths(-months);
            return (targetMonth, targetMonth.AddMonths(lenght));
        }
    }
}
