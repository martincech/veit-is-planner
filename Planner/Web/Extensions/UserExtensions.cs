﻿using System.Security.Principal;
using Web.Datacontext;

namespace Web.Extensions
{
    public static class UserExtensions
    {
        public static bool IsAdmin(this IPrincipal principal)
        {
            return principal.IsInRole(ApplicationDbContext.ADMIN);
        }
    }
}
