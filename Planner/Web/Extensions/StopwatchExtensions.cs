﻿using System.Diagnostics;

namespace Web.Extensions
{
    public static class StopwatchExtensions
    {
        public static void LogAndReset(this Stopwatch stopwatch, string operationName)
        {
            Debug.WriteLine($"Operation {operationName} took {stopwatch.ElapsedMilliseconds} ms.");
            stopwatch.Restart();
        }
    }
}
