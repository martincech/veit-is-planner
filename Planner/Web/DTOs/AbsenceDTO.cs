﻿using System;

namespace Web.DTOs
{
    public class AbsenceDTO
    {
        public DateTime Date { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public double Hours { get; set; }
        public short Type { get; set; }
    }
}
