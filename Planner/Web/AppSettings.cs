﻿using System.Configuration;
using NLog;

namespace Web
{
    public static class AppSettings
    {
        public const string VERSION_KEY = "app.version";
        public const string AUTO_PAUSE_KEY = "app.assignmentAutoPause";

        static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public static string JSVersion(this string jsFilePath)
        {
            return string.Format("{0}?v={1}", jsFilePath, ConfigurationManager.AppSettings[VERSION_KEY] ?? "1");
        }

        public static bool AssignmentAutoPause
        {
            get
            {
                bool.TryParse(ConfigurationManager.AppSettings[AUTO_PAUSE_KEY], out var result);
                return result;
            }
        }
    }
}
