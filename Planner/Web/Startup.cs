﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Web.Startup))]
namespace Web
{
    public partial class Startup
    {
#pragma warning disable CC0091 // Use static method
        public void Configuration(IAppBuilder app)
#pragma warning restore CC0091 // Use static method
        {
            ConfigureAuth(app);
        }
    }
}
