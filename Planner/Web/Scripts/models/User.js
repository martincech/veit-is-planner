﻿app.controller("userCtrl", ["$scope", "$filter", "orderByFilter", "api", "utils", "$document", "rfid", function ($scope, $filter, orderByFilter, api, utils, $document, rfid) {

    $scope.user = {};
    $scope.users = [];
    $scope.attendUsers = [];
    $scope.edit = null;
    $scope.search = { done: false };

    $scope.create = function () {
        api.createUser($scope.user).then(function (user) {
            $scope.user = {};
            if (user == null) return;
            $scope.users.push(user);
        });
    };

    $scope.createDisabled = function () {
        if ($scope.user == null || $scope.user.UserName == null || $scope.user.UserName.length < 4) return true;
        if ($scope.users.filter(function (u) { return u.UserName === $scope.user.UserName; }).length > 0) return true;
        return false;
    };

    $scope.updateDisabled = function () {
        if ($scope.user == null || $scope.user.UserName == null || $scope.user.UserName.length < 1) return true;
        if ($scope.users.filter(function (u) { return u.Id !== $scope.user.Id && u.UserName === $scope.user.UserName; }).length > 0) return true;
        return false;
    };

    $scope.update = function () {
        api.updateUser($scope.user).then(function (user) {
            $scope.user = {};
            $scope.edited.IsAdmin = user.IsAdmin;
            $scope.edited.UserName = user.UserName;
            $scope.edited.GroupId = user.GroupId;
            $scope.edited.WorkType = user.WorkType;
            $scope.edited = null;
        });
    };

    $scope.edit = function (user) {
        $scope.user = angular.copy(user);
        $scope.edited = user;
        $scope.user.attendName = user.UserName;
        $scope.search.done = false;
        $scope.attendUsers = [];
    };

    $scope.cancelEdit = function () {
        $scope.user = {};
        $scope.edited = null;
    };

    $scope.findUser = function () {
        api.findUser($scope.user.attendName).then(function (result) {
            $scope.attendUsers = result;
            $scope.search.done = true;
        });
    };

    $scope.mapUser = function (attendUser) {
        api.updateAttendMap($scope.user.Id, attendUser).then(function () {
            var usr = $scope.users.find(function (item) { return item.Id === $scope.user.Id; });
            if (usr != null) usr.Attend = attendUser;
            $scope.user.Attend = attendUser;
            $scope.search.done = false;
        });
    };

    $scope.reset = function (userId) {
        api.resetPassword(userId).then(function (result) {
            alert("Heslo se " + (result === true ? "podařilo" : "nepodařilo") + " resetovat");
        });
    };

    $scope.delete = function (userId) {
        api.deleteUser(userId).then(function () {
            $scope.users = $scope.users.filter(function (item) { return item.Id !== userId; });
        });
    };

    $scope.disable = function (user, disable) {
        api.disableUser(user.Id, disable).then(function (usr) {
            user.Disabled = usr.Disabled;
        });
    };

    $scope.getGroupName = function (groupId) {
        if (groupId == null) return "";
        return ($scope.groups.filter(function (item) { return item.Id === groupId; })[0] || {}).Name;
    };

    api.getUsers().then(function (data) {
        $scope.users = data.Users;
        $scope.groups = data.Groups;
        $scope.tags = data.Tags;
    });




    $scope.tag = "";
    $scope.tagDone = false;
    $scope.readTag = false;

    $scope.tagExist = function () {
        return $scope.tags.find(function (t) { return t.Code === $scope.tag; }) != null;
    };

    $scope.isValidTag = function () {
        if ($scope.tag.length !== 10 || isNaN($scope.tag)) return false;
        return !$scope.tagExist();
    };

    $scope.setupTag = function (read) {
        $scope.readTag = read;
        $scope.tagDone = !read;
        $scope.tag = "";
    };


    $scope.addTagToUser = function () {
        api.createTag($scope.tag, $scope.user.Id).then(function (tag) {
            if (tag == null || tag.Id == null) {
                alert("Chyba na severu. Kontaktuje spravce.");
                return;
            }
            $scope.tags
                .filter(function (t) { return t.UserId === tag.UserId && t.Disabled == null })
                .forEach(function (t) { t.Disabled = new Date().toISOString(); });
            $scope.tags.push(tag);
            $scope.readTag = false;
        });
    };

    $scope.enableTag = function (tg, enable) {
        if ((enable && tg.Disabled == null) || (!enable && tg.Disabled != null)) return;
        api.updateTag(tg.Code, enable).then(function (tag) {
            if (tag == null || tag.Id == null) {
                alert("Chyba na severu. Kontaktuje spravce.");
                return;
            }
            tg.Disabled = tag.Disabled;
            if (!enable) return;
            $scope.tags
                .filter(function (t) { return t.UserId === tag.UserId && t.Disabled == null && t.Id !== tg.Id; })
                .forEach(function (t) { t.Disabled = new Date().toISOString(); });
        });
    };

    $document.bind('keypress', function (e) {
        if (!$scope.readTag || $scope.tagDone) return;

        if (e.keyCode === 13) {
            $scope.tagDone = true;
            $scope.$apply();
            return;
        }
        $scope.tag = rfid.buildCode($scope.tag, e.key);
    });

}]);