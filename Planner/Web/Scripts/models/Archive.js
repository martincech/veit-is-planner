﻿app.controller("archiveCtrl", ["$scope", "$filter", "orderByFilter", "api", "utils", function ($scope, $filter, orderByFilter, api, utils) {

    $scope.property = "Stop";
    $scope.reverse = true;

    $scope.pagination = {
        pages: function () { return Math.floor($scope.assignments.length / $scope.pagination.pageSize); },
        pageSize: 50,
        current: 0,
        from: function () {
            return $scope.pagination.pageSize * $scope.pagination.current;
        },

        to: function () {
            return $scope.pagination.pageSize * ($scope.pagination.current + 1);
        },
        goTo: function (page) {
            if (page < 0 || page > $scope.pagination.pages()) return;
            $scope.pagination.current = page;
        }

    };

    $scope.assignments = [];
    $scope.groups = [];
    $scope.users = [];

    $scope.strict = true;

    $scope.assignmentTime = utils.duration;

    $scope.orderBy = function (property) {
        if ($scope.property === property) {
            $scope.reverse = !$scope.reverse;
            return;
        }

        $scope.reverse = false;
        $scope.property = property;
    };

    $scope.getExpectedDuration = function (duration) {
        duration = duration || 0;
        return Math.floor(duration / (1000 * 60 * 60)) + " hodin " + Math.floor(duration % (1000 * 60 * 60) / (1000 * 60)) + " minut";
    };

    $scope.download = function () {
        var assignments = $filter("filterStrict")($scope.assignments, $scope.search, $scope.strict);
        assignments = orderByFilter(assignments, $scope.property, $scope.reverse);
        utils.donwload(assignments);
    };

    $scope.delete = function (assign) {
        if (assign == null) return;
        api.delete(assign).then(function () {
            $scope.assignments = $scope.assignments.filter(function (item) {
                return item.Id !== assign.Id;
            });
        });
    };

    $scope.getClass = function (assignment) {
        if (assignment == null || assignment.ExpectedDuration === 0) return "";
        return assignment.Duration > assignment.ExpectedDuration ? "back-red" : "back-green";
    };

    function getNameDic(items, propName) {
        var dic = {};
        items.forEach(function (item) {
            dic[item.Id] = item[propName];
        });
        return dic;
    }

    api.getArchiveData().then(function (data) {
        $scope.assignments = data.Assignments;
        $scope.groups = data.Groups;
        $scope.users = data.Users;
        $scope.projects = data.Projects;
        $scope.dic = {};
        $scope.dic.users = getNameDic($scope.users, 'UserName');
        $scope.dic.groups = getNameDic($scope.groups, 'Name');
        $scope.dic.projects = getNameDic($scope.projects, 'Name');
        $scope.assignments.forEach(function (i) {
            i.user = $scope.dic.users[i.UserId];
            i.role = $scope.dic.groups[i.RoleId];
            i.project = $scope.dic.projects[i.ProjectId];
        });
    });
}])
    .filter('filterStrict', ["$filter", function ($filter) {
        return function (items, filterStrict, enabled) {
            var filtered = [];
            if (filterStrict == null || filterStrict === "") return items;
            if (!enabled) return $filter("filter")(items, filterStrict);
            angular.forEach(items, function (item) {
                if (item == null) return;
                if (item.user != null && item.user.indexOf(filterStrict) !== -1) {
                    filtered.push(item);
                    return;
                }
                if (item.role != null && item.role.indexOf(filterStrict) !== -1) {
                    filtered.push(item);
                    return;
                }
                if (item.project != null && item.project.indexOf(filterStrict) !== -1) {
                    filtered.push(item);
                    return;
                }
                if (item.BlockName != null && item.BlockName.indexOf(filterStrict) !== -1) {
                    filtered.push(item);
                    return;
                }
                if (item.Description != null && item.Description.indexOf(filterStrict) !== -1) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }]);