﻿app.controller("tasksCtrl", ["$scope", "$interval", "api", "utils", "attend", function ($scope, $interval, api, utils, attend) {
    utils.isLoading(true);
    $scope.filter = {};
    $scope.showLog = false;
    $scope.userId = null;
    $scope.all = false;
    $scope.users = [];
    $scope.assignment = null;
    $scope.assignments = [];
    $scope.isWorking = true;
    $scope.hasAttendance = false;

    $scope.month = utils.getMonthName(new Date().getMonth());

    $scope.assignmentTime = utils.duration;

    $scope.assignmentsCount = function () {
        return utils.count($scope.assignments);
    };

    function calcTaskTimes(assignment) {
        if (assignment == null) return;

        var duration = assignment.Duration || 0;
        if (assignment.StartRun != null) {
            duration = duration + (new Date().getTime() - new Date(assignment.StartRun).getTime());
        }
        assignment.time = {
            expected: utils.getTime(assignment.ExpectedDuration),
            finished: utils.getTime(duration),
            plan: utils.getTime(duration - assignment.ExpectedDuration)
        };
    }

    $scope.getStateColor = function (duration) {
        if (duration === 0) return '';
        return duration > 0 ? 'back-red' : 'back-green';
    };

    function start(assignment) {
        utils.isLoading(true);
        assignment.UserId = $scope.userId;
        api.start(assignment).then(function (assign) {
            // utils.update(assignment, assign);
            // $scope.assignment = assignment;
            update();
        });
        $scope.modal = null;
    }

    $scope.start = function (assignment) {
        if (assignment.UserId == null || assignment.UserId === $scope.userId) {
            start(assignment);
            return;
        }

        $scope.modal = {
            title: "Převzít úkol?",
            text: "Úkol má přiřazen " + $scope.users[assignment.UserId] + " chcete jej převzít?",
            action: function () {
                start(assignment);
            },
            cancel: function () {
                $scope.modal = null;
            }
        };
    };

    $scope.stop = function (assignment) {
        utils.isLoading(true);
        api.stop(assignment || $scope.assignment).then(function (assign) {
            utils.update(assignment || $scope.assignment, assign);
            update();
        });
    };

    $scope.pause = function (assignment) {
        utils.isLoading(true);
        api.pause(assignment || $scope.assignment).then(function (assign) {
            utils.update(assignment || $scope.assignment, assign);
            $scope.assignment = null;
            update();
        });
    };

    $scope.canRun = function () {
        return $scope.assignments.filter(function (a) { return a.StartRun != null && a.UserId === $scope.userId; })[0] == null;
    };

    $scope.getExpectedDuration = function (duration) {
        duration = duration || 0;
        return Math.floor(duration / (1000 * 60 * 60)) + " hodin " + Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60)) + " minut";
    };

    $scope.getDuration = function () {
        duration = $scope.assignment.Duration || 0;
        if ($scope.assignment.StartRun != null) {
            duration = duration + (new Date().getTime() - new Date($scope.assignment.StartRun).getTime());
        }
        return Math.floor(duration / (1000 * 60 * 60)) + " hodin " + Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60)) + " minut";
    };


    $scope.getAssignmentClass = function (assign) {
        if (assign.StartRun == null && assign.Stop == null && assign.Start == null) return 'warning';
        if (assign.StartRun != null && assign.Stop == null) return 'success';
        if (assign.StartRun == null && assign.Stop == null && assign.Start != null) return 'danger';
    };

    $scope.getProjectName = function (projectId) {
        return ($scope.projects.filter(function (pr) { return pr.Id === projectId; })[0] || {}).Name;
    };

    $scope.loadProjectData = function (projectId) {
        if (projectId === $scope.filter.projectId) {
            $scope.filter.projectId = null;
            return;
        }
        api.getAssignments(projectId, true).then(function (data) {
            $scope.projectAssignments = data;
            $scope.filter.projectId = projectId;
            $scope.projectAssignments.forEach(calcTaskTimes);
        });
    };

    $scope.orderByState = function (assignment) {
        if (assignment.Stop != null) return 3;
        if (assignment.StartRun != null) return 0;
        if (assignment.Start != null) return 1;
        return 2;
    };

    $scope.getStyle = function (assignment) {
        if (assignment.time == null || assignment.time.expected.duration === 0 || assignment.Start == null) return '';
        return assignment.time.plan.duration > 0 ? 'background-color:salmon' : 'background-color:darkseagreen';
    };

    $scope.getEntry = function () {
        return attend.entryDate($scope.logs);
    };

    $scope.getLeave = function () {
        return attend.leaveDate($scope.logs);
    };

    $scope.formatType = attend.logTypeText;


    function update() {
        api.getMyData().then(function (data) {
            $scope.projectsTasks = data.ProjectsTasks;
            $scope.assignments = data.Assignments;
            $scope.users = data.Users;
            $scope.projects = data.Projects;
            $scope.times = data.Times;
            $scope.logs = attend.reduceLog(data.Logs);
            $scope.hasAttendance = data.PersonId != null; // data.Logs.length !== 0;
            $scope.isWorking = data.Logs.length === 0 ? !$scope.hasAttendance : data.Logs[data.Logs.length - 1].Type === '01';           
            $scope.assignments.forEach(calcTaskTimes);
            $scope.assignment = $scope.assignments.filter(function (item) { return item.StartRun != null && item.Stop == null && item.UserId === $scope.userId; })[0];
            if ($scope.assignment != null) $scope.project = data.Projects.filter(function (p) { return p.Id === $scope.assignment.ProjectId; })[0];
            if ($scope.times.length > 0) {
                var times = [];
                for (var i = 0; i < $scope.times.length; i++) {

                    var time = {};
                    time.plan = utils.getTime($scope.times[i].Plan);
                    time.state = { finished: $scope.times[i].Done, total: $scope.times[i].Count };
                    times.push(time);
                }
                $scope.projectTimes = times;
            }
            utils.isLoading(false);
        });
    }
    $interval(update, 60000);
    update();
}]);