﻿var app = angular.module('app', ['ngCookies'])
    .factory("api", ["$http", function ($http) {

        function filter(response) {
            return response.data;
        }

        return {

            //assignment
            getData: function getPlannerData() {
                return $http({ method: 'GET', url: '/assignment/get' }).then(filter);
            },
            getMyData: function getPlannerData() {
                return $http({ method: 'GET', url: '/assignment/getbyuser' }).then(filter);
            },
            getAssignments: function (projectId, noArchive) {
                return $http({ method: 'GET', url: '/assignment/getAssignments?projectId=' + projectId + '&noArchive=' + noArchive || false }).then(filter);
            },
            create: function (assignment) {
                return $http({ method: 'POST', url: '/assignment/create', data: assignment }).then(filter);
            },
            createMultiple: function (assignments) {
                return $http({ method: 'POST', url: '/assignment/createmultiple', data: assignments }).then(filter);
            },
            start: function (assignment) {
                return $http({ method: 'POST', url: '/assignment/start', data: assignment }).then(filter);
            },
            update: function (assignment) {
                return $http({ method: 'POST', url: '/assignment/update', data: assignment }).then(filter);
            },
            pause: function (assignment) {
                return $http({ method: 'POST', url: '/assignment/pause', data: assignment }).then(filter);
            },
            stop: function (assignment) {
                return $http({ method: 'POST', url: '/assignment/stop', data: assignment }).then(filter);
            },
            swap: function (id, id2) {
                return $http({ method: 'POST', url: '/assignment/swap', data: { id: id, id2: id2 } }).then(filter);
            },
            archive: function () {
                return $http({ method: 'POST', url: '/assignment/archive' }).then(filter);
            },
            archiveByProject: function (projectId) {
                return $http({ method: 'POST', url: '/assignment/archiveByProject', data: { projectId: projectId } }).then(filter);
            },
            delete: function (assignment) {
                return $http({ method: 'POST', url: '/assignment/delete', data: assignment }).then(filter);
            },
            //archive
            getArchiveData: function getPlannerData() {
                return $http({ method: 'GET', url: '/archive/get' }).then(filter);
            },
            //user
            getUsers: function () {
                return $http({ method: 'GET', url: '/user/get' }).then(filter);
            },
            createUser: function (user) {
                if (user.IsAdmin == null) user.IsAdmin = false;
                return $http({ method: 'POST', url: '/user/create', data: user }).then(filter);
            },
            deleteUser: function (userId) {
                return $http({ method: 'POST', url: '/user/delete', data: { userId: userId } }).then(filter);
            },
            disableUser: function (userId, disable) {
                return $http({ method: 'POST', url: '/user/disable', data: { userId: userId, disable: disable } }).then(filter);
            },
            resetPassword: function (userId) {
                return $http({ method: 'POST', url: '/user/reset', data: { userId: userId } }).then(filter);
            },
            updateUser: function (user) {
                if (user.IsAdmin == null) user.IsAdmin = false;
                return $http({ method: 'POST', url: '/user/update', data: user }).then(filter);
            },
            //group
            getGroups: function () {
                return $http({ method: 'GET', url: '/group/get' }).then(filter);
            },
            createGroup: function (name) {
                return $http({ method: 'POST', url: '/group/create', data: { name: name } }).then(filter);
            },
            deleteGroup: function (groupId) {
                return $http({ method: 'POST', url: '/group/delete', data: { groupId: groupId } }).then(filter);
            },
            updateGroup: function (group) {
                return $http({ method: 'POST', url: '/group/update', data: group }).then(filter);
            },
            //template
            getTemplates: function () {
                return $http({ method: 'GET', url: '/template/get' }).then(filter);
            },
            createTemplate: function (template) {
                return $http({ method: 'POST', url: '/template/create', data: template }).then(filter);
            },
            updateTemplate: function (template) {
                return $http({ method: 'POST', url: '/template/update', data: template }).then(filter);
            },
            deleteTemplate: function (id) {
                return $http({ method: 'POST', url: '/template/delete', data: { id: id } }).then(filter);
            },
            //block
            createBlock: function (block) {
                return $http({ method: 'POST', url: '/block/create', data: block }).then(filter);
            },
            updateBlock: function (block) {
                return $http({ method: 'POST', url: '/block/update', data: block }).then(filter);
            },
            deleteBlock: function (id) {
                return $http({ method: 'POST', url: '/block/delete', data: { id: id } }).then(filter);
            },
            //projectTemplates
            createProjectTemplate: function (pTemplate) {
                return $http({ method: 'POST', url: '/projecttemplate/create', data: pTemplate }).then(filter);
            },
            updateProjectTemplate: function (pTemplate) {
                return $http({ method: 'POST', url: '/projecttemplate/update', data: pTemplate }).then(filter);
            },
            deleteProjectTemplate: function (id) {
                return $http({ method: 'POST', url: '/projecttemplate/delete', data: { id: id } }).then(filter);
            },
            addToTemplateProject: function (projectTemplateId, templateId) {
                return $http({ method: 'POST', url: '/projecttemplate/addtemplate', data: { projectTemplateId: projectTemplateId, templateId: templateId } }).then(filter);
            },
            removeFromTemplateProject: function (projectTemplateId, templateId) {
                return $http({ method: 'POST', url: '/projecttemplate/removetemplate', data: { projectTemplateId: projectTemplateId, templateId: templateId } }).then(filter);
            },
            //project          
            getProjects: function () {
                return $http({ method: 'GET', url: '/project/get' }).then(filter);
            },
            createProject: function (project) {
                return $http({ method: 'POST', url: '/project/create', data: project }).then(filter);
            },
            updateProject: function (project) {
                return $http({ method: 'POST', url: '/project/update', data: project }).then(filter);
            },
            deleteProject: function (id) {
                return $http({ method: 'POST', url: '/project/delete', data: { id: id } }).then(filter);
            },
            findProject: function (projectName) {
                return $http({ method: 'POST', url: '/project/find', data: { projectName: projectName } }).then(filter);
            },
            addToProject: function (projectId, assignmentIds) {
                return $http({ method: 'POST', url: '/project/addtoproject', data: { projectId: projectId, assignmentIds: assignmentIds } }).then(filter);
            },
            openProject: function (projectId) {
                return $http({ method: 'POST', url: '/project/open', data: { projectId: projectId } }).then(filter);
            },
            closeProject: function (projectId) {
                return $http({ method: 'POST', url: '/project/close', data: { projectId: projectId } }).then(filter);
            },
            projectChangeView: function (projectId, view, months) {
                if (months == null || isNaN(months)) months = 0;
                return $http({ method: 'POST', url: '/project/changeview', data: { projectId: projectId, view: view, months: months } }).then(filter);
            },
            projectChangeWorkers: function (projectId, workType, workers) {
                return $http({ method: 'POST', url: '/project/changeworkers', data: { projectId: projectId, workType: workType, workers: workers } }).then(filter);
            },
            projectChangePlanned: function (projectId, planned) {
                return $http({ method: 'POST', url: '/project/changeplanned', data: { projectId: projectId, planned: planned } }).then(filter);
            },
            //tag          
            createTag: function (code, userId) {
                return $http({ method: 'POST', url: '/tag/create', data: { code: code, userId: userId } }).then(filter);
            },
            updateTag: function (code, enabled) {
                return $http({ method: 'POST', url: '/tag/update', data: { code: code, enabled: enabled } }).then(filter);
            },
            //mapping user attendance
            findUser: function (surename) {
                return $http({ method: 'POST', url: '/user/findbyname', data: { surename: surename } }).then(filter);
            },
            updateAttendMap: function (userId, attendUser) {
                return $http({ method: 'POST', url: '/user/UpdateAttendMap', data: { userId: userId, attendUser: attendUser } }).then(filter);
            }


        };
    }])
    .factory("utils", ["$filter", function ($filter) {

        //work type enum
        var workType = {
            others: 0,
            assembly: 1,
            electricity: 2
        };
        var monthsName = ["leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"];

        function getMonthName(month) {
            return monthsName[month];
        }

        function addColumn(value) {
            return value + ";";
        }

        function formatDate(date) {
            return $filter('date')(date, "dd.MM.yyyy HH:mm");
        }

        function download(content, fileName, mimeType) {
            var a = document.createElement('a');
            mimeType = mimeType || 'application/octet-stream';

            if (navigator.msSaveBlob) { // IE10
                navigator.msSaveBlob(new Blob([content], {
                    type: mimeType
                }), fileName);
            } else if (URL && 'download' in a) { //html5 A[download]
                a.href = URL.createObjectURL(new Blob([content], {
                    type: mimeType
                }));
                a.setAttribute('download', fileName);
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
            } else {
                location.href = 'data:application/octet-stream,' + encodeURIComponent(content); // only this mime type is supported
            }
        }

        function hours(duration) {
            var diffHours = duration / (1000 * 60 * 60);
            return diffHours.toFixed(3);
        }

        function actualDuration(assignment) {
            if (assignment == null || assignment.Start == null) return null;
            var diff = -assignment.ExpectedDuration + assignment.Duration;
            if (assignment.StartRun != null) diff = diff + moment(new Date()).diff(moment(assignment.StartRun));
            return diff;
        }

        function durationTime(duration) {
            duration = duration || 0;
            return Math.floor(duration / (1000 * 60 * 60)) + "h " + Math.floor(duration % (1000 * 60 * 60) / (1000 * 60)) + "min";
        }

        function sumTemplateTime(templates) {
            var duration = 0;
            if (templates != null) templates.forEach(function (item) { duration += item.Duration; });
            return durationTime(duration);
        }

        var timeTemplate = {
            plan: { hours: 0, minutes: 0, duration: 0 },
            finished: { hours: 0, minutes: 0, duration: 0 },
            total: { hours: 0, minutes: 0, duration: 0 },
            planned: { hours: 0, minutes: 0, duration: 0 },
            state: { finished: 0, total: 0 }
        };

        function getTime(duration) {
            var time = { hours: 0, minutes: 0, duration: 0 };
            if (duration == null || duration === 0) return time;
            time.hours = Math.floor(Math.abs(duration) / (1000 * 60 * 60));
            if (duration < 0) time.hours = -time.hours;
            time.minutes = Math.floor(Math.abs(duration) % (1000 * 60 * 60) / (1000 * 60));
            time.duration = duration;
            return time;
        }

        function getProjectTimes(assignments) {
            var time = angular.copy(timeTemplate);
            if (assignments == null || assignments.length === 0) return time;
            var plan = 0;
            var planned = 0;
            var finished = 0;
            var total = 0;
            var done = 0;

            assignments.forEach(function (it) {
                if (it.Stop != null) {
                    if (it.ExpectedDuration > 0) {
                        plan += it.Duration - it.ExpectedDuration;
                    }
                    total += it.Duration;
                    finished += it.Duration;
                    done += 1;
                } else {
                    var running = 0;
                    if (it.StartRun != null) {
                        running = new Date().getTime() - new Date(it.StartRun).getTime();
                    }

                    if (it.ExpectedDuration > 0) {
                        var totalDuration = it.Duration + running;
                        if (totalDuration > it.ExpectedDuration) {
                            plan += totalDuration - it.ExpectedDuration;
                        }
                    }

                    total += Math.max(it.Duration + running, it.ExpectedDuration);
                    finished += it.Duration + running;
                }
                planned += it.ExpectedDuration;
            });

            time.plan = getTime(plan);
            time.planned = getTime(planned);
            time.finished = getTime(finished);
            time.total = getTime(total);
            time.state = { finished: done, total: assignments.length };
            return time;
        }

        function getProjectTimesByType(assignments) {
            return [
                {},
                getProjectTimes(assignments.filter(function (a) { return a.WorkType === workType.assembly; })),
                getProjectTimes(assignments.filter(function (a) { return a.WorkType === workType.electricity; })),
                getProjectTimes(assignments)
            ];
        }

        function update(original, modified) {
            if (original == null || modified == null) return;
            original.Start = modified.Start;
            original.Duration = modified.Duration;
            original.ExpectedDuration = modified.ExpectedDuration;
            original.StartRun = modified.StartRun;
            original.Stop = modified.Stop;
            original.Paused = modified.Paused;
            original.Description = modified.Description;
            original.Code = modified.Code;
            original.UserId = modified.UserId;
            original.WorkType = modified.WorkType;
        }

        function fullUpdate(original, modified) {
            if (original == null || modified == null) return;
            update(original, modified);
            original.Archived = modified.Archived;
            original.Name = modified.Name;
            original.BlockName = modified.BlockName;
            original.ProjectId = modified.ProjectId;
            original.RoleId = modified.RoleId;
        }

        function isLoading(loading) {
            document.body.classList.toggle('loading', loading === true);
        }

        function workTimesDuration(workTimes) {
            if (workTimes == null || workTimes.length === 0) return 0;
            var duration = 0;
            const now = new Date();
            workTimes.forEach(function (workTime) {
                const from = new Date(workTime.From);
                if (workTime.To == null) {
                    duration += now - from;
                } else {
                    const to = new Date(workTime.To);
                    duration += to - from;
                }
            });
            return getTime(duration);
        }

        return {
            workType: workType,
            update: update,
            fullUpdate: fullUpdate,
            duration: function (assignment) {
                var diff = actualDuration(assignment);
                if (diff == null) return null;
                var diffHours = diff / (1000 * 60 * 60);
                var prefix = diffHours < 0 ? "-" : "+";
                diffHours = Math.abs(diffHours);
                return prefix + " " + Math.floor(diffHours) + "h " + Math.floor(diffHours % 1 * 60) + "min";
            },
            count: function (assignments) {
                if (assignments == null) return "0/0";
                var finished = assignments.filter(function (item) { return item.Stop != null; });
                return finished.length + "/" + assignments.length;
            },
            donwload: function (assignments) {
                if (assignments == null || assignments.length === 0) return;
                var csvContent = "\ufeffUzivatel;Skupina;Projekt;Blok ukolu;Abra Kod;Nazev ukolu;Popis;Vytvoreno;Zacatek;Konec;Planovany Cas;Odpracovany Cas;Celkovy Cas\n";
                for (var i = 0; i < assignments.length; i++) {
                    csvContent += addColumn(assignments[i].user);
                    csvContent += addColumn(assignments[i].role || '');
                    csvContent += addColumn(assignments[i].project || '');
                    csvContent += addColumn(assignments[i].BlockName || '');
                    csvContent += addColumn(assignments[i].Code || '');
                    csvContent += addColumn((assignments[i].Name || "").replace(/(\r\n|\n|\r)/gm, " "));
                    csvContent += addColumn((assignments[i].Description || "").replace(/(\r\n|\n|\r)/gm, " "));
                    csvContent += addColumn(formatDate(assignments[i].Created));
                    csvContent += addColumn(formatDate(assignments[i].Start));
                    csvContent += addColumn(formatDate(assignments[i].Stop));
                    csvContent += addColumn(hours(assignments[i].ExpectedDuration));
                    csvContent += addColumn(hours(assignments[i].Duration));
                    csvContent += addColumn(hours(assignments[i].Duration - assignments[i].ExpectedDuration));
                    csvContent += "\n";
                }
                download(csvContent, 'ukoly.csv', 'text/csv;encoding:utf-8');
            },
            actualDuration: actualDuration,
            durationTime: durationTime,
            sumTemplateTime: sumTemplateTime,
            getProjectTimes: getProjectTimes,
            getProjectTimesByType: getProjectTimesByType,
            getTime: getTime,
            getMonthName: getMonthName,
            isLoading: isLoading,
            workTimesDuration: workTimesDuration
        };
    }])
    .factory("rfid", ["$cookies", function ($cookies) {

        var specialChars = "+ěščřžýáíé+ĚŠČŘŽÝÁÍÉ";
        var numChars = "12345678901234567890";

        var rfid = {
            mapToNumber: mapToNumber,
            buildCode: buildCode,
            isMobile: isMobile,
            isLoginViewLocked: isLoginViewLocked,
            useRFIDLogin: useRFIDLogin,
            setLoginView: setLoginView
        };

        function mapToNumber(key) {
            var result = key;
            var index = specialChars.indexOf(key);
            if (index !== -1) {
                result = numChars[index];
            }
            return result;
        }

        function buildCode(tag, key) {
            var k = mapToNumber(key);
            if (isNaN(k)) return tag;
            tag += k;
            return tag;
        }

        function isMobile() {
            var check = false;
            (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
            return check;
        }

        var view_key = "useRFID";

        function useRFIDLogin() {
            var rfidCookie = $cookies.get(view_key);
            var isViewLocked = rfidCookie != null;
            var isViewMobile = isMobile();
            return isViewLocked ? rfidCookie === "true" : !isViewMobile;
        }

        function isLoginViewLocked() {
            return $cookies.get(view_key) != null;
        }

        function setLoginView(useRFID) {
            var isViewLocked = isLoginViewLocked();
            if (isViewLocked) {
                $cookies.remove(view_key);
            } else {
                $cookies.put(view_key, useRFID, { expires: new Date(2144232732000) });
            }
        }

        return rfid;
    }])
    .factory("attend", ["$filter", function ($filter) {

        const logType = {
            entry: '01',
            shortBreak: '#02a',
            lunchBreak: '02',
            leave: '03'
        };

        const shortBreak = 15 * 60 * 1000; //ms
        const lunchBreak = 30 * 60 * 1000; //ms

        const today = new Date();
        const shiftStart = new Date(today.getFullYear(), today.getMonth(), today.getDate(), 6, 30);

        function reduceLog(logs) {
            if (logs == null) return [];
            return logs.reduce(function (prev, curr) {
                if (prev.length === 0 && curr.Type === logType.entry) {
                    var date = new Date(curr.Date);
                    if (date < shiftStart) curr.Date = $filter('date')(shiftStart, 'yyyy-MM-ddTHH:mm:ss');
                    return [curr];
                }
                var last = prev.length === 0 ? null : prev[prev.length - 1];
                if (last != null && last.DateEnd == null && last.Type === curr.Type && last.Type !== logType.entry && curr.Type !== logType.entry) {
                    return prev;
                }

                if (curr.Type === logType.shortBreak || curr.Type === logType.lunchBreak) {
                    prev.push(curr);
                    return prev;
                }

                if ((last.Type === logType.shortBreak || last.Type === logType.lunchBreak) && curr.Type === logType.entry) {
                    last.DateEnd = curr.Date;
                    var from = new Date(last.Date);
                    var to = new Date(curr.Date);
                    var diff = Math.ceil((to - from) / (1000 * 60)) * 1000 * 60;
                    if (last.Type === logType.shortBreak) diff = diff > shortBreak ? diff - shortBreak : 0;
                    last.diff = diff;
                }

                if (curr.Type === logType.leave) {
                    prev.push(curr);
                }

                return prev;
            }, []);
        }

        function entryDate(logs) {
            var entry = (logs || []).filter(f => f.Type === '01')[0];
            if (entry == null) return null;
            return new Date(entry.Date);
        }

        function leaveDate(logs) {
            var entry = entryDate(logs);
            if (entry == null) return null;
            var correction = 0;
            var hasLunch = false;
            var totalLunch = 0;

            logs.forEach(function (log) {
                if (log.diff != null) {
                    correction += log.diff;
                    if (log.Type === '02') {
                        hasLunch = true;
                        totalLunch += log.diff;
                    }

                }
            });

            if (!hasLunch) {
                correction += lunchBreak;
            }
            else if (totalLunch < lunchBreak) {
                correction += lunchBreak - totalLunch;
            }

            entry.setTime(entry.getTime() + 8 * 60 * 60 * 1000 + correction);
            return entry;
        }

        function logTypeText(type) {
            switch (type) {
                case '#02a':
                case '02a':
                    return 'Placená přestávka';
                case '01':
                    return 'Začátek směny';
                case '02':
                    return 'Přestávka';
                case '03':
                    return 'Odchod';
                case '04':
                    return 'Služ.cesta';
                case '16':
                    return 'Domácí úkol';
                case '07':
                    return 'K lékaři';
                case '05':
                    return 'Soukr.odchod';
                case '08':
                    return 'Dovolená';
                default:
                    return null;
            }
        }

        return {
            reduceLog: reduceLog,
            entryDate: entryDate,
            leaveDate: leaveDate,
            logTypeText: logTypeText
        };
    }])
    .directive('convertToNumber', function () {
        return {
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                ngModel.$parsers.push(function (val) {
                    if (val == null || val === '') return undefined;
                    return val != null ? parseInt(val, 10) : null;
                });
                ngModel.$formatters.push(function (val) {
                    return val != null ? '' + val : null;
                });
            }
        };
    })
    .directive('autofocus', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                $timeout(function () {
                    $element[0].focus();
                });
            }
        };
    }])
    .directive('workType', [function () {
        return {
            restrict: 'A',
            scope: {
                workType: "=workType"
            },
            template: '<i ng-class="getClass(workType)">',
            controller: function ($scope) {
                $scope.getClass = function (workType) {
                    if (workType === 1) return "fa fa-wrench";
                    if (workType === 2) return "fa fa-bolt";
                    return "fa fa-paper-plane";
                };
            }
        };
    }])
    .directive('editAssignment', ['$compile', '$templateCache', 'api', 'utils', function ($compile, $templateCache, api, utils) {

        function updateDuration(assignment) {
            assignment = assignment || {};

            assignment.duration = {};
            assignment.expectedDuration = {};

            var duration = assignment.Duration || 0;
            if (assignment.StartRun != null) {
                duration = duration + (new Date().getTime() - new Date(assignment.StartRun).getTime());
            }
            assignment.duration.hours = Math.floor(duration / (1000 * 60 * 60));
            assignment.duration.minutes = Math.floor(duration % (1000 * 60 * 60) / (1000 * 60));
            var expectedDuration = (assignment || {}).ExpectedDuration || 0;
            assignment.expectedDuration.hours = Math.floor(expectedDuration / (1000 * 60 * 60));
            assignment.expectedDuration.minutes = Math.floor(expectedDuration % (1000 * 60 * 60) / (1000 * 60));
        }

        function calcDuration(assignment) {
            assignment.ExpectedDuration = assignment.expectedDuration.hours * 1000 * 60 * 60 + assignment.expectedDuration.minutes * 1000 * 60;
            var duration = assignment.duration.hours * 1000 * 60 * 60 + assignment.duration.minutes * 1000 * 60;
            if (assignment.StartRun != null) {
                duration = duration - (new Date().getTime() - new Date(assignment.StartRun).getTime());
            }
            assignment.Duration = duration;
        }

        function close() {
            var element = angular.element(document.getElementById('modal-holder'));
            element.empty();
        }

        function cancel() {
            close();
        }

        return {
            restrict: 'A',
            replace: false,
            scope: {
                editAssignment: '=editAssignment',
                callback: '=callback',
                users: '=users',
                projects: '=projects',
                groups: '=groups'
            },
            link: function (scope, elem, attrs) {

                function ok(assignment) {
                    calcDuration(assignment);
                    api.update(assignment).then(function (ass) {

                        scope.editAssignment.user = (scope.users.filter(function (i) { return i.Id === ass.UserId; })[0] || {}).UserName;
                        scope.editAssignment.project = (scope.projects.filter(function (i) { return i.Id === ass.ProjectId; })[0] || {}).Name;
                        scope.editAssignment.role = (scope.groups.filter(function (i) { return i.Id === ass.RoleId; })[0] || {}).Name;
                        if (scope.subaction != null && scope.assignment.status !== scope.subaction.name) {
                            scope.subaction(ass).then(function (assignEdited) {
                                scope.subaction = null;
                                utils.fullUpdate(scope.editAssignment, assignEdited);
                                if (scope.callback != null) scope.callback();
                                close();
                            });
                        } else {
                            utils.fullUpdate(scope.editAssignment, ass);
                            if (scope.callback != null) scope.callback();
                            close();
                        }
                    });
                }

                function start() {
                    scope.subaction = api.start;
                }

                function stop() {
                    scope.subaction = api.stop;
                }

                function pause(assignment) {
                    scope.subaction = api.pause;
                }

                function isActive(action) {
                    if (scope.subaction != null) {
                        return scope.subaction.name === action.name;
                    } else {
                        const assign = scope.assignment;
                        if (action === scope.start) return assign.StartRun != null;
                        if (action === scope.stop) return assign.Stop != null;
                        if (action === scope.pause) return assign.Start != null && assign.StartRun == null && assign.Stop == null;
                        return false;
                    }
                }

                elem.bind('click', function () {
                    scope.assignment = angular.copy(scope.editAssignment);
                    const assign = scope.assignment;
                    if (assign.StartRun != null) {
                        scope.assignment.status = 'start';
                    } else if (assign.Stop != null) {
                        scope.assignment.status = 'stop';
                    } else if (assign.Start != null && assign.StartRun == null && assign.Stop == null) {
                        scope.assignment.status = 'pause';
                    }
                    scope.ok = ok;
                    scope.cancel = cancel;
                    scope.close = close;

                    scope.start = start;
                    scope.stop = stop;
                    scope.pause = pause;
                    scope.isActive = isActive;

                    var element = angular.element(document.getElementById('modal-holder'));
                    if (element == null) return;
                    updateDuration(scope.assignment);
                    element.html($templateCache.get('editAssignment.html'));
                    $compile(element.contents())(scope);
                });
            }
        };
    }]);