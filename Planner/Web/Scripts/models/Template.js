﻿app.controller("templateCtrl", ["$scope", "api", "utils", function ($scope, api, utils) {

    $scope.filter = {
        template: true
    };

    $scope.blocks = [];
    $scope.template = null;
    $scope.templates = {};
    $scope.groups = [];

    $scope.templatesMap = {};

    $scope.projectTemplate = {};
    $scope.projectTemplates = [];

    $scope.duration = {
        hours: 0,
        minutes: 0
    };

    $scope.getDuration = utils.durationTime;
    $scope.sumDuration = utils.sumTemplateTime;

    $scope.select = function (template) {
        if ($scope.template != null && template.Id === $scope.template.Id) {
            $scope.template = null;
            return;
        }
        $scope.template = angular.copy(template);
        $scope.template.BlockOriginId = $scope.template.BlockId;
        if ($scope.template.BlockId != null) $scope.template.BlockId = $scope.template.BlockId + "";
    };

    $scope.create = function () {
        api.createTemplate($scope.template).then(function (template) {
            if (template.Id == null) return;
            $scope.templates[template.BlockId || 0] = $scope.templates[template.BlockId || 0] || [];
            $scope.templates[template.BlockId || 0].unshift(template);
            $scope.template = null;
        });
    };

    $scope.update = function () {
        api.updateTemplate($scope.template).then(function (template) {
            if (template == null) return;
            var blockId = template.BlockId || 0;
            var blockOriginId = $scope.template.BlockOriginId || 0;
            $scope.template = null;
            $scope.templates[blockId] = $scope.templates[blockId] || [];
            if (blockId !== blockOriginId) {
                $scope.templates[blockOriginId] = $scope.templates[blockOriginId].filter(function (f) { return f.Id !== template.Id; });
                $scope.templates[blockId].unshift(template);
            } else {
                var tmp = $scope.templates[blockId].filter(function (f) { return f.Id === template.Id; })[0];
                if (tmp == null) return;
                tmp.Name = template.Name;
                tmp.Code = template.Code;
                tmp.RoleId = template.RoleId;
                tmp.BlockId = template.BlockId;
                tmp.Description = template.Description;
                tmp.Duration = template.Duration;
                tmp.WorkType = template.WorkType;
            }
        });
    };

    $scope.delete = function (template) {
        api.deleteTemplate(template.Id).then(function (result) {
            if (!result) return;
            var index = $scope.templates[template.BlockId || 0].indexOf(template);
            if (index !== -1) $scope.templates[template.BlockId || 0].splice(index, 1);
        });
    };

    $scope.cancel = function () {
        $scope.template = null;
    };

    $scope.durationChanged = function () {
        $scope.template.Duration = $scope.duration.hours * 1000 * 60 * 60 + $scope.duration.minutes * 1000 * 60;
    };

    $scope.isInvalid = function () {
        return ($scope.template == null
            || $scope.template.Name == null
            || $scope.template.Name === "");
    };


    function createBlock(block) {
        api.createBlock({ Name: block.newName, Description: block.Description }).then(function (bl) {
            $scope.blocks.splice(1, 0, bl);
            block.newName = '';
            block.edit = false;
        });
    }

    function updateBlock(block) {
        api.updateBlock({ Id: block.Id, Name: block.newName, Description: block.Description }).then(function (bl) {
            block.Name = bl.Name;
            block.Description = bl.Description;
            block.edit = false;
        });
    }

    $scope.saveBlock = function (block) {
        block.Id === 0 ? createBlock(block) : updateBlock(block);
    };

    $scope.deleteBlock = function (block) {
        if (!confirm("Odstranit " + block.Name + " ?")) {
            return;
        }
        api.deleteBlock(block.Id).then(function (result) {
            if (!result) return;
            var templates = $scope.templates[block.Id] || [];
            for (var i = 0; i < templates.length; i++) {
                templates[i].BlockId = null;
                $scope.templates[0].push(templates[i]);
            }

            var index = $scope.blocks.indexOf(block);
            if (index !== -1) $scope.blocks.splice(index, 1);
        });
    };

    //ProjectTemplates
    $scope.createProjectTemplate = function () {
        api.createProjectTemplate($scope.projectTemplate).then(function (template) {
            $scope.projectTemplates.unshift(template);
            $scope.projectTemplate = {};
        });
    };

    $scope.editProject = function (project) {
        api.updateProjectTemplate({ Id: project.Id, Name: project.newName }).then(function (res) {
            project.Name = res.Name;
            project.edit = false;
        });
    };

    $scope.deleteProject = function (project) {
        api.deleteProjectTemplate(project.Id).then(function () {
            var index = $scope.projectTemplates.indexOf(project);
            if (index > -1) {
                $scope.projectTemplates.splice(index, 1);
                project.delete = false;
            };
        });
    };

    $scope.toggleProjectTemplate = function (projectId, templateId) {
        if (projectId == null || templateId == null) return;
        if (($scope.templatesMap[projectId] || []).indexOf(templateId) > -1) {
            api.removeFromTemplateProject(projectId, templateId).then(function () {
                var index = $scope.templatesMap[projectId].indexOf(templateId);
                if (index > -1) $scope.templatesMap[projectId].splice(index, 1);
            });
        } else {
            api.addToTemplateProject(projectId, templateId).then(function () {
                if ($scope.templatesMap[projectId] == null) { $scope.templatesMap[projectId] = [templateId]; }
                else {
                    $scope.templatesMap[projectId].push(templateId);
                }
            });
        }
    };

    $scope.sumProjectDuration = function (templateIds) {
        if (templateIds == null || templateIds.length === 0) return $scope.sumDuration([]);
        var templates = [];
        $scope.blocks.forEach(function (b) {
            ($scope.templates[b.Id] || []).forEach(function (t) {
                if (templateIds.indexOf(t.Id) !== -1) templates.push(t);
            });
        });
        return $scope.sumDuration(templates);
    };

    $scope.getSelectedByBlock = function (templates, projectTemplateIds) {
        if (templates == null || templates.length === 0
            || projectTemplateIds == null || projectTemplateIds.length === 0) return [];
        return templates.filter(function (f) { return projectTemplateIds.indexOf(f.Id) > -1; });
    };

    $scope.$watch("template.Duration", function (duration) {
        duration = duration || 0;
        $scope.duration.hours = Math.floor(duration / (1000 * 60 * 60));
        $scope.duration.minutes = (duration % (1000 * 60 * 60)) / (1000 * 60);
    });

    api.getTemplates().then(function (data) {
        $scope.projectTemplates = data.ProjectTemplates;
        $scope.templatesMap = data.TemplatesMap;
        $scope.templates = data.Templates;
        $scope.groups = data.Groups;
        $scope.blocks = data.Blocks;
        $scope.groupsDic = {};
        $scope.groups.forEach(function (item) { $scope.groupsDic[item.Id] = item; });
    });
}]);