﻿app.controller("groupCtrl", ["$scope", "api", "utils", function ($scope, api, utils) {
    $scope.groups = [];

    $scope.group = {};

    $scope.select = function (group) {
        if ($scope.group.Id === group.Id) {
            $scope.group = {};
            return;
        }
        $scope.group = angular.copy(group);
    }

    $scope.isInvalid = function () {
        if ($scope.group.Name === "" || $scope.group.Name == null) return true;
        if ($scope.groups.filter(function (i) { return i.Name === $scope.group.Name; })[0] != null) return true;
        return false;
    }

    $scope.create = function () {
        if ($scope.isInvalid()) return;
        api.createGroup($scope.group.Name).then(function (group) {
            if (group == null) {
                alert("Nepodařilo se vytvořit skupinu " + $scope.group.Name);
                return;
            }
            $scope.groups.push(group);
            $scope.group = {};
        });
    }

    $scope.update = function () {
        if ($scope.isInvalid()) return;
        api.updateGroup($scope.group).then(function (group) {
            if (group == null) {
                alert("Nepodařilo se upravit skupinu " + $scope.group.Name);
                return;
            }
            var original = $scope.groups.filter(function (item) { return group.Id === item.Id; })[0];
            if (original != null) original.Name = group.Name;
        });
    }

    $scope.deleteGroup = function (id) {
        api.deleteGroup(id).then(function (success) {
            if (!success) return;
            $scope.groups = $scope.groups.filter(function (group) { return group.Id !== id });
            $scope.users
                .filter(function (item) { return item.GroupId === id })
                .forEach(function (item) { item.GroupId = null; });
            $scope.group = {};
        });
    }

    $scope.addToGroup = function (user) {
        var us = angular.copy(user);
        us.GroupId = $scope.group.Id;
        api.updateUser(us).then(function () {
            user.GroupId = us.GroupId;
        });
    }

    $scope.removeFromGroup = function (user) {
        var us = angular.copy(user);
        us.GroupId = null;
        api.updateUser(us).then(function () {
            user.GroupId = null;
        });
    }

    api.getGroups().then(function (data) {
        $scope.groups = data.Groups;
        $scope.users = data.Users;
    });
}]);