﻿app.controller("loginController", ["$scope", "$document", "rfid", function ($scope, $document, rfid) {

    var email = document.getElementById('Email');
    var passord = document.getElementById('Password');
    var form = document.forms[0];

    if (email.value === "rfid") email.value = "";

    $scope.vm = {};
    $scope.tag = "";

    $scope.vm.isMobile = rfid.isMobile();  
    $scope.isViewLocked = rfid.isLoginViewLocked();
    $scope.vm.useRFID = rfid.useRFIDLogin();

    $scope.lockView = function () {
        rfid.setLoginView($scope.vm.useRFID);
        $scope.isViewLocked = rfid.isLoginViewLocked();
    };

    function submit() {
        email.value = "rfid";
        passord.value = $scope.tag;
        $scope.tag = "";
        form.submit();
    }

    $document.bind('keypress', function (e) {
        if (!$scope.vm.useRFID) return;   
        if (e.keyCode === 13) {
            if ($scope.tag.length === 10) submit();
            return;
        }
        $scope.tag = rfid.buildCode($scope.tag, e.key);
        if ($scope.tag.length === 10) {
            submit();
        }
    });

}]);