﻿app.controller("projectCtrl", ["$scope", "$filter", "orderByFilter", "api", "utils", function ($scope, $filter, orderByFilter, api, utils) {

    $scope.state = {
        tab: "users",
        projectName: "",
        months: 0,
        listProjects: 0
    };

    $scope.projectTimes = [{}, {}, {}, {}];

    $scope.found = [];
    $scope.groups = [];
    $scope.projects = [];
    $scope.assignments = [];

    $scope.duration = utils.duration;
    $scope.durationTime = utils.durationTime;


    function mapAssignments(assignments) {
        $scope.assignments = assignments.filter(function (assign) { return assign.ProjectId === $scope.project.Id; });
        $scope.projectTimes = utils.getProjectTimesByType($scope.assignments);
        $scope.users.forEach(function (user) {
            user.assignment = null;
            user.assignments = $scope.assignments.filter(function (t) {
                if (t.StartRun != null && t.UserId === user.Id) user.assignment = t;
                return t.UserId === user.Id;
            });
        });
    }

    $scope.filterProjects = function (project) {
        if ($scope.state.listProjects === -1) return project.CloseDate == null && project.Planned;
        if ($scope.state.listProjects === 0) return project.CloseDate == null && !project.Planned;
        if ($scope.state.listProjects === 1) return project.CloseDate != null;
        return false;
    };

    $scope.select = function (project) {
        if ($scope.project != null && $scope.project.mod != null) $scope.project.mod = null;
        $scope.project = project;
        if (project == null) return;
        $scope.project.mod = angular.copy($scope.project);
        $scope.found = [];
        // api.getAssignments(project.Id).then(mapAssignments);
        $scope.state.months = 0;
        $scope.project.tDate = null;
        if ($scope.project.TargetDate != null) $scope.project.tDate = new Date($scope.project.TargetDate);
        $scope.changeView($scope.project.View, 0);
    };

    $scope.isProjectEdited = function () {
        if ($scope.project == null) return false;
        return $scope.project.mod.RoleId !== $scope.project.RoleId || $scope.project.mod.Name !== $scope.project.Name;
    };

    $scope.updateProject = function () {
        if (!$scope.isProjectEdited()) return;
        api.updateProject($scope.project.mod).then(function (project) {
            $scope.project.RoleId = project.RoleId;
            $scope.project.Name = project.Name;
        });
    };

    $scope.findProject = function () {
        api.findProject($scope.state.projectName).then(function (assignments) {
            $scope.found = assignments;
        });
    };

    $scope.removeFound = function (assignment) {
        var index = $scope.found.indexOf(assignment);
        if (index > -1) $scope.found.splice(index, 1);
    };

    $scope.getGroupName = function (groupId) {
        return ($scope.groups.filter(function (gr) {
            return gr.Id === groupId;
        })[0] || {}).Name || "";
    };

    $scope.addToProject = function () {
        api.addToProject($scope.project.Id, $scope.found.map(function (item) { return item.Id; })).then(function () {
            $scope.found = [];
            $scope.select($scope.project);
        });
    };

    $scope.getUserName = function (userId) {
        return ($scope.users.filter(function (item) {
            return item.Id === userId;
        })[0] || {}).UserName || "";
    };

    $scope.orderByLength = function (user) {
        return -(user.assignments || []).length;
    };

    $scope.orderByState = function (assignment) {
        if (assignment.Stop != null) return 3;
        if (assignment.StartRun != null) return 0;
        if (assignment.Start != null) return 1;
        return 2;
    };

    $scope.getStateClass = function (assignment) {
        var state = $scope.orderByState(assignment);
        if (state === 3) return "fa-stop";
        if (state === 0) return "fa-play";
        if (state === 1) return "fa-pause";
        return "fa-question";
    };

    $scope.getTaskColor = function (assignment) {
        if (assignment == null || assignment.Start == null || assignment.ExpectedDuration === 0) return "";
        var actualDuration = utils.actualDuration(assignment);
        return actualDuration > 0 ? "back-red" : "back-green";
    };

    $scope.openProject = function () {
        api.openProject($scope.project.Id).then(function () {
            $scope.project.CloseDate = null;
            $scope.modal = null;
        });
    };
        
    $scope.closeProject = function () {
        if (!$scope.canCloseProject()) {
            return;
        }
        api.closeProject($scope.project.Id).then(function () {
            $scope.project.CloseDate = new Date();
        });
    };

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    $scope.workersChanged = function (workType, workersCount) {
        api.projectChangeWorkers($scope.project.Id, workType, workersCount);
    };

    $scope.getFinishedDay = function (workType, workersCount) {
        if ($scope.projectTimes[workType].total == null) return null;
        var diff = $scope.projectTimes[workType].total.duration - $scope.projectTimes[workType].finished.duration;
        if (workersCount == null || workersCount === 0) return null;
        var days = (diff / (1000 * 60 * 60)) / 8 / workersCount;
        var date = new Date();
        var hours = (days % 1) * 8;
        days = Math.floor((date.getHours() + hours) > 15 ? days + 1 : days);
        while (days > 0) {
            date = addDays(date, 1);
            var dayOfWeek = date.getDay();
            if (dayOfWeek !== 0 && dayOfWeek !== 6) days--;
        }
        return date;
    };

    $scope.editCallback = function () {
        mapAssignments($scope.assignments);
    };

    $scope.saveTargetDate = function () {
        $scope.project.TargetDate = $scope.project.tDate;
        api.updateProject($scope.project).then(function () {
            $scope.state.setTargetDate = false;
        });
    };

    $scope.changeView = function (view, months) {
        $scope.project.View = view;
        $scope.state.months = months;
        api.projectChangeView($scope.project.Id, view, $scope.state.months).then(mapAssignments);
    };

    var monthsName = ["leden", "únor", "březen", "duben", "květen", "červen", "červenec", "srpen", "září", "říjen", "listopad", "prosinec"];

    $scope.getDate = function () {
        var d = new Date();
        d.setMonth(d.getMonth() - $scope.state.months);
        return monthsName[d.getMonth()] + " " + d.getFullYear();
    };

    $scope.plannedChanged = function () {
        api.projectChangePlanned($scope.project.Id, $scope.project.Planned);
    };

    $scope.archiveTasks = function () {
        api.archiveByProject($scope.project.Id).then(function () {
            $scope.assignments.forEach(function (assign) {
                if (assign.Stop != null) assign.Archived = true;
            });
        });
    };

    $scope.canCloseProject = function () {
        return $scope.assignments.filter(function (a) { return a.Stop == null; })[0] == null;
    };

    $scope.canArchive = function () {
        return $scope.assignments.filter(function (a) { return a.Stop != null && !a.Archived; })[0] != null;
    };

    api.getProjects().then(function (data) {
        $scope.projects = $filter('orderBy')(data.Projects, ['Planned', '-CloseDate']);
        $scope.groups = data.Groups;
        $scope.users = data.Users;
        $scope.select($scope.projects[0]);
    });
}]);