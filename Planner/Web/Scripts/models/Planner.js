﻿app.controller("plannerCtrl", ["$scope", "$interval", "api", "utils", "orderByFilter", function ($scope, $interval, api, utils, orderBy) {

    $scope.detail = {};
    $scope.detailAll = {};

    $scope.edited = null;
    $scope.assignment = {};

    $scope.blocks = [];
    $scope.users = [];

    $scope.state = {
        create: false
    };

    $scope.project = {
        id: null,
        active: null,
        mod: null
    };
    $scope.projects = [];
    $scope.assignment = { WorkType: 1 };
    $scope.assignments = {};

    $scope.newAssignments = [];

    $scope.duration = {
        hours: 0,
        minutes: 0
    };

    $scope.expectedDuration = {
        hours: 0,
        minutes: 0
    };

    function calcTaskTimes(assignment) {
        if (assignment == null) return;

        var duration = assignment.Duration || 0;
        if (assignment.StartRun != null) {
            duration = duration + (new Date().getTime() - new Date(assignment.StartRun).getTime());
        }
        assignment.time = {
            expected: utils.getTime(assignment.ExpectedDuration),
            finished: utils.getTime(duration),
            plan: utils.getTime(duration - assignment.ExpectedDuration)
        };
    }

    function defAssign() {
        return { WorkType: 1 };
    }

    $scope.assignmentTime = utils.duration;
    $scope.getDuration = utils.durationTime;
    $scope.sumDuration = utils.sumTemplateTime;


    $scope.getDurationWork = function (assignment) {
        duration = assignment.Duration || 0;
        if (assignment.StartRun != null) {
            duration = duration + (new Date().getTime() - new Date(assignment.StartRun).getTime());
        }
        return Math.floor(duration / (1000 * 60 * 60)) + " hodin " + Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60)) + " minut";
    };

    $scope.toggleDetail = function (userId) {
        $scope.detail[userId] = !$scope.detail[userId];
    };

    $scope.assignmentsCount = function (userId) {
        return utils.count($scope.assignments[userId]);
    };

    $scope.select = function (assign) {
        if ($scope.newAssignments.length > 0) return;
        $scope.assignment = angular.copy(assign || defAssign());
        $scope.project.id = $scope.assignment.ProjectId;
        $scope.projectChanged();
    };

    $scope.selectFrom = function (assign) {
        $scope.assignment = assign === $scope.assignment ? defAssign() : assign;
    };

    $scope.workerChanged = function () {
        var userId = $scope.assignment.UserId === "" ? null : $scope.assignment.UserId;
        var user = $scope.users.filter(function (u) { return u.Id === userId; })[0];

        if ($scope.assignment.Duration != null || $scope.newAssignments.length === 0) {
            if (user != null) $scope.assignment.WorkType = user.WorkType;
        } else {
            $scope.newAssignments.forEach(function (item) {
                item.UserId = userId;
                if (user != null) item.WorkType = user.WorkType;
            });
        }
    };

    $scope.groupChanged = function () {
        if ($scope.assignment.Duration != null || $scope.newAssignments.length === 0) return;
        $scope.newAssignments.forEach(function (item) { item.RoleId = $scope.assignment.RoleId; });
        var group = $scope.assignment.RoleId === "" ? null : $scope.assignment.RoleId;
        $scope.newAssignments.forEach(function (item) { item.RoleId = group; });
    };

    $scope.create = function () {
        $scope.assignment.ProjectId = $scope.project.id;
        if (($scope.assignment.RoleId || '') === '') {
            $scope.assignment.RoleId = ($scope.project.active || {}).RoleId;
        }
        api.create($scope.assignment).then(function (item) {
            if (item.UserId != null) {
                if ($scope.assignments[item.UserId] == null) $scope.assignments[item.UserId] = [];
                $scope.assignments[item.UserId].push(item);
                $scope.assignment = { UserId: item.UserId };
            }

            if (item.UserId == null && item.RoleId != null) {
                if ($scope.assignments[item.RoleId] == null) $scope.assignments[item.RoleId] = [];
                $scope.assignments[item.RoleId].push(item);
                $scope.assignment = { RoleId: item.RoleId };
            }
        });
    };

    $scope.createAssignments = function () {
        $scope.newAssignments.forEach(function (assignment) {
            assignment.ProjectId = $scope.project.id;
            if ((assignment.RoleId || '') === '') {
                assignment.RoleId = ($scope.project.active || {}).RoleId;
            }
        });
        api.createMultiple($scope.newAssignments).then(function (assignments) {
            $scope.newAssignments = [];
            $scope.assignment = {};
            if (assignments == null) return;
            assignments.forEach(function (item) {
                var target = item.UserId || item.RoleId;
                if ($scope.assignments[target] == null)
                    $scope.assignments[target] = [];
                $scope.assignments[target].push(item);
            });
        });
    };

    $scope.isInvalid = function (assignment) {
        if (assignment == null
            || !((assignment.UserId || "" !== "") || (assignment.RoleId || "" !== "") || (($scope.project.active || {}).RoleId || "" !== ""))) return true;
        return false;
    };

    $scope.isNewAssignmentsInvalid = function () {
        return $scope.newAssignments.filter(function (f) { return $scope.isInvalid(f); })[0] != null;
    };

    $scope.cancelAssignments = function () {
        $scope.assignment = defAssign();
        $scope.newAssignments = [];
    };


    $scope.addFromTemplates = function (templates) {
        if (templates == null || templates.length === 0) return;
        if ($scope.assignment.Id != null) $scope.assignment = defAssign();
        if ($scope.group != null) $scope.group = null;
        for (var i = 0; i < templates.length; i++) {
            var tmp = templates[i];
            var blockName = "";
            if (tmp.BlockId != null) blockName = ($scope.blocks.filter(function (bl) { return bl.Id === tmp.BlockId; })[0] || {}).Name;
            var assign = {
                BlockName: blockName,
                Name: tmp.Name,
                Description: tmp.Description,
                ExpectedDuration: tmp.Duration,
                Code: tmp.Code,
                Duration: 0,
                RoleId: tmp.RoleId,
                UserId: null,
                WorkType: tmp.WorkType
            };

            $scope.newAssignments.push(assign);
        }
        $scope.assignment = defAssign();
    };

    $scope.removeNewAssignment = function (assignments) {
        if (assignments == null) return;
        var index = $scope.newAssignments.indexOf(assignments);
        if (index !== -1) $scope.newAssignments.splice(index, 1);
        if ($scope.assignment === assignments) $scope.assignment = $scope.newAssignments[0] || defAssign();
    };

    $scope.update = function () {
        $scope.assignment.ProjectId = $scope.project.id;
        api.update($scope.assignment).then(function (assign) {
            for (var group in $scope.assignments) {
                $scope.assignments[group] = $scope.assignments[group].filter(function (item) { return item.Id !== assign.Id; });
            }

            var groupId = assign.UserId || assign.RoleId;
            $scope.assignments[groupId] = $scope.assignments[groupId] || [];
            $scope.assignments[groupId].push(assign);
            $scope.assignments[groupId].sort(function (a, b) { return a.Order - b.Order; });
            $scope.select();
        });
    };

    $scope.delete = function (assign) {
        if (assign == null) return;
        api.delete(assign).then(function () {
            $scope.assignments[assign.UserId || assign.RoleId] = $scope.assignments[assign.UserId || assign.RoleId].filter(function (item) {
                return item.Id !== assign.Id;
            });
        });
    };

    $scope.start = function (assignment) {
        api.start(assignment).then(function (assign) {
            utils.update(assignment, assign);
            calcTaskTimes(assignment);
        });
    };

    $scope.stop = function (assignment) {
        api.stop(assignment).then(function (assign) {
            utils.update(assignment, assign);
            calcTaskTimes(assignment);
        });
    };

    $scope.pause = function (assignment) {
        api.pause(assignment).then(function (assign) {
            utils.update(assignment, assign);
            calcTaskTimes(assignment);
        });
    };

    $scope.archive = function () {
        api.archive().then(function () {
            $scope.autoUpdate();
        });
    };

    //function findUserTasks(task) {
    //    if (task == null) return [];
    //    if (task.UserId == null) return $scope.assignments[task.RoleId].filter(function (t) { return t.Stop == null; }) || [];
    //    return $scope.assignments[task.UserId].filter(function (t) { return t.Stop == null; }) || [];
    //}

    function findUserTasks(task) {
        if (task == null) return [];
        var assign = [];
        if (task.UserId == null) {
            assign = $scope.assignments[task.RoleId].filter(function (t) { return t.Stop == null; }) || [];
        }
        else {
            assign = $scope.assignments[task.UserId].filter(function (t) { return t.Stop == null; }) || [];
        }

        return orderBy(assign, "Order", false);
    }

    function swapOrder(task, pos) {
        var userTasks = findUserTasks(task);
        var index = userTasks.indexOf(task);
        if (index === -1 || (index === 0 && pos < 0)) return;
        var otherTask = userTasks[index + pos];
        api.swap(task.Id, otherTask.Id).then(function (success) {
            var order = otherTask.Order;
            otherTask.Order = task.Order;
            task.Order = order;
        });
    }

    $scope.up = function (task) {
        swapOrder(task, -1);
    };

    $scope.down = function (task) {
        swapOrder(task, 1);
    };

    $scope.canUp = function (task) {
        var userTasks = findUserTasks(task);
        var index = userTasks.indexOf(task);
        return !(index === -1 || index === 0);
    };

    $scope.canDown = function (task) {
        var userTasks = findUserTasks(task);
        var index = userTasks.indexOf(task);
        return !(index === -1 || index === userTasks.length - 1);
    };

    $scope.filterGroup = function (group) {
        if ($scope.group === group) {
            $scope.group = null;
            return;
        }
        $scope.group = group;
    };

    $scope.getGroupName = function (groupId) {
        if (groupId == null) return "";
        return ($scope.groups.filter(function (item) { return item.Id === groupId; })[0] || {}).Name;
    };

    $scope.durationChanged = function () {
        var duration = $scope.duration.hours * 1000 * 60 * 60 + $scope.duration.minutes * 1000 * 60;
        if ($scope.assignment.StartRun != null) {
            duration = duration - (new Date().getTime() - new Date($scope.assignment.StartRun).getTime());
        }
        $scope.assignment.Duration = duration;
    };

    $scope.expectedDurationChanged = function () {
        $scope.assignment.ExpectedDuration = $scope.expectedDuration.hours * 1000 * 60 * 60 + $scope.expectedDuration.minutes * 1000 * 60;
    };

    $scope.getExpectedDuration = function (duration) {
        duration = duration || 0;
        return Math.floor(duration / (1000 * 60 * 60)) + " hodin " + Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60)) + " minut";
    };

    $scope.getAssignmentClass = function (assign) {
        if (assign.StartRun == null && assign.Stop == null && assign.Start == null) return 'warning';
        if (assign.StartRun != null && assign.Stop == null) return 'success';
        if (assign.StartRun == null && assign.Stop == null && assign.Start != null) return 'danger';
    };

    function updateDuration(assignment) {
        assignment = assignment || {};
        var duration = (assignment || {}).Duration || 0;
        if (assignment.StartRun != null) {
            duration = duration + (new Date().getTime() - new Date(assignment.StartRun).getTime());
        }
        $scope.duration.hours = Math.floor(duration / (1000 * 60 * 60));
        $scope.duration.minutes = Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60));
        var expectedDuration = (assignment || {}).ExpectedDuration || 0;
        $scope.expectedDuration.hours = Math.floor(expectedDuration / (1000 * 60 * 60));
        $scope.expectedDuration.minutes = Math.floor((expectedDuration % (1000 * 60 * 60)) / (1000 * 60));
    }

    //Project
    $scope.createProject = function () {
        if ($scope.project.mod.Id > 0) {
            api.updateProject($scope.project.mod).then(function (project) {
                var index = $scope.projects.indexOf($scope.project.active);
                if ($scope.project.active.RoleId !== $scope.project.mod.RoleId) {
                    loadData();
                    $scope.project.mod = null;
                    return;
                }
                if (index !== -1) {
                    $scope.projects.splice(index, 1);
                    $scope.projects.splice(index, 0, project);
                    $scope.project.active = project;
                }
            });
        } else {
            api.createProject($scope.project.mod).then(function (project) {
                $scope.projects.push(project);
                $scope.project.active = project;
                $scope.project.id = project.Id;
                $scope.project.mod = null;
            });
        }
    };

    $scope.deleteProject = function () {
        api.deleteProject($scope.project.id).then(function () {
            var index = $scope.projects.indexOf($scope.project.active);
            if (index > -1) $scope.projects.splice(index, 1);
            $scope.project.id = null;
            $scope.project.del = false;
        });
    };

    $scope.editProject = function () {
        $scope.project.mod = angular.copy($scope.project.active);
    };

    $scope.projectChanged = function () {
        $scope.project.active = $scope.projects.filter(function (p) { return p.Id === $scope.project.id; })[0] || {};
    };

    $scope.getProjectName = function (projectId) {
        return ($scope.projects.filter(function (pr) { return pr.Id === projectId; })[0] || {}).Name;
    };

    $scope.getStyle = function (assignment) {
        if (assignment.time == null || assignment.time.expected.duration === 0 || assignment.Start == null) return '';
        return assignment.time.plan.duration > 0 ? 'background-color:salmon' : 'background-color:darkseagreen';
    };

    $scope.$watch("assignment", updateDuration);
    //Load DATA

    function loadData() {
        api.getData().then(function (data) {
            $scope.users = data.Users;
            $scope.projects = data.Projects;
            $scope.assignments = data.Assignments;
            for (var id in $scope.assignments) {
                ($scope.assignments[id] || []).forEach(calcTaskTimes);
            }
            $scope.groups = data.Groups;
            $scope.templates = data.Templates;
            $scope.projectTemplates = data.ProjectTemplates;
            $scope.blocks = data.Blocks;
            $scope.roles = {};
            $scope.groups.forEach(function (g) {
                $scope.roles[g.Id] = g;
            });
            $scope.usersDic = {};
            $scope.users.forEach(function (g) {
                $scope.usersDic[g.Id] = g;
            });
        });
    }

    loadData();
}]);