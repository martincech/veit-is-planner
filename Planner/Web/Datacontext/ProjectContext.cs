﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Web.Datacontext.Enums;

namespace Web.Datacontext
{
    public partial class ApplicationDbContext
    {
        public IDbSet<Project> Projects { get; set; }

        #region Project
        internal Project CreateProject(Project p)
        {
            if (p == null) return null;
            Projects.Add(p);
            SaveChanges();
            return p;
        }

        internal Project UpdateProject(Project p)
        {
            if (p == null) return null;
            var project = Projects.FirstOrDefault(f => f.Id == p.Id);
            if (project == null) return null;
            if (project.RoleId != p.RoleId)
            {
                foreach (var ass in project.Assignments)
                {
                    if (ass.RoleId == project.RoleId || ass.RoleId == null) ass.RoleId = p.RoleId;
                }
            }
            project.Name = p.Name;
            project.RoleId = p.RoleId;
            project.Note = p.Note;
            project.CloseDate = p.CloseDate;
            project.TargetDate = p.TargetDate;
            project.View = p.View;
            SaveChanges();
            return project;
        }

        internal bool DeleteProject(int id)
        {
            var project = Projects.FirstOrDefault(f => f.Id == id);
            if (project == null) return false;
            project.Assignments.Clear();
            SaveChanges();
            Projects.Remove(project);
            SaveChanges();
            return true;
        }

        internal bool AddToProject(int projectId, List<int> assignmentIds)
        {
            var project = Projects.FirstOrDefault(f => f.Id == projectId);
            if (project == null) return false;
            var assignments = Assignments.Where(w => assignmentIds.Contains(w.Id)).ToList();
            foreach (var item in assignments)
            {
                if (item.RoleId == null && item.User != null) item.RoleId = project.RoleId;
                item.ProjectId = project.Id;
            }
            SaveChanges();
            return true;
        }

        internal bool CloseProject(int projectId)
        {
            var project = Projects.FirstOrDefault(f => f.Id == projectId);
            if (project == null || project.Assignments.Any(a => a.Stop == null)) return false;
            project.CloseDate = DateTime.Now;
            foreach (var item in project.Assignments)
            {
                item.Archived = true;
            }
            SaveChanges();
            return true;
        }

        internal bool OpenProject(int projectId)
        {
            var project = Projects.FirstOrDefault(f => f.Id == projectId);
            if (project == null) return false;
            project.CloseDate = null;
            SaveChanges();
            return true;
        }

        internal List<Assignment> ChangeProjectView(int projectId, ProjectViewType view, int months)
        {
            var project = Projects.FirstOrDefault(f => f.Id == projectId);
            if (project == null) return new List<Assignment>();
            if ((int)view != project.View && System.Threading.Thread.CurrentPrincipal.IsInRole(ADMIN))
            {
                project.View = (int)view;
                SaveChanges();
            }
            var assignments = project.Assignments.ToList();
            if (view == 0) return assignments;
            var monthDate = DateTime.Now.AddMonths(-months);
            var monthStart = new DateTime(monthDate.Year, monthDate.Month, 1);
            if (months <= 0) return assignments.Where(w => w.Stop == null || w.Stop.Value > monthStart).ToList();
            var monthStop = new DateTime(monthDate.Year, monthDate.Month, DateTime.DaysInMonth(monthDate.Year, monthDate.Month), 23, 59, 59);
            return assignments.Where(w => w.Stop.HasValue && w.Stop.Value > monthStart && w.Stop.Value < monthStop).ToList();
        }

        internal bool ChangeProjectWorkers(int projectId, WorkType workType, int workers)
        {
            var project = Projects.FirstOrDefault(f => f.Id == projectId);
            if (project == null) return false;
            if (workType == WorkType.Assembly) project.WorkersAs = workers;
            if (workType == WorkType.Electricity) project.WorkersEl = workers;
            SaveChanges();
            return true;
        }

        internal bool ChangeProjectPlanned(int projectId, bool planned)
        {
            var project = Projects.FirstOrDefault(f => f.Id == projectId);
            if (project == null) return false;
            project.Planned = planned;
            SaveChanges();
            return true;
        }

        #endregion
    }
}
