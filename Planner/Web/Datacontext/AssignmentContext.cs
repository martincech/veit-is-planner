﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Web.Datacontext.Enums;

namespace Web.Datacontext
{
    public partial class ApplicationDbContext
    {
        public IDbSet<Assignment> Assignments { get; set; }

        public List<Assignment> GetAllAssignments()
        {
            return Assignments.AsNoTracking().Where(w => !w.Archived).OrderBy(o => o.Order).Select(s => s).ToList();
        }

        public List<Assignment> GetArchiveAssignments()
        {
            return Assignments.AsNoTracking().Where(w => w.UserId != null && w.Archived).OrderByDescending(o => o.Stop).Select(s => s).ToList();
        }

        public List<Assignment> GetAssignmentsByUser(string userId)
        {
            var user = Users.FirstOrDefault(f => f.Id == userId);
            if (user == null) return new List<Assignment>();
            return Assignments.AsNoTracking()
                .Where(w => !w.Archived)
                .ToList()
                .Where(w => w.UserId == userId || (w.RoleId != null && user.Roles.Any(a => a.RoleId == w.RoleId)))
                .OrderBy(o => o.Order)
                .ToList();
        }

        public Assignment CreateAssignment(Assignment assignment)
        {
            if (assignment == null) return null;
            assignment.Created = DateTime.Now;
            if (assignment.WorkType == WorkType.Others) assignment.WorkType = WorkType.Assembly;
            assignment.Duration = 0;
            Assignments.Add(assignment);
            SaveChanges();
            assignment.Order = assignment.Id;
            SaveChanges();
            return assignment;
        }

        public List<Assignment> CreateAssignments(List<Assignment> assignments)
        {
            if (assignments == null) return null;
            return assignments.Select(s => CreateAssignment(s)).ToList();
        }

        public Assignment UpdateAssignment(Assignment assignment)
        {
            if (assignment == null) return null;
            var assign = Assignments.First(f => f.Id == assignment.Id);
            if (assign == null) return null;
            assign.Description = assignment.Description;
            assign.Code = assignment.Code;
            assign.Name = assignment.Name;
            assign.BlockName = assignment.BlockName;

            assign.ExpectedDuration = assignment.ExpectedDuration;
            assign.Archived = assignment.Archived;
            assign.UserId = assignment.UserId;
            assign.RoleId = assignment.RoleId;
            assign.ProjectId = assignment.ProjectId;
            assign.WorkType = assignment.WorkType;

            assign.Start = assignment.Start;
            assign.StartRun = assignment.StartRun;
            assign.Stop = assignment.Stop;
            SaveChanges();
            return assign;
        }

        public Assignment StartAssignment(Assignment assignment)
        {
            if (assignment == null) return null;
            var assign = Assignments.First(f => f.Id == assignment.Id);
            if (assign == null) return null;
            assign.UserId = assignment.UserId;
            assign.StartRun = DateTime.Now;
            assign.Start = assign.Start ?? assign.StartRun;
            assign.Stop = null;
            assign.Archived = false;
            SaveChanges();
            return assign;
        }

        public Assignment PauseAssignment(Assignment assignment)
        {
            if (assignment == null) return null;
            var assign = Assignments.First(f => f.Id == assignment.Id);
            if (assign == null) return null;
            CalcAssignment(assign);
            assign.Archived = false;
            SaveChanges();
            return assign;
        }

        public Assignment StopAssignment(Assignment assignment)
        {
            if (assignment == null) return null;
            var assign = Assignments.First(f => f.Id == assignment.Id);
            if (assign == null) return null;
            CalcAssignment(assign);
            assign.Stop = DateTime.Now;
            SaveChanges();
            return assign;
        }

        public Assignment ArchiveAssignment(Assignment assignment)
        {
            if (assignment == null) return null;
            var assign = Assignments.First(f => f.Id == assignment.Id);
            if (assign == null || assign.Stop == null) return null;
            assign.Archived = true;
            SaveChanges();
            return assign;
        }

        private static void CalcAssignment(Assignment assign)
        {
            if (assign == null) return;
            assign.StartRun = null;
            assign.Stop = null;
        }

        public bool DeleteAssignment(Assignment assignment)
        {
            if (assignment == null) return false;
            var assign = Assignments.First(f => f.Id == assignment.Id);
            if (assign == null) return false;
            Assignments.Remove(assign);
            SaveChanges();
            return true;
        }

        public bool SwapAssignmentOrder(int id, int id2)
        {
            var assign = Assignments.FirstOrDefault(f => f.Id == id);
            var assign2 = Assignments.FirstOrDefault(f => f.Id == id2);
            if (assign == null || assign2 == null || assign.UserId != assign2.UserId) return false;
            var order = assign.Order;
            assign.Order = assign2.Order;
            assign2.Order = order;
            SaveChanges();
            return true;
        }


        public bool ArchiveAssignments()
        {
            try
            {
                var now = DateTime.Now.Date;
                var startOfMonth = new DateTime(now.Year, now.Month, 1);
                var projects = Projects.Where(w => w.View == (int)ProjectViewType.Month).Select(s => s.Id);
                var query = $"UPDATE [dbo].[Assignments] SET Archived=1";
                query += $" WHERE Archived=0 AND Stop < '{startOfMonth.ToString("yyyy-MM-ddTHH:mm:ss")}'";
                query += $" AND (ProjectId IS NULL OR ProjectId IN ({string.Join(",", projects)}))";
                var result = Database.ExecuteSqlCommand(query);
                logger.Info($"Archived {result} assignments.");
            }
            catch (Exception e)
            {
                logger.Error(e);
                return false;
            }
            return true;
        }

        public bool ArchiveAssignments(int projectId)
        {
            try
            {
                var result = Database.ExecuteSqlCommand($"UPDATE [dbo].[Assignments] SET Archived=1 WHERE Stop IS NOT NULL AND ProjectId={projectId}");
                logger.Info($"Archived {result} assignments for project ID: {projectId}.");
            }
            catch (Exception e)
            {
                logger.Error(e);
                return false;
            }
            return true;
        }

        public Assignment UpdateAssignmentTime(Assignment assignment)
        {
            if (assignment == null) return null;
            var assign = Assignments.FirstOrDefault(f => f.Id == assignment.Id);
            if (assign == null) return null;

            assign.ExpectedDuration = assignment.ExpectedDuration;
            SaveChanges();
            return assign;
        }

        #region Stats utils

        public StatResult UsersClosedAssignmentnsPercent(int month)
        {
            var thisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var mFrom = thisMonth.AddMonths(-month);
            var mTo = mFrom.AddMonths(1);

            var closedAssignments = Assignments.Include(i => i.WorkTimes)
                .Where(w => w.ExpectedDuration > 0 && w.Stop != null && w.Stop > mFrom && w.Stop < mTo).ToList();

            var userDurations = new Dictionary<string, double[]>();
            foreach (var closedAssignment in closedAssignments)
            {
                if (closedAssignment.ExpectedDuration == 0 || closedAssignment.Duration == 0) continue;

                var userWorkTimes = closedAssignment.WorkTimes.Where(w => w.From != null && w.To != null).GroupBy(g => g.UserId);
                if (userWorkTimes.Count() > 1)
                {
                    var totalWorkTime = closedAssignment.WorkTimes.Where(w => w.From != null && w.To != null).Sum(s => (s.To - s.From).Value.TotalMilliseconds);
                    foreach (var worker in userWorkTimes)
                    {
                        if (worker.Key != null)
                        {
                            var userWorkTime = worker.Sum(s => (s.To - s.From).Value.TotalMilliseconds);
                            var multiplier = userWorkTime / totalWorkTime;
                            if (!userDurations.ContainsKey(worker.Key)) userDurations.Add(worker.Key, new double[] { 0, 0 });
                            userDurations[worker.Key][0] += (closedAssignment.ExpectedDuration * multiplier);
                            userDurations[worker.Key][1] += (closedAssignment.Duration * multiplier);
                        }
                    }
                }
                else
                {
                    if (!userDurations.ContainsKey(closedAssignment.UserId)) userDurations.Add(closedAssignment.UserId, new double[] { 0, 0 });
                    userDurations[closedAssignment.UserId][0] += (closedAssignment.ExpectedDuration);
                    userDurations[closedAssignment.UserId][1] += (closedAssignment.Duration);
                }
            }

            var userPercent = new Dictionary<string, int?>();
            double totalDur = 0;
            double totalExp = 0;
            foreach (var x in userDurations)
            {
                totalExp += x.Value[0];
                totalDur += x.Value[1];
                userPercent.Add(x.Key, Convert.ToInt32((x.Value[1] / x.Value[0]) * 100));
            }
            var totalPercent = totalExp == 0 ? 0 : Convert.ToInt32((totalDur / totalExp) * 100);
            return new StatResult { UserStats = userPercent, TotalPercent = totalPercent };
        }

        #endregion
    }

    public class StatResult
    {
        public Dictionary<string, int?> UserStats { get; set; }
        public int TotalPercent { get; set; }
    }
}
