﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Web.Datacontext
{
    public class ProjectTemplate
    {
        public ProjectTemplate()
        {
            Templates = new HashSet<Template>();
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public virtual ICollection<Template> Templates { get; set; }
    }
}