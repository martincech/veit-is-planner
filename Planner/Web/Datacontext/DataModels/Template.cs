﻿using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Web.Datacontext.Enums;

namespace Web.Datacontext
{
    public class Template
    {
        public Template()
        {
            ProjectTemplates = new HashSet<ProjectTemplate>();
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Duration { get; set; }
        public WorkType WorkType { get; set; }
        public string Code { get; set; }

        [ForeignKey(nameof(Role))]
        public string RoleId { get; set; }

        [JsonIgnore]
        public IdentityRole Role { get; set; }

        [ForeignKey(nameof(Block))]
        public int? BlockId { get; set; }

        [JsonIgnore]
        public Block Block { get; set; }

        [JsonIgnore]
        public virtual ICollection<ProjectTemplate> ProjectTemplates { get; set; }
    }
}
