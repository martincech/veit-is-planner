﻿namespace Web.Datacontext
{
    public class Group
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
