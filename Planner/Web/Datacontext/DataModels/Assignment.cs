﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using Web.Datacontext.Enums;

namespace Web.Datacontext
{
    public class Assignment
    {
        public Assignment()
        {
            WorkTimes = new HashSet<WorkTime>();
        }

        [Key]
        public int Id { get; set; }

        public int? Order { get; set; }

        public DateTime Created { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? Stop { get; set; }

        public DateTime? StartRun { get; set; }

        public bool Archived { get; set; }
        public double Duration { get; set; }
        public double ExpectedDuration { get; set; }
        public string Name { get; set; }
        public string BlockName { get; set; }
        public string Description { get; set; }
        public WorkType WorkType { get; set; }
        public string Code { get; set; }

        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

        [JsonIgnore]
        public ApplicationUser User { get; set; }

        [ForeignKey(nameof(Role))]
        public string RoleId { get; set; }

        [JsonIgnore]
        public IdentityRole Role { get; set; }

        [ForeignKey(nameof(Project))]
        public int? ProjectId { get; set; }

        [JsonIgnore]
        public Project Project { get; set; }

        [JsonIgnore]
        public virtual ICollection<WorkTime> WorkTimes { get; set; }
    }
}
