﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Web.Datacontext
{
    public class WorkTime
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey(nameof(Assignment))]
        public int AssignmentId { get; set; }
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

        public DateTime From { get; set; }
        public DateTime? To { get; set; }

        [JsonIgnore]
        public ApplicationUser User { get; set; }

        [JsonIgnore]
        public Assignment Assignment { get; set; }
    }
}
