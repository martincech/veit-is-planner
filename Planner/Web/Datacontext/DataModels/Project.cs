﻿using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Datacontext
{
    public class Project
    {
        public Project()
        {
            Assignments = new HashSet<Assignment>();
        }

        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CloseDate { get; set; }
        public DateTime? TargetDate { get; set; }
        public string Note { get; set; }
        public int View { get; set; }

        public int WorkersEl { get; set; }
        public int WorkersAs { get; set; }

        public bool Planned { get; set; }

        [ForeignKey(nameof(Role))]
        public string RoleId { get; set; }

        [JsonIgnore]
        public IdentityRole Role { get; set; }

        [JsonIgnore]
        public virtual ICollection<Assignment> Assignments { get; set; }
    }
}
