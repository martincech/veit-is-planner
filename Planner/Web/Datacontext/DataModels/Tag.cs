﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Web.Datacontext
{
    public class Tag
    {
        [Key]
        public int Id { get; set; }

        [Required]

        [StringLength(10)]
        [Index("CodeIndex", IsUnique = true)]
        public string Code { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Disabled { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public string UserId { get; set; }

        [JsonIgnore]
        public ApplicationUser User { get; set; }
    }
}
