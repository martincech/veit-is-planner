﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AttendDatabase.DataModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Web.Datacontext.Enums;
using Web.Migrations;

namespace Web.Datacontext
{
    public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public const string ADMIN_ID = "261aefc1-02eb-4188-944e-f9d778cb5aa8";
        public const string ADMIN = "Admin";

        static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();

        public IDbSet<Tag> Tags { get; set; }

        public ApplicationDbContext()
           : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public List<ApplicationUser> GetAllUsers()
        {
            return Users.Where(w => w.Roles.All(a => a.RoleId != ADMIN_ID)).Select(s => s).ToList();
        }

        public List<Group> GetAllGroups()
        {
            return Roles.Where(w => w.Id != ADMIN_ID).Select(s => new Group { Id = s.Id, Name = s.Name }).ToList();
        }


        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public static void CreateAndSeedIfNotExist()
        {

            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());

            using (var db = new ApplicationDbContext())
            {
                if (db.Database.Exists()) return;
                db.Database.CreateIfNotExists();
                var role = new IdentityRole(ADMIN)
                {
                    Id = ADMIN_ID
                };

                var ph = new PasswordHasher();

                var user = new ApplicationUser
                {
                    Email = "pavel",
                    PasswordHash = ph.HashPassword("pavel"),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    UserName = "pavel"
                };

                db.Users.Add(user);
                db.Roles.Add(role);
                role.Users.Add(new IdentityUserRole { RoleId = role.Id, UserId = user.Id });
                db.SaveChanges();
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>()
               .ToTable("Users");

            modelBuilder.Entity<IdentityRole>()
               .ToTable("Roles");

            modelBuilder.Entity<IdentityUserRole>()
               .ToTable("UserRoles");

            modelBuilder.Entity<IdentityUserClaim>()
               .ToTable("UserClaims");

            modelBuilder.Entity<IdentityUserLogin>()
               .ToTable("UserLogins");
        }

        //User
        public object UpdateUser(string id, string name, bool isAdmin, string groupId, WorkType workType)
        {
            var user = Users.FirstOrDefault(f => f.Id == id);
            if (user == null) return null;
            user.UserName = name;
            user.Email = name;
            user.WorkType = workType;
            SetRole(user, isAdmin, groupId);
            SaveChanges();
            return CreateUser(user);
        }

        private void SetRole(ApplicationUser user, bool isAdmin, string groupId)
        {
            if (user == null) return;
            if (isAdmin)
            {
                if (user.Roles.All(a => a.RoleId != ADMIN_ID)) user.Roles.Add(new IdentityUserRole { RoleId = ADMIN_ID, UserId = user.Id });
            }
            else
            {
                var role = user.Roles.FirstOrDefault(a => a.RoleId == ADMIN_ID);
                if (role != null) user.Roles.Remove(role);
            }
            foreach (var role in user.Roles.ToList())
            {
                if (role.RoleId == groupId) continue;
                user.Roles.Remove(role);
            }
            if (user.Roles.All(a => a.RoleId != groupId) && Roles.Any(a => a.Id == groupId))
                user.Roles.Add(new IdentityUserRole { RoleId = groupId, UserId = user.Id });
        }


        public object DisableUser(string id, bool disable)
        {
            var user = Users.FirstOrDefault(f => f.Id == id);
            if (user == null) return false;
            user.LockoutEnabled = disable;
            user.LockoutEndDateUtc = disable ? (DateTime?)DateTime.Now.AddYears(100) : null;
            SaveChanges();
            return CreateUser(user);
        }
        public bool DeleteUser(string id)
        {
            var user = Users.FirstOrDefault(f => f.Id == id);
            if (user == null || !user.LockoutEnabled) return false;
            user.Roles.Clear();
            Assignments.Where(w => w.UserId == user.Id).ToList().ForEach(f => f.UserId = null);
            Users.Remove(user);
            SaveChanges();
            return true;
        }

        public bool ResetPassword(string userId)
        {
            var user = Users.FirstOrDefault(f => f.Id == userId);
            if (user == null) return false;
            var ph = new PasswordHasher();
            user.PasswordHash = ph.HashPassword(user.Email);
            SaveChanges();
            return true;
        }

        //Group
        public Group CreateGroup(string groupName)
        {
            if (string.IsNullOrEmpty(groupName)) return null;
            var role = Roles.FirstOrDefault(f => f.Name == groupName);
            if (role != null) return null;
            role = new IdentityRole(groupName);
            Roles.Add(role);
            SaveChanges();
            return new Group { Id = role.Id, Name = role.Name };
        }

        public Group UpdateGroup(Group group)
        {
            if (group == null) return null;
            var role = Roles.FirstOrDefault(w => w.Id != ADMIN_ID && w.Id == group.Id && w.Name != group.Name);
            if (role == null) return null;
            role.Name = group.Name;
            SaveChanges();
            return group;
        }

        public bool DeleteGroup(string groupId)
        {
            var role = Roles.FirstOrDefault(w => w.Id != ADMIN_ID && w.Id == groupId);
            if (role == null) return false;
            Roles.Remove(role);
            SaveChanges();
            return true;
        }

        public Tag CreateTag(string code, string userId)
        {
            if (string.IsNullOrEmpty(code) || string.IsNullOrEmpty(userId)) return null;
            var tag = Tags.FirstOrDefault(f => f.Code == code);
            var user = Users.FirstOrDefault(f => f.Id == userId);
            if (tag != null || user == null) return null;
            foreach (var tg in Tags.Where(w => w.UserId == user.Id))
            {
                tg.Disabled = tg.Disabled ?? DateTime.Now;
            }
            tag = new Tag { Code = code, Created = DateTime.Now };
            user.Tags.Add(tag);
            SaveChanges();
            return tag;
        }

        public Tag UpdateTag(string code, bool enabled)
        {
            var tag = Tags.FirstOrDefault(f => f.Code == code);
            if (tag == null) return null;
            foreach (var t in Tags.Where(w => w.UserId == tag.UserId))
            {
                if (t.Code == code)
                {
                    t.Disabled = enabled ? null : (t.Disabled ?? (DateTime?)DateTime.Now);
                }
                else
                {
                    t.Disabled = t.Disabled ?? DateTime.Now;
                }
            }
            SaveChanges();
            return tag;
        }

        public static object CreateUser(ApplicationUser user)
        {
            return CreateUser(user, null);
        }

        public static object CreateUser(ApplicationUser user, List<AttendUser> attendUsers)
        {
            if (user == null) return null;
            var group = user.Roles.FirstOrDefault(f => f.RoleId != ADMIN_ID);

            var attendUser = attendUsers != null && user.PersonId != null
                ? attendUsers.FirstOrDefault(f => f.PersonId == user.PersonId && f.PersonStartDate == user.PersonStartDate)
                : null;

            return new
            {
                user.Id,
                user.UserName,
                Disabled = user.LockoutEnabled,
                DisabledDate = user.LockoutEnabled ? user.LockoutEndDateUtc?.AddYears(-100) : null,
                IsAdmin = user.Roles.Any(a => a.RoleId == ADMIN_ID),
                user.WorkType,
                GroupId = group?.RoleId,
                Attend = attendUser,
            };
        }
    }
}
