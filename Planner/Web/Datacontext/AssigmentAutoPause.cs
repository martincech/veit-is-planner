﻿using System;
using System.Collections.Generic;
using System.Linq;
using AttendDatabase;

namespace Web.Datacontext
{
    public static class AssigmentAutoPause
    {
        static readonly NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        public static void Run()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                using (var dba = new AttendContext())
                {
                    var from = DateTime.Now.AddMinutes(-2); // to prevent issue with synchronization between terminal and attendace database
                    var today = from.Date;

                    var usersAction = dba.AttendLogs
                        .Where(w => w.Date > today)
                        .OrderByDescending(o => o.Date)
                        .ToList()
                        .GroupBy(g => g.PersonId)
                        .Select(s => s.First())
                        .Where(w => w.Type != "01");
                    var userIds = usersAction.Select(s => s.PersonId);
                    var workersAction = db.Users.Where(w => userIds.Contains(w.PersonId)).ToDictionary(k => k.Id, v => usersAction.First(f => f.PersonId == v.PersonId));
                    var workersIds = workersAction.Keys;
                    var workTimes = db.WorkTimes.Where(w => w.From > today && w.To == null && workersIds.Contains(w.UserId)).ToList();
                    var paused = new List<int>();
                    foreach (var workTime in workTimes)
                    {
                        try
                        {
                            if (!workersAction.ContainsKey(workTime.UserId)) continue;
                            var date = workersAction[workTime.UserId].Date;
                            if (workTime.From > date || workTime.From > from)
                            {
                                if (workTime.From > from) logger.Info($"WorkTime (id:{workTime.Id}) is started before terminal sync!");
                                if (workTime.From > date) logger.Info($"WorkTime (id:{workTime.Id}) is started when user(id:{workTime.UserId}) is not working. Skipping pause action for this assignment!");
                                continue;
                            }
                            var upateAssignment = $"UPDATE Assignments SET StartRun=NULL, Stop=NULL WHERE Id={workTime.AssignmentId}";
                            var upateWorkTime = $"UPDATE WorkTimes SET [To]='{date.ToString("yyyy-MM-ddTHH:mm:ss")}' WHERE Id={workTime.Id}";

                            db.Database.ExecuteSqlCommand(upateAssignment);
                            db.Database.ExecuteSqlCommand(upateWorkTime);
                            paused.Add(workTime.Id);
                        }
                        catch (Exception e)
                        {
                            logger.Error(e, $"Failed to pause task {workTime.AssignmentId} for user {workTime.UserId}.");
                        }
                    }
                    if (paused.Count > 0)
                    {
                        logger.Info($"Paused {paused.Count} workTimes (IDs: {string.Join(", ", paused)}).");
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
            }
        }
    }
}
