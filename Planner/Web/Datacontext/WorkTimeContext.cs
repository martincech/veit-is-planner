﻿using System;
using System.Data.Entity;
using System.Linq;

namespace Web.Datacontext
{
    public partial class ApplicationDbContext
    {
        public IDbSet<WorkTime> WorkTimes { get; set; }

        public WorkTime StartWorking(string userId, int assignmentId)
        {
            try
            {
                var workTime = new WorkTime
                {
                    UserId = userId,
                    AssignmentId = assignmentId,
                    From = DateTime.Now
                };

                WorkTimes.Add(workTime);
                SaveChanges();
                return workTime;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void StopWorking(int assignmentId)
        {
            try
            {
                var workTimes = WorkTimes
                   .Where(f => f.AssignmentId == assignmentId && f.To == null);

                foreach (var workTime in workTimes)
                {
                    workTime.To = DateTime.Now;
                }
                SaveChanges();
            }
            catch (Exception)
            {
                return;
            }
        }

        public void DeleteWorkTime(int id)
        {
            var wTime = WorkTimes.FirstOrDefault(f => f.Id == id);
            if (wTime != null)
            {
                WorkTimes.Remove(wTime);
                SaveChanges();
            }
        }

        public WorkTime UpdateWorkTime(WorkTime workTime)
        {
            if (workTime == null) return null;
            var wTime = WorkTimes.FirstOrDefault(f => f.Id == workTime.Id);
            if (wTime == null) return null;
            wTime.From = workTime.From;
            wTime.To = workTime.To;

            var assignment = Assignments.FirstOrDefault(f => f.Id == wTime.AssignmentId);
            if (assignment != null)
            {
                var isLast = assignment.WorkTimes
                    .OrderByDescending(o => o.From)
                    .FirstOrDefault()?.Id == wTime.Id;

                if (isLast && workTime.To == null)
                {
                    assignment.StartRun = workTime.From;
                }

            }

            SaveChanges();
            return wTime;
        }
    }
}
