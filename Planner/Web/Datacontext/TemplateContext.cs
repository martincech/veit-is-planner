﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Web.Datacontext
{
    public partial class ApplicationDbContext
    {
        public IDbSet<Template> Templates { get; set; }
        public IDbSet<Block> Blocks { get; set; }
        public IDbSet<ProjectTemplate> ProjectTemplates { get; set; }

        //Template
        public Template CreateTemplate(Template template)
        {
            if (template == null || string.IsNullOrEmpty(template.Name)) return null;
            Templates.Add(template);
            SaveChanges();
            return template;
        }

        public Template UpdateTemplate(Template template)
        {
            if (template == null || string.IsNullOrEmpty(template.Name)) return null;
            var record = Templates.FirstOrDefault(f => f.Id == template.Id);
            if (record == null) return null;
            record.Description = template.Description;
            record.Name = template.Name;
            record.Code = template.Code;
            record.Duration = template.Duration;
            record.RoleId = template.RoleId;
            record.WorkType = template.WorkType;
            record.BlockId = template.BlockId;
            SaveChanges();
            return record;
        }

        public bool DeleteTemplate(int templateId)
        {
            var template = Templates.FirstOrDefault(f => f.Id == templateId);
            if (template == null) return false;
            Templates.Remove(template);
            SaveChanges();
            return true;
        }

        //Block
        public Block CreateBlock(Block block)
        {
            if (block == null || string.IsNullOrEmpty(block.Name)) return null;
            Blocks.Add(block);
            SaveChanges();
            return block;
        }

        public Block UpdateBlock(Block block)
        {
            if (block == null || string.IsNullOrEmpty(block.Name)) return null;
            var record = Blocks.FirstOrDefault(f => f.Id == block.Id);
            if (record == null) return null;
            record.Description = block.Description;
            record.Name = block.Name;
            SaveChanges();
            return record;
        }

        public bool DeleteBlock(int blockId)
        {
            var block = Blocks.FirstOrDefault(f => f.Id == blockId);
            if (block == null) return false;
            Templates.Where(w => w.BlockId == blockId).ToList().ForEach(f => f.BlockId = null);
            Blocks.Remove(block);
            SaveChanges();
            return true;
        }

        #region ProjectTemplate
        internal ProjectTemplate CreateProjectTemplate(ProjectTemplate projectTemplate)
        {
            if (projectTemplate == null) return null;
            ProjectTemplates.Add(projectTemplate);
            SaveChanges();
            return projectTemplate;
        }

        internal ProjectTemplate UpdateProjectTemplate(ProjectTemplate projectTemplate)
        {
            if (projectTemplate == null) return null;
            var pTemplate = ProjectTemplates.FirstOrDefault(f => f.Id == projectTemplate.Id);
            if (pTemplate == null) return null;
            pTemplate.Name = projectTemplate.Name;
            SaveChanges();
            return pTemplate;
        }

        internal bool DeleteProjectTemplate(int id)
        {
            var pTemplate = ProjectTemplates.FirstOrDefault(f => f.Id == id);
            if (pTemplate == null) return false;
            ProjectTemplates.Remove(pTemplate);
            SaveChanges();
            return true;
        }

        public bool AddToTemplateProject(int templateId, int projectTemplateId)
        {
            var template = Templates.FirstOrDefault(f => f.Id == templateId);
            var projectTemplate = ProjectTemplates.FirstOrDefault(f => f.Id == projectTemplateId);
            if (template == null || projectTemplate == null) return false;
            if (projectTemplate.Templates != null && projectTemplate.Templates.Any(a => a.Id == templateId)) return true;
            if (projectTemplate.Templates == null) projectTemplate.Templates = new List<Template>();
            projectTemplate.Templates.Add(template);
            SaveChanges();
            return true;
        }

        public bool RemoveFromTemplateProject(int templateId, int projectTemplateId)
        {
            var template = Templates.FirstOrDefault(f => f.Id == templateId);
            var projectTemplate = ProjectTemplates.FirstOrDefault(f => f.Id == projectTemplateId);
            if (template == null || projectTemplate == null) return false;
            if (!projectTemplate.Templates.Any(a => a.Id == templateId)) return true;
            projectTemplate.Templates.Remove(template);
            SaveChanges();
            return true;
        }

        #endregion
    }
}