﻿using System;
using System.Collections.Generic;
using System.Linq;
using AttendDatabase.DataModels;
using Nager.Date;
using Web.DTOs;

namespace Web.Map
{
    public static class MapAbsence
    {
        public static List<AbsenceDTO> Map(this IEnumerable<Absence> absences)
        {
            var abs = new List<AbsenceDTO>();
            if (absences == null || !absences.Any()) return abs;
            foreach (var item in absences)
            {
                abs.AddRange(item.Map());
            }
            return abs.OrderBy(o => o.Date).ToList();
        }

        public static List<AbsenceDTO> Map(this Absence absence)
        {
            var abs = new List<AbsenceDTO>();
            if (absence == null) return abs;
            switch (absence.LenghtType)
            {
                case 0:
                    abs.AddRange(Split(absence));
                    break;
                case 1:
                case 2:
                    var from = absence.From.Date.AddHours(absence.LenghtType == 1 ? 6.5 : 11);
                    var to = absence.To.Date.AddHours(absence.LenghtType == 1 ? 10.5 : 15);
                    abs.Add(Create(from, to, absence.LenghtType));
                    break;
                case 3:
                    abs.Add(Create(absence.TimeFrom.Value, absence.TimeTo.Value, absence.LenghtType));
                    break;
                default:
                    break;
            }
            return abs.Where(w => w != null).ToList();
        }


        static AbsenceDTO Create(DateTime from, DateTime to, short type)
        {
            if (DateSystem.IsPublicHoliday(from, CountryCode.CZ)) return null;
            return new AbsenceDTO
            {
                Date = from.Date,
                From = from,
                To = to,
                Hours = type == 0 ? 8 : (to - from).TotalHours,
                Type = type,
            };
        }

        static List<AbsenceDTO> Split(Absence absence)
        {
            var abs = new List<AbsenceDTO>();
            if (absence.From.Date == absence.To.Date)
            {
                abs.Add(AddWholeDay(absence.From, absence.LenghtType));
            }
            else
            {
                var days = (absence.To.Date.AddDays(1) - absence.From.Date).Days;
                for (var i = 0; i < days; i++)
                {
                    var day = absence.From.AddDays(i);
                    if (DateSystem.IsWeekend(day, CountryCode.CZ)) continue;
                    abs.Add(AddWholeDay(day, absence.LenghtType));
                }
            }
            return abs;
        }

        static AbsenceDTO AddWholeDay(DateTime date, short type)
        {
            return Create(date.Date.AddHours(6.5), date.Date.AddHours(15), type);
        }
    }
}
