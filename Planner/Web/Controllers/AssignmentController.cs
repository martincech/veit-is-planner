﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using AttendDatabase.DataModels;
using Microsoft.AspNet.Identity;
using Nager.Date;
using Web.Datacontext;
using Web.Extensions;
using Web.Map;

namespace Web.Controllers
{
    public class AssignmentController : BaseController
    {
        public ActionResult Index()
        {
            var path = User.IsAdmin() ? "Planner.cshtml" : "Tasks.cshtml";
            return View($"~/Views/Assignment/{path}");
        }

        public JsonResult GetBy(string userId, int month)
        {
            var assign = new List<Assignment>();
            IQueryable<WorkTime> queryWT = Context.WorkTimes;
            var workTimes = new List<WorkTime>();

            if (!User.IsAdmin()) userId = User.Identity.GetUserId();
            queryWT = queryWT.Where(w => w.UserId == userId);

            var (mFrom, mTo) = DateTime.Now.GetMonthDuration(month);
            if (month == 0)
            {
                workTimes = queryWT.Where(w => w.From >= mFrom).ToList();
                var ids = workTimes.Select(s => s.AssignmentId).Distinct();
                assign = Context.Assignments.Where(w => ((w.Stop >= mFrom || w.Stop == null) && w.UserId == userId) || ids.Contains(w.Id)).ToList();
            }
            else if (month > 0)
            {
                workTimes = queryWT.Where(w => w.From >= mFrom && w.From < mTo).ToList();
                var ids = workTimes.Select(s => s.AssignmentId).Distinct();
                assign = Context.Assignments.Where(w => (w.Stop >= mFrom && w.Stop < mTo && w.UserId == userId) || ids.Contains(w.Id)).ToList();
            }

            var user = Context.Users.FirstOrDefault(f => f.Id == userId);
            var absences = user?.PersonId != null ? GetAbsences(mFrom, mTo, user.PersonId) : new List<Absence>();
            var att = user?.PersonId != null ? GetDayAttendances(mFrom, user.PersonId) : new List<AttendDay>();

            var holiday = DateSystem.GetPublicHoliday(mFrom.Year, CountryCode.CZ).Where(w => w.Date >= mFrom && w.Date < mTo).Select(s => s.Date.Day);
            return JsonNetResult(new { Assignments = assign, WorkTimes = workTimes, Attendance = att, Absences = absences.Map(), Holiday = holiday });
        }


        public JsonResult GetLastBy(int month)
        {
            var data = new Dictionary<string, object>();
            var (mFrom, mTo) = DateTime.Now.GetMonthDuration(month);
            var currentUserId = User.Identity.GetUserId();
            var absences = GetAbsences(mFrom, mTo);
            if (month < 0)
            {
                foreach (var user in Context.Users.ToList())
                {
                    if (!User.IsAdmin() && user.Id != currentUserId) continue;
                    var userBasences = absences.Where(w => w.PersonId == user.PersonId).Map().Where(w => w.Date >= mFrom && w.Date < mTo);

                    data.Add(user.Id, new { Absences = userBasences });
                }
                return JsonNetResult(data);
            }

            var query = Context.WorkTimes.AsNoTracking();
            query = month <= 0
                ? query.Where(w => w.From >= mFrom)
                : query.Where(w => w.From >= mFrom & w.From < mTo);

            var workTimesByUser = query
                .GroupBy(g => g.UserId).ToList();

            var totalHours = new Dictionary<string, double>();
            foreach (var wt in workTimesByUser)
            {
                if (wt.Key != null)
                {
                    var sum = wt.Where(w => w.From != null && w.To != null).Sum(s => (s.To - s.From).Value.TotalMilliseconds);
                    var byDay = wt.Where(w => w.From != null && w.To != null)
                        .Where(w => ((w.From.Hour + ((double)w.From.Minute / 100)) < 10))
                        .GroupBy(g => g.From.Day);
                    sum += byDay.Count() * (15 + 10) * 60 * 1000;
                    totalHours.Add(wt.Key, sum);
                }
            }

            var workTimes = workTimesByUser.Select(s => s.OrderByDescending(o => o.Id).FirstOrDefault())
                 .ToList();

            var ids = workTimes.Where(w => w != null).Select(s => s.AssignmentId).Distinct().ToList();
            var assignments = Context.Assignments.AsNoTracking().Where(w => ids.Contains(w.Id)).ToList();
            var usersPercent = Context.UsersClosedAssignmentnsPercent(month);
            var attendance = GetMonthAttendances(mFrom);

            foreach (var user in Context.Users.ToList())
            {
                if (!User.IsAdmin() && user.Id != currentUserId) continue;
                var hours = totalHours.ContainsKey(user.Id) ? totalHours[user.Id] : 0;
                var percent = usersPercent.UserStats.ContainsKey(user.Id) ? usersPercent.UserStats[user.Id] : null;
                var workTime = workTimes.FirstOrDefault(f => f.UserId == user.Id);
                var a = attendance.FirstOrDefault(f => f.PersonId == user.PersonId);
                var userBasences = absences.Where(w => w.PersonId == user.PersonId).Map().Where(w => w.Date >= mFrom && w.Date < mTo);
                Assignment assignment = null;
                assignment = workTime != null
                    ? assignments.FirstOrDefault(f => workTime.AssignmentId == f.Id)
                    : assignments.FirstOrDefault(f => f.UserId == user.Id);
                data.Add(user.Id, new { Assignment = assignment, Hours = hours, Percent = percent, Average = usersPercent.TotalPercent, Attendance = a, Absences = userBasences });
            }
            return JsonNetResult(data);
        }

        public JsonResult GetById(int assignmentId)
        {
            var assignment = Context.Assignments.FirstOrDefault(f => f.Id == assignmentId);
            if (assignment == null) return JsonNetResult(new { });
            var project = Context.Projects.FirstOrDefault(f => f.Id == assignment.ProjectId);
            var times = Context.WorkTimes.Where(w => w.AssignmentId == assignment.Id).ToList();
            var userIds = times.Select(s => s.UserId).ToList();
            if (assignment.UserId != null && !userIds.Contains(assignment.UserId)) userIds.Add(assignment.UserId);
            var users = Context.Users.Where(w => userIds.Contains(w.Id)).Select(s => new { s.Id, s.UserName });
            var role = Context.Roles.FirstOrDefault(f => f.Id == assignment.RoleId);
            return JsonNetResult(new { Assignment = assignment, Times = times, Users = users, Project = project, Role = role });
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Get()
        {
            var users = Context.GetAllUsers().Where(w => !w.LockoutEnabled).Select(ApplicationDbContext.CreateUser).ToList();
            var assignments = Context.GetAllAssignments();
            var userAssignments = assignments.Where(w => w.UserId != null).GroupBy(o => o.UserId).ToDictionary(g => g.Key, g => g.ToList());
            var groupAssignments = assignments.Where(w => w.RoleId != null && w.UserId == null).GroupBy(o => o.RoleId).ToDictionary(g => g.Key, g => g.ToList());

            var tempaltes = Context.Templates.AsNoTracking().OrderBy(o => o.Name).ToList().GroupBy(g => g.BlockId);
            var projectTemplates = Context.ProjectTemplates.AsNoTracking().OrderBy(o => o.Name).ToList();
            var blocks = Context.Blocks.AsNoTracking().OrderBy(o => o.Name).ToList();
            blocks.Insert(0, new Block { Id = 0, Name = "Nezařazené" });
            return JsonNetResult(new
            {
                Users = users,
                Assignments = userAssignments.Union(groupAssignments).ToDictionary(pair => pair.Key, pair => pair.Value),
                Groups = Context.GetAllGroups(),
                Templates = tempaltes.ToDictionary(k => (k.Key ?? 0).ToString(), v => v),
                ProjectTemplates = projectTemplates,
                Blocks = blocks,
                Projects = Context.Projects.AsNoTracking().Where(w => w.CloseDate == null).ToList()
            });
        }

        private static object CalcTimes(ICollection<Assignment> assignments)
        {
            var expected = 0.0;
            var finished = 0.0;
            var plan = 0.0;
            var done = 0;

            foreach (var ass in assignments)
            {
                if (ass.Stop != null)
                {
                    finished += ass.Duration;
                    expected += ass.Duration;
                    done++;
                    if (ass.ExpectedDuration != 0)
                    {
                        plan += ass.Duration - ass.ExpectedDuration;
                    }
                }
                else
                {
                    var duration = ass.Duration;
                    if (ass.StartRun != null)
                    {
                        duration += (DateTime.Now - ass.StartRun.Value).TotalMilliseconds;
                    }
                    if (ass.ExpectedDuration > 0 && duration > ass.ExpectedDuration)
                    {
                        plan += duration;
                    }
                    finished += duration;
                    expected += Math.Max(duration, ass.ExpectedDuration);
                }
            }
            return new { Expected = expected, Finished = finished, Plan = plan, Done = done, assignments.Count };
        }

        public JsonResult GetByUser()
        {
            var user = Context.Users.Find(User.Identity.GetUserId());
            var assignments = Context.GetAssignmentsByUser(User.Identity.GetUserId());
            var users = Context.GetAllUsers().ToDictionary(k => k.Id, v => v.UserName);
            var running = assignments.FirstOrDefault(f => f.StartRun != null && User.Identity.GetUserId() == f.UserId);
            var projects = Context.Projects.AsNoTracking().Where(w => w.CloseDate == null).ToList();
            var thisMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            var projectsTasks = new Dictionary<int, object>();

            foreach (var project in projects)
            {
                var assgin = project.Assignments.ToList();
                if (project.View == 1) assgin = assgin.Where(w => w.Stop >= thisMonth || w.Stop == null).ToList();
                projectsTasks.Add(project.Id, new { Count = assgin.Count(), Finished = assgin.Count(c => c.Stop != null) });
            }

            var times = new List<object>();
            if (running != null && running.ProjectId != null)
            {
                var project = projects.FirstOrDefault(f => f.Id == running.ProjectId);
                var tasks = project.View == 0
                   ? project.Assignments
                   : project.Assignments.Where(w => w.Stop >= thisMonth || w.Stop == null);

                times.Add(CalcTimes(tasks.Where(w => w.WorkType == Datacontext.Enums.WorkType.Others).ToList()));
                times.Add(CalcTimes(tasks.Where(w => w.WorkType == Datacontext.Enums.WorkType.Assembly).ToList()));
                times.Add(CalcTimes(tasks.Where(w => w.WorkType == Datacontext.Enums.WorkType.Electricity).ToList()));
            }

            var today = DateTime.Now.Date;
            var logs = user.PersonId != null ? ContextAttend.AttendLogs.Where(w => w.PersonId == user.PersonId && w.Date > today).ToList() : new List<AttendLog>();
            var workTimes = Context.WorkTimes.Where(w => w.From > today && w.UserId == user.Id);

            return JsonNetResult(new { Assignments = assignments, Users = users, Projects = projects.ToList(), Times = times, ProjectsTasks = projectsTasks, Logs = logs, WorkTimes = workTimes, user.PersonId });
        }

        public JsonNetResult GetAssignments(int projectId, bool? noArchive)
        {
            var items = Context.Assignments.Where(w => w.ProjectId == projectId).ToList();
            if (noArchive.HasValue && noArchive.Value)
            {
                items = items.Where(w => !w.Archived).ToList();
            }
            return JsonNetResult(items);
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Create(Assignment assignment)
        {
            return JsonNetResult(Context.CreateAssignment(assignment));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult CreateMultiple(List<Assignment> assignments)
        {
            return JsonNetResult(Context.CreateAssignments(assignments));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Update(Assignment assignment)
        {
            return JsonNetResult(Context.UpdateAssignment(assignment));
        }

        [HttpPost]
        public JsonResult Start(Assignment assignment)
        {
            Context.StartWorking(GetAssignmentUser(assignment), assignment.Id);
            return JsonNetResult(Context.StartAssignment(assignment));
        }

        [HttpPost]
        public JsonResult Pause(Assignment assignment)
        {
            Context.StopWorking(assignment.Id);
            return JsonNetResult(Context.PauseAssignment(assignment));
        }

        [HttpPost]
        public JsonResult Stop(Assignment assignment)
        {
            if (assignment != null && assignment.StartRun != null)
                Context.StopWorking(assignment.Id);
            return JsonNetResult(Context.StopAssignment(assignment));
        }

        [HttpPost]
        public JsonResult ArchiveAssignment(Assignment assignment)
        {
            return JsonNetResult(Context.ArchiveAssignment(assignment));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Delete(Assignment assignment)
        {
            return JsonNetResult(Context.DeleteAssignment(assignment));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Swap(int id, int id2)
        {
            return JsonNetResult(Context.SwapAssignmentOrder(id, id2));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Archive()
        {
            return JsonNetResult(Context.ArchiveAssignments());
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult ArchiveByProject(int projectId)
        {
            return JsonNetResult(Context.ArchiveAssignments(projectId));
        }

        private string GetAssignmentUser(Assignment ass)
        {
            if (User.IsAdmin()) return ass?.UserId;
            return User.Identity.GetUserId();
        }

        private List<Absence> GetAbsences(DateTime from, DateTime to, string personId = null)
        {
            var query = ContextAttend.Absences.AsNoTracking().Where(w => w.To >= from && w.To < to
                    || w.From >= from && w.From < to
                    || w.From >= from && w.To < to
                    || w.From <= from && w.To > to);
            if (!string.IsNullOrEmpty(personId))
            {
                query = query.Where(w => w.PersonId == personId);
            }
            return query.ToList();
        }

        private List<AttendMonth> GetMonthAttendances(DateTime month)
        {
            return ContextAttend.AttendMonths.AsNoTracking().Where(w => w.Month == month && w.Type == AttendType.Hours).ToList();
        }

        private List<AttendDay> GetDayAttendances(DateTime month, string personId)
        {
            var monthEnd = month.AddMonths(1);
            return ContextAttend.AttendDays.AsNoTracking().Where(w => w.Day >= month && w.Day < monthEnd && w.Type == AttendType.Hours && w.PersonId == personId).ToList();
        }
    }
}
