﻿using System.Linq;
using System.Web.Mvc;
using Web.Datacontext;

namespace Web.Controllers
{
    public class GroupController : AdminBaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Get()
        {

            var data = new
            {
                Groups = Context.GetAllGroups(),
                Users = Context.GetAllUsers().Select(ApplicationDbContext.CreateUser)
            };

            return JsonNetResult(data);
        }

        public JsonResult Create(string name)
        {
            return JsonNetResult(Context.CreateGroup(name));
        }

        public JsonResult Update(Group group)
        {
            return JsonNetResult(Context.UpdateGroup(group));
        }

        public JsonResult Delete(string groupId)
        {
            return JsonNetResult(Context.DeleteGroup(groupId));
        }
    }
}
