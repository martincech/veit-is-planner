﻿using System.Net;
using System.Web.Mvc;
using Web.Datacontext;

namespace Web.Controllers
{
    public class WorkTimeController : AdminBaseController
    {
        [HttpPost]
        public JsonResult Update(WorkTime workTime)
        {
            return JsonNetResult(Context.UpdateWorkTime(workTime));
        }

        [HttpPost]
        public JsonNetResult UpdateDuration(Assignment assignment)
        {
            return JsonNetResult(Context.UpdateAssignmentTime(assignment));
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Context.DeleteWorkTime(id);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
    }
}
