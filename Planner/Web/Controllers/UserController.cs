﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AttendDatabase.DataModels;
using Microsoft.AspNet.Identity;
using Web.Datacontext;
using Web.Datacontext.Enums;
using Web.Extensions;

namespace Web.Controllers
{
    public class UserController : BaseController
    {
        const string AttendUserCommand =
                 "SELECT [dbo].[Osoba].Jmeno as Name, [dbo].[Osoba].Prijmeni as Surename, " +
                 "[dbo].[PracovniPomer].OsobniCislo as PersonId, [dbo].[PracovniPomer].DatumNastupu as PersonStartDate " +
                 "FROM [dbo].[Osoba], [dbo].[PracovniPomer] " +
                 "WHERE [dbo].[PracovniPomer].DatumUkonceni IS NULL AND [dbo].[Osoba].RC = [dbo].[PracovniPomer].RC";

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Get()
        {
            var userId = User.Identity.GetUserId();
            var users = User.IsAdmin() ? Context.Users.Where(w => w.Id != userId).ToList() : Context.Users.Where(w => w.Id == userId).ToList();
            var groups = Context.GetAllGroups();
            var tags = Context.Tags.ToList();
            var attendUsers = ContextAttend.Database.SqlQuery<AttendUser>(AttendUserCommand).ToList();
            return JsonNetResult(new { Users = users.Select(s => ApplicationDbContext.CreateUser(s, attendUsers)), Groups = groups, Tags = tags });
        }

        public JsonResult GetById(string userId)
        {
            ApplicationUser user = null;
            if (User.IsAdmin())
            {
                user = Context.Users.FirstOrDefault(f => f.Id == userId);
            }
            else
            {
                var currentUserId = User.Identity.GetUserId();
                user = Context.Users.FirstOrDefault(f => f.Id == currentUserId);
            }
            var role = UserManager.GetRoles(user?.Id).FirstOrDefault();
            return JsonNetResult(new { User = ApplicationDbContext.CreateUser(user), Group = new { Name = role } });
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult FindByName(string surename)
        {
            if (string.IsNullOrEmpty(surename)) return JsonNetResult(new List<AttendUser>());
            var command = AttendUserCommand + $" AND LTRIM(RTRIM([dbo].[Osoba].Prijmeni)) LIKE '{surename.Trim()}' COLLATE Latin1_General_CI_AI";
            var attendUsers = ContextAttend.Database.SqlQuery<AttendUser>(command).ToList();
            return JsonNetResult(attendUsers);
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult UpdateAttendMap(string userId, AttendUser attendUser)
        {
            var user = Context.Users.FirstOrDefault(f => f.Id == userId);
            if (user != null)
            {
                user.PersonId = attendUser?.PersonId;
                user.PersonStartDate = user.PersonId == null ? null : attendUser?.PersonStartDate;
                Context.SaveChanges();
                return JsonNetResult(attendUser);
            }
            return JsonNetResult(null);
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Create(string userName, bool isAdmin, string groupId)
        {
            if (string.IsNullOrEmpty(userName) || userName.Length < 4) return null;
            var user = new ApplicationUser { UserName = userName, Email = userName };
            var result = UserManager.Create(user, userName);
            if (isAdmin) UserManager.AddToRole(user.Id, ApplicationDbContext.ADMIN);
            var group = Context.GetAllGroups().FirstOrDefault(f => f.Id == groupId);
            if (group != null) UserManager.AddToRole(user.Id, group.Name);
            if (!result.Succeeded) return JsonNetResult(null);
            return JsonNetResult(ApplicationDbContext.CreateUser(user));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Update(string id, string userName, bool isAdmin, string groupId, WorkType workType)
        {
            return JsonNetResult(Context.UpdateUser(id, userName, isAdmin, groupId, workType));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Reset(string userId)
        {
            return JsonNetResult(Context.ResetPassword(userId));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Disable(string userId, bool disable)
        {
            return JsonNetResult(Context.DisableUser(userId, disable));
        }

        [HttpPost]
        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Delete(string userId)
        {
            return JsonNetResult(Context.DeleteUser(userId));
        }
    }
}
