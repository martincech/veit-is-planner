﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Web.Datacontext;

namespace Web.Controllers
{
    public class TemplateController : AdminBaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Get()
        {
            var tempaltes = Context.Templates.OrderBy(o => o.Name).ToList().GroupBy(g => g.BlockId);
            var blocks = Context.Blocks.OrderBy(o => o.Name).ToList();
            blocks.Insert(0, new Block { Id = 0, Name = "Nezařazené" });
            var data = new
            {
                Templates = tempaltes.ToDictionary(k => (k.Key ?? 0).ToString(), v => v),
                Groups = Context.GetAllGroups().OrderBy(o => o.Name).ToList(),
                Blocks = blocks,
                ProjectTemplates = Context.ProjectTemplates.OrderBy(o => o.Name).ToList(),
                TemplatesMap = Context.ProjectTemplates.ToList().ToDictionary(d => d.Id, v => v.Templates?.Select(s => s.Id) ?? new List<int>())
            };

            return JsonNetResult(data);
        }

        public JsonResult Create(Template template)
        {
            return JsonNetResult(Context.CreateTemplate(template));
        }
        public JsonResult Update(Template template)
        {
            return JsonNetResult(Context.UpdateTemplate(template));
        }
        public JsonResult Delete(int id)
        {
            return JsonNetResult(Context.DeleteTemplate(id));
        }
    }
}
