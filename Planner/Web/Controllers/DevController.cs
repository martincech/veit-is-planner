﻿using System.Web.Mvc;

namespace Web.Controllers
{
    public class DevController : Controller
    {
        public ActionResult Index()
        {
            return File("~/Content/dist/index.html", "text/html");
        }
    }
}
