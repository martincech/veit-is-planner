﻿using System;
using System.Web;
using System.Web.Mvc;
using AttendDatabase;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using Web.Datacontext;

namespace Web.Controllers
{
    [Authorize]
    public class BaseController : Controller
    {
        private ApplicationUserManager _userManager;

        private ApplicationDbContext context;
        protected ApplicationDbContext Context { get { return context ?? (context = new ApplicationDbContext()); } }

        private AttendContext contextAttend;
        protected AttendContext ContextAttend { get { return contextAttend ?? (contextAttend = new AttendContext()); } }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected static JsonNetResult JsonNetResult(object data, JsonRequestBehavior behavior = JsonRequestBehavior.AllowGet)
        {
            return new JsonNetResult
            {
                Data = data,
                JsonRequestBehavior = behavior
            };
        }
    }

    [Authorize(Roles = ApplicationDbContext.ADMIN)]
    public class AdminBaseController : BaseController
    {

    }

    public class JsonNetResult : JsonResult
    {
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var response = context.HttpContext.Response;

            response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data == null)
                return;

            // If you need special handling, you can call another form of SerializeObject below
            var serializedObject = JsonConvert.SerializeObject(Data, Formatting.Indented);
            response.Write(serializedObject);
        }
    }
}
