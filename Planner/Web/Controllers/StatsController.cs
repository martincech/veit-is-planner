﻿using System;
using System.Collections.Generic;
using System.Linq;
using Web.Datacontext;

namespace Web.Controllers
{
    public class StatsController : AdminBaseController
    {
        public JsonNetResult Workers(int months)
        {
            var users = Context.Users.Where(w => w.Roles.All(a => a.RoleId != ApplicationDbContext.ADMIN_ID)).ToList();
            var from = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(-months);
            var to = from.AddMonths(1);

            var data = new Dictionary<string, object>();

            foreach (var user in users)
            {
                var tasks = months == 0
                    ? Context.Assignments.Where(w => w.UserId == user.Id).ToList().Where(w => (w.Stop.HasValue && w.Stop.Value > from) || (w.Start.HasValue && !w.Stop.HasValue)).ToList()
                    : Context.Assignments.Where(w => w.UserId == user.Id).ToList().Where(w => w.Stop > from && w.Stop < to).ToList();

                double done = 0;
                double plan = 0;
                Assignment active = null;
                foreach (var task in tasks)
                {

                    if (task.Stop.HasValue && task.ExpectedDuration != 0)
                    {
                        plan += task.Duration - task.ExpectedDuration;
                        done += task.Duration;
                    }
                    else
                    {
                        if (task.StartRun.HasValue)
                        {
                            done += (DateTime.Now - task.StartRun.Value).TotalMilliseconds;
                            active = task;
                        }
                        else
                        {
                            done += task.Duration;
                        }
                    }
                }

                var doneTime = TimeSpan.FromMilliseconds(done);
                var planTime = TimeSpan.FromMilliseconds(Math.Abs(plan));

                data.Add(user.Id, new
                {
                    plan = new
                    {
                        duration = plan,
                        hours = Math.Floor(planTime.TotalHours),
                        minutes = planTime.Minutes
                    },
                    done = new
                    {
                        hours = Math.Floor(doneTime.TotalHours),
                        minutes = doneTime.Minutes
                    },
                    active
                });
            }

            return JsonNetResult(data);
        }
    }
}
