﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Web.Datacontext;

namespace Web.Controllers
{
    public class ProjectTemplateController : AdminBaseController
    {
        // GET api/<controller>
        public JsonResult Get()
        {
            return JsonNetResult(Context.ProjectTemplates.ToList());
        }

        // GET api/<controller>/5
        public JsonResult Get(int id)
        {
            return JsonNetResult(Context.ProjectTemplates.FirstOrDefault(f => f.Id == id));
        }

        public JsonResult Create(ProjectTemplate projectTemplate)
        {
            return JsonNetResult(Context.CreateProjectTemplate(projectTemplate));
        }

        public JsonResult Update(ProjectTemplate projectTemplate)
        {
            return JsonNetResult(Context.UpdateProjectTemplate(projectTemplate));
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Context.DeleteProjectTemplate(id);
        }

        public bool AddTemplate(int templateId, int projectTemplateId)
        {
            return Context.AddToTemplateProject(templateId, projectTemplateId);
        }

        public bool RemoveTemplate(int templateId, int projectTemplateId)
        {
            return Context.RemoveFromTemplateProject(templateId, projectTemplateId);
        }
    }
}