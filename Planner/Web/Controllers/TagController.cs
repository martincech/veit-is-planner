﻿using System.Web.Mvc;

namespace Web.Controllers
{
    public class TagController : AdminBaseController
    {
        [HttpPost]
        public JsonResult Create(string code, string userId)
        {
            return JsonNetResult(Context.CreateTag(code, userId));
        }

        [HttpPost]
        public JsonResult Update(string code, bool enabled)
        {
            return JsonNetResult(Context.UpdateTag(code, enabled));
        }
    }
}
