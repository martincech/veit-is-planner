﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Web.Datacontext;
using Web.Datacontext.Enums;

namespace Web.Controllers
{
    public class ProjectController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
            return View("~/Views/Project/Overview.cshtml");
        }

        public JsonNetResult Get()
        {
            var data = new
            {
                Users = Context.GetAllUsers().Select(ApplicationDbContext.CreateUser),
                Groups = Context.GetAllGroups(),
                Projects = Context.Projects.ToList()
            };
            return JsonNetResult(data);
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Create(Project project)
        {
            return JsonNetResult(Context.CreateProject(project));
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonResult Update(Project project)
        {
            return JsonNetResult(Context.UpdateProject(project));
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            Context.DeleteProject(id);
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public bool Close(int projectId)
        {
            return Context.CloseProject(projectId);
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public bool Open(int projectId)
        {
            return Context.OpenProject(projectId);
        }

        public JsonNetResult ChangeView(int projectId, ProjectViewType view, int months)
        {
            return JsonNetResult(Context.ChangeProjectView(projectId, view, months));
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonNetResult ChangeWorkers(int projectId, WorkType workType, int workers)
        {
            return JsonNetResult(Context.ChangeProjectWorkers(projectId, workType, workers));
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonNetResult ChangePlanned(int projectId, bool planned)
        {
            return JsonNetResult(Context.ChangeProjectPlanned(projectId, planned));
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonNetResult Find(string projectName)
        {
            if (string.IsNullOrEmpty(projectName) || projectName.Length < 3) return JsonNetResult(new object[0]);
            var assignments = Context.Assignments.ToList();
            return JsonNetResult(assignments.Where(w => w.Description != null && w.Description.Contains(projectName)));
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public void SetTimes()
        {
            var templates = Context.Templates.Where(w => w.Duration != 0).GroupBy(g => g.Name).ToList().Select(s => s.First()).ToDictionary(k => k.Name, v => v.Duration);
            var tasks = Context.Assignments.Where(w => w.ExpectedDuration == 0 && w.Name != null);
            foreach (var t in tasks)
            {
                if (templates.ContainsKey(t.Name))
                {
                    t.ExpectedDuration = templates[t.Name];
                }
            }
            Context.SaveChanges();
        }

        [Authorize(Roles = ApplicationDbContext.ADMIN)]
        public JsonNetResult AddToProject(int projectId, List<int> assignmentIds)
        {
            return JsonNetResult(Context.AddToProject(projectId, assignmentIds));
        }
    }
}
