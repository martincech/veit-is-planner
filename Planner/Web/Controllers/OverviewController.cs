﻿using System.Web.Mvc;

namespace Web.Controllers
{
    public class OverviewController : BaseController
    {
        public ActionResult Index()
        {
            return File("~/Content/dist/index.html", "text/html");
        }
    }
}
