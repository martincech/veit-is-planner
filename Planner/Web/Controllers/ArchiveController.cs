﻿using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Web.Datacontext;

namespace Web.Controllers
{
    public class ArchiveController : AdminBaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult Get()
        {
            var assignments = Context.GetArchiveAssignments();
            var groups = Context.GetAllGroups();
            var users = Context.GetAllUsers().Select(ApplicationDbContext.CreateUser);
            var projects = Context.Projects.AsNoTracking().ToList();
            return JsonNetResult(new { Assignments = assignments, Groups = groups, Users = users, Projects = projects });
        }
    }
}
