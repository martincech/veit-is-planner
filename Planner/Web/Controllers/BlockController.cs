﻿using System.Web.Mvc;
using Web.Datacontext;

namespace Web.Controllers
{
    public class BlockController : AdminBaseController
    {
        public JsonResult Create(Block block)
        {
            return JsonNetResult(Context.CreateBlock(block));
        }
        public JsonResult Update(Block block)
        {
            return JsonNetResult(Context.UpdateBlock(block));
        }
        public JsonResult Delete(int id)
        {
            return JsonNetResult(Context.DeleteBlock(id));
        }
    }
}
