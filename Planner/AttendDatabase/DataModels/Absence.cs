﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AttendDatabase.DataModels
{
    [Table(nameof(Absence))]
    public class Absence : AttendIdentity
    {
        [Key, Column("DatumOd", Order = 2)]
        public DateTime From { get; set; }
        [Column("DatumDo")]
        public DateTime To { get; set; }
        [Column("CasOd")]
        public DateTime? TimeFrom { get; set; }
        [Column("CasDo")]
        public DateTime? TimeTo { get; set; }
        [Column("Delka")]
        public short LenghtType { get; set; }
    }
}
