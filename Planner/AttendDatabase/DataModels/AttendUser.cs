﻿using System.ComponentModel.DataAnnotations.Schema;

namespace AttendDatabase.DataModels
{
    public class AttendUser : AttendIdentity
    {
        [Column("Jmeno")]
        public string Name { get; set; }
        [Column("Prijmeni")]
        public string Surename { get; set; }
    }
}
