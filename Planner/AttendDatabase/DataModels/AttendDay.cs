﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AttendDatabase.DataModels
{
    [Table("DenniVysledekVypoctu")]
    public class AttendDay : AttendIdentity
    {
        [Key, Column("Den", Order = 2)]
        public DateTime Day { get; set; }
        [Key, Column("TypCasoveSlozkyMzdy", Order = 3)]
        public string Type { get; set; }
        [Column("Hodiny")]
        public double Hours { get; set; }
    }
}
