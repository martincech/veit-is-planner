﻿namespace AttendDatabase.DataModels
{
    public static class AttendLogType
    {
        public const string Entry = "01";
        public const string Break = "02";
        public const string Departure = "03";
    }
}
