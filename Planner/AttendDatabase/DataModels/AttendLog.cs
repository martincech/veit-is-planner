﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AttendDatabase.DataModels
{
    [Table("PuvodniDochazka")]
    public class AttendLog : AttendIdentity
    {
        [Key, Column("DatumCasOperace", Order = 2)]
        public DateTime Date { get; set; }
        [Key, Column("KodOperace", Order = 3)]
        public string Type { get; set; }
    }
}
