﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AttendDatabase.DataModels
{
    public abstract class AttendIdentity
    {
        [Key, Column("OsobniCislo", Order = 0)]
        public string PersonId { get; set; }
        [Key, Column("DatumNastupu", Order = 1)]
        public DateTime PersonStartDate { get; set; }
    }
}
