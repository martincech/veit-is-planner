﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AttendDatabase.DataModels
{
    [Table("MesicniVysledekVypoctu")]
    public class AttendMonth : AttendIdentity
    {
        [Key, Column("Mesic", Order = 2)]
        public DateTime Month { get; set; }
        [Key, Column("TypCasoveSlozkyMzdy", Order = 3)]
        public string Type { get; set; }
        [Column("Hodiny")]
        public double Hours { get; set; }
    }
}
