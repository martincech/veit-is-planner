﻿using System.Data.Entity;
using AttendDatabase.DataModels;

namespace AttendDatabase
{
    public class AttendContext : DbContext
    {
        public IDbSet<Absence> Absences { get; set; }
        public IDbSet<AttendMonth> AttendMonths { get; set; }
        public IDbSet<AttendDay> AttendDays { get; set; }
        public IDbSet<AttendLog> AttendLogs { get; set; }


        public AttendContext() : base("AttendConnection")
        {

        }
    }
}
