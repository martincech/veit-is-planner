import { FETCH_USERS, FETCH_CURRENT_USER } from './actions/users.actions';
import { FETCH_ASSIGNMENTS, FETCH_ASSIGNMENT, FETCH_LAST_ASSIGNMENTS } from './actions/assignments.actions';
import { FETCH_PROJECTS } from './actions/projects.actions';
import { UPDATE_WORK_TIME, DELETE_WORK_TIME, UPDATE_DURATION } from './actions/workTimes.actions';

import {
  CLEAR_FILTER, SET_FILTER_TEXT, SET_FILTER_GROUP, SET_FILTER_WORK_TYPE,
} from './actions/filter.actions';

const defaultState = {
  projects: [],
  users: [],
  groups: [],
  assignments: [],
  workTimes: [],
  month: 0,
  searchValue: '',
  filter: {},
  isAdmin: false,
};

const rootReducer = (state = defaultState, action) => {
  switch (action.type) {
    case '@@router/LOCATION_CHANGE':
      return Object.assign({}, state, { searchValue: '' });
    case FETCH_ASSIGNMENT:
    case FETCH_PROJECTS:
    case FETCH_ASSIGNMENTS:
    case FETCH_USERS:
    case FETCH_CURRENT_USER:
    case 'SET_MONTH':
    case 'RESET':
    case 'SET_SEARCH':
      return Object.assign({}, state, action.payload);
    case CLEAR_FILTER:
      return Object.assign({}, state, { filter: {} });
    case SET_FILTER_TEXT:
      return Object.assign({}, state, { filter: { ...state.filter, text: action.payload } });
    case SET_FILTER_GROUP:
      return Object.assign({}, state, { filter: { ...state.filter, groupId: action.payload } });
    case SET_FILTER_WORK_TYPE:
      return Object.assign({}, state, { filter: { ...state.filter, workType: action.payload } });
    case FETCH_LAST_ASSIGNMENTS:
      return Object.assign({}, state, { usersStats: action.payload });
    case UPDATE_WORK_TIME: {
      const index = state.times.findIndex(f => f.Id === action.payload.Id);
      return Object.assign({}, state, {
        times: [
          ...state.times.slice(0, index),
          {
            ...action.payload,
          },
          ...state.times.slice(index + 1),
        ],
      });
    }
    case DELETE_WORK_TIME: {
      const times = state.times.filter(f => f.Id !== action.payload);
      return Object.assign({}, state, { times });
    }
    case UPDATE_DURATION:
      return Object.assign({}, state, { assignment: action.payload });
    case 'MODAL_ASSIGNMENT':
      return Object.assign({}, state, { modal: action.payload });
    case 'ASSIGNMENT_EDIT':
    case 'ASSIGNMENT_STOP':
    case 'ASSIGNMENT_PAUSE':
    case 'ASSIGNMENT_START':
    case 'ASSIGNMENT_ARCHIVE':
      return Object.assign({}, state, { assignment: action.payload });
    case 'SET_LOADER':
      return Object.assign({}, state, { isFetching: action.payload });
    default:
      return state;
  }
};

export default rootReducer;
