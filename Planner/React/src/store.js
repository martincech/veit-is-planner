/* eslint no-underscore-dangle: ["error", { "allow": ["__REDUX_DEVTOOLS_EXTENSION__"] }] */
import {
  applyMiddleware, compose, createStore, combineReducers,
} from 'redux';
import thunk from 'redux-thunk';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import logger from 'redux-logger';

import rootReducer from './rootReducer';
import { modalReducer } from './actions/modal.actions';


export default function configureStore(history, initialState) {
  const middleware = [
    routerMiddleware(history),
    thunk,
  ];

  // In development, use the browser's Redux dev tools extension if installed
  const enhancers = [];
  const isDevelopment = process.env.REACT_APP_ENV === 'development';
  if (isDevelopment) {
    middleware.push(logger);
    if (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__) {
      enhancers.push(window.__REDUX_DEVTOOLS_EXTENSION__());
    }
  }

  const reducers = combineReducers({
    router: connectRouter(history),
    root: rootReducer,
    modal: modalReducer,
  });

  return createStore(
    reducers,
    initialState,
    compose(applyMiddleware(...middleware), ...enhancers),
  );
}
