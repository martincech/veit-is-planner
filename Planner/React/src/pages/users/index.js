import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Table, Alert } from 'reactstrap';

import { getAllUsers } from '../../actions/users.actions';
import { getLastAssignments } from '../../actions/assignments.actions';
import Page from '../../components/Page';

import TableRow from './TableRow';
import Submenu from './Submenu';

const AverageValue = ({ average }) => {
  const avg = average != null && average !== 0 ? average - 100 : 0;
  const value = avg === 0 ? avg : avg >= 0 ? `+ ${avg}` : `- ${Math.abs(avg)}`;
  return (
    <Alert color={avg > 0 ? 'danger' : 'success'} style={{ alignSelf: 'flex-start' }}>
      {`Průměrná úspěšnost : ${value} %`}
    </Alert>
  );
};

class Users extends Component {
  componentDidMount() {
    this.props.getAllUsers();
    this.fetchData();
  }

  componentDidUpdate() {
    if (this.props.month !== this.state.month) {
      this.fetchData();
    }
  }

  fetchData = () => {
    this.props.getLastAssignments({ month: this.props.month });
    this.setState({ month: this.props.month });
  }

  filter = (f) => {
    if (f.IsAdmin) return false;

    const date = new Date();
    date.setDate(1);
    date.setHours(1);
    date.setMonth(date.getMonth() - this.props.month);
    if (f.Disabled && new Date(f.DisabledDate) < date) return false;
    let result = true;

    if (this.props.searchValue != null && this.props.searchValue.length > 0) {
      const search = this.props.searchValue.toLowerCase();
      result = f.UserName.toLowerCase().indexOf(search) !== -1;
    }

    if (this.props.filter.groupId) {
      result = result && f.GroupId === this.props.filter.groupId;
    }

    if (this.props.filter.workType) {
      result = result && f.WorkType === this.props.filter.workType;
    }

    return result;
  }

  orderBy = (a, b) => {
    const property = 'UserName';
    if (a[property] < b[property]) { return -1; }
    if (a[property] > b[property]) { return 1; }
    return 0;
  }

  render() {
    const { usersStats } = this.props;
    const average = usersStats != null && Object.keys(usersStats).length !== 0 ? usersStats[Object.keys(usersStats)[0]].Average : null;
    return (
      <Page
        title="Uživatelé"
        breadcrumbs={[{ name: 'Uživatelé', active: true }]}
        submenu={<Submenu isAdmin={this.props.isAdmin} />}
        middleTitle={<AverageValue average={average} />}
      >
        {
          usersStats
          && (
            <Table striped style={{ tableLayout: 'fixed' }}>
              <thead>
                <tr>
                  <th style={{ width: '170px' }}>Jméno</th>
                  <th>Poslední úkol</th>
                  <th style={{ width: '80px' }}></th>
                  <th style={{ width: '105px' }}>Docházka</th>
                  <th style={{ width: '145px' }}>Odpracováno</th>
                  <th style={{ width: '110px' }}>Úspěšnost</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.props.users.filter(this.filter).sort(this.orderBy).map(u => (
                    <TableRow key={u.Id} user={u} stats={this.props.usersStats[u.Id]} />
                  ))
                }
              </tbody>
            </Table>
          )
        }
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    users: state.root.users,
    isAdmin: state.root.isAdmin,
    groups: state.root.groups,
    usersStats: state.root.usersStats,
    month: state.root.month,
    searchValue: state.root.searchValue,
    filter: state.root.filter,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    getAllUsers: bindActionCreators(getAllUsers, dispatch),
    getLastAssignments: bindActionCreators(getLastAssignments, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);
