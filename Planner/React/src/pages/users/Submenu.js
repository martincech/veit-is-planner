import React from 'react';

import MonthSelector from '../../components/Veit/MonthSelector';
import WorkerTypeSelector from '../../components/Veit/WorkerTypeSelector';
import GroupSelector from '../../components/Veit/GroupSelector';

const Submenu = ({ isAdmin }) => {
  return (
    <div style={{ display: 'flex' }}>
      {isAdmin && (
        <React.Fragment>
          <div style={{ paddingRight: '10px' }}>
            {' '}
            <GroupSelector />
          </div>
          <div style={{ paddingRight: '10px' }}>
            <WorkerTypeSelector />
          </div>
        </React.Fragment>
      )}
      <div>
        {' '}
        <MonthSelector />
        {' '}
      </div>
    </div>
  );
};

export default Submenu;
