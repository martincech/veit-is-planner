import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';

import { TimeDuration, SignedTimeDuration } from '../../components/Veit/DateAndTime';
import WorkerTypeIcon from '../../components/Veit/WorkerTypeIcon';
import AssignmentStatusIcon from '../../components/Veit/AssignmentStatusIcon';
import assignmentsUtils, { getColorByPercent } from '../../utils/assignment';
import { UncontrolledTooltip } from 'reactstrap';

const AssignmentInfo = ({ assignment }) => {
  if (assignment == null) return null;
  return (
    <React.Fragment>
      <AssignmentStatusIcon assignment={assignment} />
      <Link to={`/task/${assignment.Id}`}>
        {` ${assignment.BlockName ? `${assignment.BlockName} | ` : ''}${assignment.Name ? `${assignment.Name} | ` : ''}${assignment.Description || ''}`}
      </Link>
    </React.Fragment>
  );
};

const PercentCol = ({
  percent, base = 100, correction = 0, isUserAverage,
}) => {
  if (percent == null) return <td></td>;
  const per = percent + correction;
  const color = getColorByPercent(per - base + 100, isUserAverage ? -10 : 0);

  return correction === 0
    ? (
      <td
        style={{ backgroundColor: color, textAlign: 'center' }}
      >
        {per > base ? '+' : ''}
        {`${per - base}%`}
      </td>
    ) : (
      <td
        style={{ backgroundColor: color, textAlign: 'center' }}
        data-tooltip
        title={correction ? `Korekce z docházky : ${correction >= 0 ? '+' : '-'} ${Math.abs(correction)} %` : ''}
      >
        {per > base ? '+' : ''}
        {`${per - base}%`}
      </td>
    );
};

const AbsenceType = ({ absence }) => {
  if (absence == null) return null;
  switch (absence.Type) {
    case 0: return ' - Celodenní';
    case 1: return ' - Ranní';
    case 2: return ' - Odpolední';
    case 3: return ` - Od ${moment(absence.From).format('HH:mm')} do ${moment(absence.To).format('HH:mm')}`;
    default: return null;
  }
};

const TableRow = ({ user, stats: statsData }) => {
  if (user == null) return null;
  const stats = statsData || {};
  const assignment = stats.Assignment;
  const time = assignment == null ? null : assignmentsUtils.getTime(assignment);

  let attendancePercent = 0;
  if (stats.Attendance) {
    const attHours = stats.Attendance.Hours * 60 * 60 * 1000;
    attendancePercent = Math.floor(100 - (stats.Hours / attHours) * 100);
  }

  const hasAbsences = stats.Absences && stats.Absences.length !== 0;
  const hasAbsenceToday = hasAbsences ? stats.Absences.filter(f => moment(f.Date).isSame(moment(), 'day')).length !== 0 : false;

  return (
    <tr>
      <td>
        <WorkerTypeIcon type={user.WorkType} style={{ color: hasAbsences ? hasAbsenceToday ? 'gold' : 'greenyellow' : null }} className="icon-rounded" id={`user_${user.Id}`} />
        {
          hasAbsences && (
            <UncontrolledTooltip target={`user_${user.Id}`} placement="right">
              <table>
                <tr><td colSpan="2">Absence</td></tr>
                {
                  stats.Absences.map(m => (
                    <tr>
                      <td style={{ textAlign: 'left' }}>{moment(m.Date).format('D. dddd')}</td>
                      <td><AbsenceType absence={m} /></td>
                    </tr>
                  ))
                }
              </table>
            </UncontrolledTooltip>
          )
        }

        {' '}
        <Link to={`/user/${user.Id}`}>
          {' '}
          {user.UserName}
          {' '}
        </Link>
      </td>
      <td style={{ overflowX: 'hidden', whiteSpace: 'nowrap' }}>
        <AssignmentInfo assignment={assignment} />
      </td>
      <PercentCol percent={time ? time.percent : null} />
      <td style={{ padding: '0 0.75em', whiteSpace: 'nowrap' }}>
        <div>
          {stats.Attendance && (
            <div>
              <div><TimeDuration duration={stats.Attendance.Hours * 60 * 60 * 1000} /></div>
              <div><SignedTimeDuration duration={stats.Hours - stats.Attendance.Hours * 60 * 60 * 1000} /></div>
            </div>
          )}
        </div>
      </td>
      <td>
        <TimeDuration duration={stats.Hours} />
      </td>
      <PercentCol isUserAverage percent={stats.Percent} base={stats.Average} correction={attendancePercent < 0 ? 0 : attendancePercent} />
    </tr>
  );
};

export default TableRow;
