import React, { Component } from 'react';
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

import { getAllProjects } from '../../actions/projects.actions';

class Projects extends Component {
  componentDidMount() {
    this.props.getAllProjects();
  }

  render() {
    return (
      <div>
        {this.props.projects && this.props.projects.map(project => (
          <div>
            <Link to={`/project/${project.Id}`}>
              {' '}
              {project.Name}
              {' '}
            </Link>
          </div>
        ))}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    projects: state.root.projects,
    users: state.root.users,
    groups: state.root.groups,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reset: () => { dispatch({ type: 'RESET' }); },
    getAllProjects: bindActionCreators(getAllProjects, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Projects);
