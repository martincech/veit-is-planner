import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { Button } from 'reactstrap';

import { editWorkTimeModal } from '../../actions/modal.actions';
import DateAndTime, { TimeDiff } from '../../components/Veit/DateAndTime';

const WorkTimesTableRow = ({
  time, isAdmin, userName, isRunning, editWorkTimeModal,
}) => {
  return (
    <tr key={time.Id}>
      <td>
        {' '}
        <Link to={`/user/${time.UserId}`}>
          {` ${userName} `}
        </Link>
      </td>
      <td>
        {' '}
        <DateAndTime date={time.From} />
        {' '}
      </td>
      <td>
        {' '}
        <DateAndTime date={time.To} />
      </td>
      <td><TimeDiff from={time.From} to={isRunning != null && time.To == null ? new Date() : time.To} /></td>
      <td>
        {
          isAdmin && (
            <Button onClick={() => editWorkTimeModal(time)}>
              Změnit čas
            </Button>
          )
        }
      </td>
    </tr>
  );
};

function mapDispatchToProps(dispatch) {
  return {
    editWorkTimeModal: bindActionCreators(editWorkTimeModal, dispatch),
  };
}

export default connect(
  null,
  mapDispatchToProps,
)(WorkTimesTableRow);
