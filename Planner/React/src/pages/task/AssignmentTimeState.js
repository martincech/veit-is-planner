import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FaPlay from 'react-icons/lib/fa/play';
import FaStop from 'react-icons/lib/fa/stop';
import FaArchive from 'react-icons/lib/fa/archive';
import FaPause from 'react-icons/lib/fa/pause';
import FaEdit from 'react-icons/lib/fa/edit';
import {
  UncontrolledButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle, Button,
} from 'reactstrap';
import AssignmentStatusIcon from '../../components/Veit/AssignmentStatusIcon';

import {
  startAssignment,
  stopAssignment,
  pauseAssignment,
  archiveAssignment,
} from '../../actions/assignments.actions';
import { editAssignmentModal } from '../../actions/modal.actions';

const AssignmentTimeState = ({
  assignment, isAdmin, start, stop, pause, editAssignmentModal, archive,
}) => {
  if (assignment == null) return null;
  return (
    <div style={{ display: 'flex' }}>
      {
        isAdmin && (
          <Button onClick={() => editAssignmentModal()}>
            <FaEdit />
            {' Upravit'}
          </Button>
        )
      }
      <UncontrolledButtonDropdown>
        <DropdownToggle disabled={!isAdmin} style={{ borderRadius: '.25rem', marginLeft: '15px' }} caret={false} color="primary">
          <AssignmentStatusIcon assignment={assignment} />
        </DropdownToggle>
        <DropdownMenu>
          {assignment.StartRun == null && (
            <DropdownItem disabled={assignment.UserId == null} onClick={() => start(assignment)}>
              <FaPlay />
              {' Spustit'}
            </DropdownItem>
          )}
          {(assignment.StartRun != null || assignment.Stop != null) && (
            <DropdownItem onClick={() => pause(assignment)}>
              <FaPause />
              {' Zastavit'}
            </DropdownItem>
          )}
          {assignment.Stop == null && (
            <DropdownItem onClick={() => stop(assignment)}>
              <FaStop />
              {' Ukočit'}
            </DropdownItem>
          )}
          {assignment.Stop != null && !assignment.Archived && (
            <DropdownItem onClick={() => archive(assignment)}>
              <FaArchive />
              {' Archivovat'}
            </DropdownItem>
          )}
        </DropdownMenu>
      </UncontrolledButtonDropdown>
    </div>
  );
};

function mapDispatchToProps(dispatch) {
  return {
    start: bindActionCreators(startAssignment, dispatch),
    stop: bindActionCreators(stopAssignment, dispatch),
    pause: bindActionCreators(pauseAssignment, dispatch),
    archive: bindActionCreators(archiveAssignment, dispatch),
    editAssignmentModal: bindActionCreators(editAssignmentModal, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(AssignmentTimeState);
