import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';

import { Container, Row, Col } from 'reactstrap';

import { getAssignment } from '../../actions/assignments.actions';
import { updateDuration } from '../../actions/workTimes.actions';
import { getColor } from '../../utils/assignment';

import Page from '../../components/Page';

import {
  TimeDuration, SignedTimeDuration, actualDuration, totalDuration, totalDurationPercent, getDate,
} from '../../components/Veit/DateAndTime';
import WorkerTypeName from '../../components/Veit/WorkerTypeName';

import TextValues from './TextValues';
import AssignmentTimeState from './AssignmentTimeState';
import WorkTimesTable from './WorkTimesTable';

class Task extends Component {
  componentDidMount() {
    this.props.getAssignment(this.props.match.params.id);
  }

  getName = (userId) => {
    const user = this.props.users.find(u => u.Id === userId);
    return user == null ? '-' : user.UserName;
  }

  durationUpdated = (duration, expectedDuration) => {
    this.props.updateDuration({
      ...this.props.assignment,
      ...{ Duration: duration, ExpectedDuration: expectedDuration },
    });
  }

  render() {
    if (this.props.assignment == null || this.props.currentUser == null) return null;
    const assign = this.props.assignment;
    let titleText = 'Úkol';
    if (assign.Stop != null) {
      titleText = assign.Archived ? 'Archivovaný úkol' : 'Dokončený úkol';
      titleText += ` : ${getDate(assign.Stop).toLocaleString()}`;
    }
    const title = [{ name: titleText, active: true }];
    if (assign.UserId != null) {
      title.unshift({ name: this.getName(assign.UserId), active: false, link: `/user/${assign.UserId}` });
    }
    title.unshift({ name: 'Uživatelé', active: false, link: '/users' });

    return (
      <Page
        title="Úkol"
        breadcrumbs={title}
        submenu={<AssignmentTimeState isAdmin={this.props.isAdmin} assignment={assign} />}
      >
        <Container fluid>
          <Row>
            <TextValues title="Název">{assign.Name}</TextValues>
            <TextValues title="Název bloku">{assign.BlockName}</TextValues>
          </Row>
          <Row>
            <TextValues title="Projekt">
              {this.props.project && this.props.project.Name}
            </TextValues>
            <TextValues title="Skupina">{this.props.role && this.props.role.Name}</TextValues>
          </Row>
          <Row>
            <TextValues title="Pracovnik">
              {assign.UserId && (<Link to={`/user/${assign.UserId}`}>{this.getName(assign.UserId)}</Link>)}
            </TextValues>
            <TextValues title="Odbornost">
              <WorkerTypeName type={assign.WorkType} />
            </TextValues>
          </Row>
          <Row>
            <TextValues title="Abra Kód">
              {assign.Code}
            </TextValues>
          </Row>

          <Row>
            <Col xs="12" sm="12">
              <div className="input-group-text" style={{ whiteSpace: 'initial', textAlign: 'initial' }}>{assign.Description}</div>
            </Col>
          </Row>
          <Row>
            <TextValues title="Plánovaný čas" titleWidth={180}><TimeDuration duration={assign.ExpectedDuration} /></TextValues>
            <TextValues title="Odpracovaný čas" titleWidth={180}><TimeDuration duration={actualDuration(assign)} /></TextValues>
          </Row>
          <Row>
            <TextValues title="Celkový čas" color={getColor(assign)} titleWidth={180}><SignedTimeDuration duration={totalDuration(assign)} /></TextValues>
            <TextValues title="Celkový čas %" color={getColor(assign)} titleWidth={180}>
              {`${totalDurationPercent(assign)} %`}
            </TextValues>
          </Row>
        </Container>
        <WorkTimesTable times={this.props.times} assign={assign} isAdmin={this.props.isAdmin} getName={this.getName} />
      </Page>
    );
  }
}

function mapStateToProps(state) {
  return {
    assignment: state.root.assignment,
    times: state.root.times,
    users: state.root.users,
    currentUser: state.root.currentUser,
    project: state.root.project,
    role: state.root.role,
    isAdmin: state.root.isAdmin,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    reset: () => { dispatch({ type: 'RESET', payload: { assignment: [] } }); },
    getAssignment: bindActionCreators(getAssignment, dispatch),
    updateDuration: bindActionCreators(updateDuration, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Task);
