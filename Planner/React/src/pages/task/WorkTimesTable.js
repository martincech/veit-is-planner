import React from 'react';
import { Table } from 'reactstrap';
import WorkTimesTableRow from './WorkTimesTableRow';
import dayjs from 'dayjs';

function sortTimes(a, b) {
  const aTime = dayjs(a.From);
  const bTime = dayjs(b.From);
  if (aTime.isSame(bTime)) return 0;
  return aTime.isBefore(bTime) ? 1 : -1;
}

const WorkTimesTable = ({
  times, isAdmin, assign, getName,
}) => {
  return (
    <Table striped>
      <thead>
        <th>Pracovník</th>
        <th>Spuštěno</th>
        <th>Zastaveno</th>
        <th>Celkem</th>
        <th style={{ width: '1px' }}></th>
      </thead>
      <tbody>
        {times && times.sort(sortTimes).map((time, index) => (
          <WorkTimesTableRow
            time={time}
            isAdmin={isAdmin}
            isRunning={index === 0 && assign.StartRun != null}
            userName={getName(time.UserId)}
          />
        ))}
      </tbody>
    </Table>
  );
};

export default WorkTimesTable;
