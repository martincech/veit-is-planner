import React from 'react';

import {
  InputGroup, InputGroupText, InputGroupAddon, Col,
} from 'reactstrap';

const TextValues = ({
  title, children, sm, titleWidth, color,
}) => {
  const customStyle = {
    flex: '1 1 auto', whiteSpace: 'nowrap', width: '1%', overflowX: 'hidden',
  };
  if (color != null) customStyle.backgroundColor = color;
  const customWidth = { width: `${titleWidth || 120}px` };
  return (
    <Col xs="12" sm={sm || '6'}>
      <InputGroup>
        <InputGroupAddon addonType="prepend" style={customWidth}>
          <InputGroupText style={customWidth}>{title}</InputGroupText>
        </InputGroupAddon>
        <div className="input-group-text" style={customStyle}>{children}</div>
      </InputGroup>
    </Col>
  );
};

export default TextValues;
