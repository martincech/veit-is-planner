import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Page from '../../components/Page';
import Assignments from '../../components/Veit/Assignments';
import WorkTimes from '../../components/Veit/WorkTimes';
import MonthWorkTimeChart from '../../components/Charts/MonthWorkTimeChart';

import { getUser } from '../../actions/users.actions';
import { getAssignments } from '../../actions/assignments.actions';

import { startOfMonth, endOfMonth } from '../../utils/date';
import { filterClosedBetween } from '../../utils/assignment';

import Submenu, { viewTypes } from './Submenu';

class User extends Component {
  constructor(props) {
    super(props);
    const { history, match } = this.props;
    const { id } = match.params;
    if (id == null || id === '') {
      history.push('/users');
    }
    this.props.reset();
    this.state = {
      view: viewTypes.all,
    };
  }

  componentDidMount() {
    this.props.getUser(this.props.match.params.id);
    this.props.getAssignments({ userId: this.props.match.params.id, month: this.props.month });
    this.setState({ month: this.props.month });
  }

  componentDidUpdate() {
    if (this.props.month !== this.state.month) {
      this.props.getAssignments({ userId: this.props.match.params.id, month: this.props.month });
      this.setState({ month: this.props.month });
    }
  }

  showView = (viewType) => {
    this.setState({ view: viewType });
  }

  contains = (search, object, propsName) => {
    if (propsName == null || object == null) return false;
    let result = false;
    for (let i = 0; i < propsName.length; i++) {
      const value = object[propsName[i]];
      if (value != null) {
        result = result || (`${value}`).toLowerCase().indexOf(search) !== -1;
      }
    }
    return result;
  }

  getView = (assignments, workTimes) => {
    switch (this.state.view) {
      case viewTypes.closed:
        return <Assignments userId={(this.props.user || {}).Id} assignments={assignments} />;
      case viewTypes.times:
        return <WorkTimes workTimes={workTimes} assignments={assignments} />;
      default:
        return <Assignments userId={(this.props.user || {}).Id} assignments={assignments} />;
    }
  }

  render() {
    let { assignments, workTimes } = this.props;

    if (this.state.view === viewTypes.closed) {
      const from = startOfMonth(this.props.month);
      const to = endOfMonth(this.props.month);
      assignments = assignments.filter(f => filterClosedBetween(f, from, to));
    }

    if (this.props.searchValue != null && this.props.searchValue.length > 0) {
      const search = this.props.searchValue.toLowerCase();
      assignments = assignments.filter(a => this.contains(search, a, ['BlockName', 'Description', 'Name', 'Id']));
    }

    if (assignments !== this.props.assignments) {
      const assignmentIds = assignments.map(m => m.Id);
      workTimes = workTimes.filter(f => assignmentIds.indexOf(f.AssignmentId) !== -1);
    }

    return (
      <Page
        title="Uživatel"
        breadcrumbs={[{ name: 'Uživatelé', active: false, link: '/users' }, { name: (this.props.user || {}).UserName || '-', active: true }]}
        submenu={(
          <Submenu
            changeView={this.showView}
            viewType={this.state.view}
          />
        )}
      >
        <div style={{ position: 'relative', width: '100%' }}>
          <MonthWorkTimeChart
            workTimes={workTimes}
            month={this.props.month}
            absences={this.props.absences}
            holiday={this.props.holiday}
            attendance={this.props.attendance}
          />
        </div>
        <div>
          {
            this.getView(assignments, workTimes)
          }
        </div>
      </Page>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    reset: () => { dispatch({ type: 'RESET', payload: { assignments: [] } }); },
    getUser: bindActionCreators(getUser, dispatch),
    getAssignments: bindActionCreators(getAssignments, dispatch),
  };
}

function mapStateToProps(state) {
  return {
    month: state.root.month,
    user: state.root.user,
    group: state.root.group,
    assignments: state.root.assignments,
    searchValue: state.root.searchValue,
    workTimes: state.root.workTimes,
    absences: state.root.absences,
    holiday: state.root.holiday,
    attendance: state.root.attendance,
    isAdmin: state.root.isAdmin,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(User);
