import React from 'react';
import { Button, ButtonGroup } from 'reactstrap';

import MonthSelector from '../../components/Veit/MonthSelector';

export const viewTypes = {
  all: 'ALL',
  closed: 'CLOSED',
  times: 'TIMES',
};

const Submenu = ({ changeView, viewType }) => {
  const list = [
    { type: viewTypes.closed, title: 'Dokončené' },
    { type: viewTypes.all, title: 'Úkoly' },
    { type: viewTypes.times, title: 'Časy' },
  ];

  return (
    <React.Fragment>
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1 }}></div>
        <div style={{ paddingRight: '10px' }}>
          <ButtonGroup>
            {
              list.map(l => <Button active={viewType === l.type} onClick={() => { changeView(l.type); }}>{l.title}</Button>)
            }
          </ButtonGroup>
        </div>
        <div>
          {' '}
          <MonthSelector />
        </div>
      </div>
    </React.Fragment>
  );
};

export default Submenu;
