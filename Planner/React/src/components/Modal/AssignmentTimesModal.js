import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  InputGroup,
  InputGroupAddon,
  Input,
  UncontrolledPopover,
} from 'reactstrap';

import InputMoment from '../InputMoment';

import { closeModal } from '../../actions/modal.actions';
import { updateWorkTime, deleteWorkTime } from '../../actions/workTimes.actions';

class AssignmentTimesModal extends Component {
  state = {}

  componentDidMount() {
    const workTime = this.props.data || {};
    this.setState({
      from: moment(workTime.From),
      to: workTime.To == null ? null : moment(workTime.To),
    });
  }

  toggle = () => {
    this.props.closeModal();
  }

  getLocaleDate = (date) => {
    return date ? date.toLocaleString() : null;
  }

  onChange = (from, to) => {
    this.setState({
      from: moment(from),
      to: to == null ? null : moment(to),
    });
  }

  save = () => {
    this.toggle();
    this.props.updateWorkTime(this.props.data.Id, this.state.from, this.state.to);
  }

  delete = () => {
    this.toggle();
    this.props.deleteWorkTime(this.props.data);
  }

  getDiff = () => {
    return `${this.state.to.diff(this.state.from, 'hours')} h ${this.state.to.diff(this.state.from, 'minutes') % 60} min`;
  }

  render() {
    if (this.state.from == null) return null;
    return (
      <Modal
        backdrop="static"
        isOpen
        toggle={this.toggle}
        className={this.props.className}
      >
        <ModalHeader toggle={this.toggle}>Změnit čas</ModalHeader>
        <ModalBody>
          <InputGroup>
            <InputGroupAddon addonType="prepend">Od</InputGroupAddon>
            <Input id="dateFrom" type="text" readOnly value={this.state.from.format('D.M.YYYY HH:mm:ss')} />
            <UncontrolledPopover fade={false} trigger="click" placement="bottom" target="dateFrom" style={{ maxWidth: 'unset' }}>
              <InputMoment
                moment={this.state.from}
                onChange={d => this.onChange(d, this.state.to)}
                showTime
              />
            </UncontrolledPopover>
          </InputGroup>
          {
            this.state.to && (
              <React.Fragment>
                <br />
                <InputGroup>
                  <InputGroupAddon addonType="prepend">Do</InputGroupAddon>
                  <Input id="dateTo" type="text" readOnly value={this.state.to.format('D.M.YYYY HH:mm:ss')} />
                  <UncontrolledPopover fade={false} trigger="click" placement="bottom" target="dateTo">
                    <InputMoment
                      moment={this.state.to}
                      onChange={d => this.onChange(this.state.from, d)}
                      showTime
                    />
                  </UncontrolledPopover>
                </InputGroup>
                <br />
                <InputGroup>
                  <InputGroupAddon addonType="prepend">Čas</InputGroupAddon>
                  <Input value={this.getDiff()} type="text" readOnly />
                </InputGroup>
              </React.Fragment>
            )
          }
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" disabled={this.state.to == null} onClick={this.delete}>Smazat</Button>
          <Button
            color="primary"
            disabled={this.state.to != null ? this.state.from > this.state.to : this.state.from == null}
            onClick={this.save}
          >
            Uložit
          </Button>
          <Button color="secondary" onClick={this.toggle}>Zrušit</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    updateWorkTime: bindActionCreators(updateWorkTime, dispatch),
    deleteWorkTime: bindActionCreators(deleteWorkTime, dispatch),
    closeModal: bindActionCreators(closeModal, dispatch),
  };
}

export default connect(null, mapDispatchToProps)(AssignmentTimesModal);
