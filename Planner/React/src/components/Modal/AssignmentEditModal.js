import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Container,
  Row,
  Col,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Breadcrumb,
  BreadcrumbItem,
} from 'reactstrap';

import { getAssignmentModalData, editAssignment } from '../../actions/assignments.actions';
import { closeModal } from '../../actions/modal.actions';

const InputWrapper = ({
  title, children, sm, titleWidth, color,
}) => {
  const customStyle = {
    flex: '1 1 auto', whiteSpace: 'nowrap', width: '1%', overflowX: 'hidden',
  };
  if (color != null) customStyle.backgroundColor = color;
  const customWidth = { width: `${titleWidth || 120}px` };
  return (
    <Col xs="12" sm={sm || '6'}>
      <InputGroup>
        <InputGroupAddon addonType="prepend" style={customWidth}>
          <InputGroupText style={customWidth}>{title}</InputGroupText>
        </InputGroupAddon>
        {children}
      </InputGroup>
    </Col>
  );
};

class AssignmentEditModal extends Component {
  state = {
    assignment: null,
  }

  componentDidMount() {
    this.props.getAssignmentModalData().then(() => {
      const expectedHours = Math.floor(this.props.assignment.ExpectedDuration / (1000 * 60 * 60));
      const expectedMinutes = Math.floor((this.props.assignment.ExpectedDuration % (1000 * 60 * 60)) / (1000 * 60));
      this.setState({
        expectedHours,
        expectedMinutes,
        assignment: this.props.assignment,
      });
    });
  }

  toggle = () => {
    this.props.closeModal();
  }

  orderByName = (a, b, prop) => {
    if (a[prop] < b[prop]) { return -1; }
    if (a[prop] > b[prop]) { return 1; }
    return 0;
  }

  mapProjects = (items) => {
    if (items == null) return null;
    return items.sort((a, b) => this.orderByName(a, b, 'Name')).map(m => (<option value={m.Id}>{m.Name}</option>));
  }

  getPlanned = () => {
    if (this.props.modal == null) return [];
    return this.mapProjects(this.props.modal.Projects.filter(f => f.Planned));
  }

  getActive = () => {
    if (this.props.modal == null) return [];
    return this.mapProjects(this.props.modal.Projects.filter(f => !f.Planned && f.CloseDate == null));
  }

  getClosed = () => {
    if (this.props.modal == null) return [];
    return this.mapProjects(this.props.modal.Projects.filter(f => f.CloseDate != null));
  }

  onChange = (e) => {
    const { name, value } = e.target;
    this.setState(prevState => ({ assignment: { ...prevState.assignment, [name]: value } }));
  }

  save = () => {
    this.props.editAssignment({
      ...this.state.assignment,
      ExpectedDuration: this.state.expectedHours * 1000 * 60 * 60 + this.state.expectedMinutes * 1000 * 60,
    }).then(() => {
      this.toggle();
    });
  }

  expoectedHourChanged = (e) => {
    const expectedHours = Number.isNaN(e.target.value) ? 0 : parseInt(e.target.value, 10);
    this.setState({ expectedHours });
  }

  expectedMinuteChanged = (e) => {
    const expectedMinutes = Number.isNaN(e.target.value) ? 0 : parseInt(e.target.value, 10);
    this.setState({ expectedMinutes });
  }

  render() {
    const { modal } = this.props;
    const { assignment } = this.state;
    if (assignment == null) return null;
    return (
      <Modal
        backdrop="static"
        isOpen
        className={this.props.className}
      >
        <ModalHeader toggle={this.cancel}>Upravit úkol</ModalHeader>
        <ModalBody>
          <Container>
            <Row>
              <InputWrapper title="Název">
                <Input value={assignment.Name || ''} name="Name" onChange={this.onChange} />
              </InputWrapper>
              <InputWrapper title="Název bloku">
                <Input value={assignment.BlockName || ''} name="BlockName" onChange={this.onChange} />
              </InputWrapper>
            </Row>
            <Row>
              <InputWrapper title="Projekt">
                <select value={assignment.ProjectId} name="ProjectId" className="form-control" onChange={this.onChange}>
                  <optgroup label="Aktivní">
                    {this.getActive()}
                  </optgroup>
                  <optgroup label="Plnánované">
                    {this.getPlanned()}
                  </optgroup>
                  <optgroup label="Dokončená">
                    {this.getClosed()}
                  </optgroup>
                </select>
                {/* {this.props.project && this.props.project.Name} */}
              </InputWrapper>
              <InputWrapper title="Skupina">
                <select value={assignment.RoleId} name="RoleId" className="form-control" onChange={this.onChange}>
                  <option></option>
                  {
                    modal && modal.Groups.map(m => (<option value={m.Id}>{m.Name}</option>))
                  }
                </select>
                {/* {this.props.role && this.props.role.Name} */}
              </InputWrapper>
            </Row>
            <Row>
              <InputWrapper title="Pracovnik">
                <select value={assignment.UserId} name="UserId" disabled={assignment.StartRun != null} className="form-control" onChange={this.onChange}>
                  <option></option>
                  {
                    modal && modal.Groups.sort((a, b) => this.orderByName(a, b, 'Name')).map(m => (
                      <optgroup label={m.Name}>
                        {modal && modal.Users.filter(f => f.GroupId === m.Id).sort((a, b) => this.orderByName(a, b, 'UserName')).map(m => (
                          <option value={m.Id}>{m.UserName}</option>
                        ))
                        }
                      </optgroup>
                    ))
                  }
                </select>
                {/* <Link to={`/user/${assignment.UserId}`}>{this.getName(assignment.UserId)}</Link> */}
              </InputWrapper>
              <InputWrapper title="Odbornost">
                <select value={assignment.WorkType} name="WorkType" onChange={this.onChange} className="form-control">
                  <option value={1}>Zámečník</option>
                  <option value={2}>Elektro</option>
                </select>
                {/* <WorkerTypeName type={assignment.WorkType} /> */}
              </InputWrapper>
            </Row>
            <Row>
              <InputWrapper title="Abra Kód">
                <Input value={assignment.Code || ''} name="Code" onChange={this.onChange} />
              </InputWrapper>
            </Row>

            <Row>
              <Col xs="12" sm="12">
                <div
                  className="input-group-text"
                  style={{
                    whiteSpace: 'initial',
                    textAlign: 'initial',
                    padding: 'unset',
                  }}
                >
                  <textarea
                    placeholder="Popis"
                    name="Description"
                    type="textarea"
                    value={assignment.Description || ''}
                    style={{ width: '100%' }}
                    onChange={this.onChange}
                  />
                </div>
              </Col>
            </Row>
            <Row>
              <Col>
                <Breadcrumb>
                  <BreadcrumbItem active>Plánovaný čas</BreadcrumbItem>
                </Breadcrumb>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">Hodin</InputGroupAddon>
                  <Input value={this.state.expectedHours} type="number" min={0} onChange={this.expoectedHourChanged} />
                </InputGroup>
                <br />
                <InputGroup>
                  <InputGroupAddon addonType="prepend">Minut</InputGroupAddon>
                  <Input value={this.state.expectedMinutes} type="number" min={0} max={59} onChange={this.expectedMinuteChanged} />
                </InputGroup>
              </Col>
            </Row>
          </Container>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.save}>Uložit</Button>
          <Button color="secondary" onClick={this.toggle}>Zrušit</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    assignment: state.root.assignment,
    modal: state.root.modal,
  };
};

function mapDispatchToProps(dispatch) {
  return {
    getAssignmentModalData: bindActionCreators(getAssignmentModalData, dispatch),
    editAssignment: bindActionCreators(editAssignment, dispatch),
    closeModal: bindActionCreators(closeModal, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentEditModal);
