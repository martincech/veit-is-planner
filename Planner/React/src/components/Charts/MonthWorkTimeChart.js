import React from 'react';
import { Bar } from 'react-chartjs-2';

const getTimeFromHours = (label) => {
  if (label.value === 'NaN') return null;
  const h = parseFloat(label.yLabel);
  return `${Math.floor(h)} hodin ${Math.floor((h % 1) * 60)} minut`;
};

const daysInMonth = (month, year) => {
  return new Date(year, month, 0).getDate();
};

const mapAbsences = (date, day, absences) => {
  if (absences == null) return null;
  const dayDate = new Date(date.getFullYear(), date.getMonth(), day);
  const d2 = absences.find((f) => {
    const aDate = new Date(f.Date);
    return aDate.getMonth() === dayDate.getMonth() && aDate.getDate() === day;
  });
  return d2 == null ? null : d2.Hours;
};

const genLineData = (moreData = {}, workTimes, months, absences, holiday, attendance) => {
  const date = new Date();
  date.setDate(1);
  date.setMonth(date.getMonth() - months);
  const daysCount = daysInMonth(date.getMonth() + 1, date.getFullYear());
  const days = new Array(daysCount);
  const workDays = {};
  const hasBreak = {};

  workTimes.forEach((wt) => {
    if (wt.To != null && wt.From != null) {
      const from = new Date(wt.From);
      const to = new Date(wt.To);
      const day = from.getDate() - 1;
      const duration = to - from;
      const hours = Math.abs(duration) / (1000 * 60 * 60);

      if (workDays[day] == null) workDays[day] = 0;
      workDays[day] += hours;
      const fd = from.getHours() + from.getMinutes() / 100;
      if (fd < 10) { hasBreak[day] = true; }
    }
  });

  for (let index = 0; index < days.length; index++) {
    days[index] = workDays[index] || 0;
    // if (hasBreak[index]) days[index] += 0.417;
  }

  return {
    labels: days.map((d, i) => i + 1),
    datasets: [
      {
        label: 'Svátek',
        backgroundColor: 'red',
        borderColor: 'red',
        borderWidth: 2,
        pointHitRadius: 0,
        pointHoverRadius: 0,
        pointRadius: 0,
        data: days.map((d, i) => ((holiday || []).indexOf(i + 1) !== -1 ? 0.1 : null)),
        ...moreData,
      },
      {
        label: 'Přestávka + Korekce',
        backgroundColor: 'green',
        borderColor: 'green',
        borderWidth: 1,
        data: days.map((d, i) => (hasBreak[i] ? 0.417 : 0)),
        stacked: true,
        ...moreData,
      },
      {
        label: 'Hodiny',
        backgroundColor: '#4563fa',
        borderColor: '#4563fa',
        borderWidth: 1,
        data: days,
        stacked: true,
        ...moreData,
      },
      {
        label: 'Absence',
        backgroundColor: 'gray',
        borderColor: 'gray',
        borderWidth: 1,
        data: days.map((d, i) => mapAbsences(date, i + 1, absences)),
        stacked: false,
        ...moreData,
      },
      {
        label: 'Docházka',
        backgroundColor: 'lightgray',
        borderColor: 'lightgray',
        borderWidth: 1,
        data: days.map((d, i) => (attendance[i] == null ? null : attendance[i].Hours === 0 ? null : attendance[i].Hours)),
        type: 'line',
        fill: true,
      },
    ],
  };
};

const MonthWorkTimeChart = ({
  workTimes, month, absences, holiday, attendance,
}) => {
  return (
    <Bar
      height={250}
      options={{
        maintainAspectRatio: false,
        responsive: true,
        legend: { display: true, reverse: true, intersect: false },
        tooltips: {
          mode: 'x',
          // filter: tt => (tt.datasetIndex !== 3),
          callbacks: {
            title: item => (item[0] == null ? null : `Den : ${item[0].xLabel}`),
            label: item => getTimeFromHours(item),
          },
        },
        scales: {
          xAxes: [{
            stacked: true,
          }],
          yAxes: [{
            stacked: true,
            ticks: { suggestedMax: 8 },
          }],
        },
      }}
      data={genLineData({ fill: false }, workTimes, month, absences, holiday, attendance || [])}
    />
  );
};

export default MonthWorkTimeChart;
