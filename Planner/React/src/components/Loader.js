import React from 'react';
import { connect } from 'react-redux';
import LoadingOverlay from 'react-loading-overlay';


const Loader = ({ children, isFetching }) => {
  return (
    <LoadingOverlay active={isFetching} spinner>
      {
        children
      }
    </LoadingOverlay>
  );
};

const mapStateToProps = (state) => {
  return {
    isFetching: state.root.isFetching,
  };
};

export default connect(mapStateToProps)(Loader);
