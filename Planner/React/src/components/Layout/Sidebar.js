import React from 'react';
import { connect } from 'react-redux';
import bn from '../../utils/bemnames';

import {
  Nav,
  NavItem,
  NavLink as BSNavLink,
} from 'reactstrap';
import { NavLink } from 'react-router-dom';

import {
  MdPeople,
} from 'react-icons/lib/md';

import {
  FaArchive,
  FaHome,
  FaCogs,
  FaGroup,
  FaFolder,
  FaUser,
} from 'react-icons/lib/fa';

import sidebarBgImage from '../../assets/img/sidebar/sidebar-4.jpg';

const sidebarBackground = {
  backgroundImage: `url("${sidebarBgImage}")`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
};

const links = [
  {
    to: '/', name: 'Home', admin: false, icon: FaHome,
  },
  {
    to: '/project', name: 'Projekty', admin: false, icon: FaCogs,
  },
  {
    to: '/archive', name: 'Archiv', admin: true, icon: FaArchive,
  },
  {
    to: '/user', name: 'Uživatelé', admin: true, icon: FaUser,
  },
  {
    to: '/group', name: 'Skupiny', admin: true, icon: FaGroup,
  },
  {
    to: '/template', name: 'Šablony', admin: true, icon: FaFolder,
  },
];

const bem = bn.create('sidebar');

class Sidebar extends React.Component {
  handleClick = name => () => {
    this.setState((prevState) => {
      const isOpen = prevState[`isOpen${name}`];

      return {
        [`isOpen${name}`]: !isOpen,
      };
    });
  };

  render() {
    const { isAdmin } = this.props;
    return (
      <aside className={bem.b()} data-image={sidebarBgImage}>
        <div className={bem.e('background')} style={sidebarBackground} />
        <div className={bem.e('content')}>
          <Nav vertical>
            {
              links.filter(f => isAdmin || !f.admin).map(({ to, name, icon: Icon }) => (
                <NavItem>
                  <a href={to} className="text-uppercase nav-link">
                    <Icon className="cr-sidebar__nav-item-icon" />
                    {' '}
                    {' '}
                    {name}
                  </a>
                </NavItem>
              ))
            }
            <NavItem className={bem.e('nav-item')}>
              <BSNavLink
                className="text-uppercase"
                tag={NavLink}
                to="/users"
                activeClassName="active"
                exact={false}
              >
                <MdPeople className={bem.e('nav-item-icon')} />
                <span className="">Dashboard</span>
              </BSNavLink>
            </NavItem>
          </Nav>
        </div>
      </aside>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAdmin: state.root.isAdmin,
  };
};

export default connect(mapStateToProps)(Sidebar);
