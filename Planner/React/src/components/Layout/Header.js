import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Navbar,
  // NavbarToggler,
  Nav,
  NavItem,
  NavLink,
  Popover,
  PopoverBody,
  ListGroup,
  ListGroupItem,
  Button,
} from 'reactstrap';
import {
  MdClearAll,
  MdExitToApp,
} from 'react-icons/lib/md';

import bn from '../../utils/bemnames';
import Avatar from '../Avatar';
import UserCard from '../Card/UserCard';
import SearchInput from '../SearchInput';
import { getCurrentUser } from '../../actions/users.actions';

const bem = bn.create('header');

class Header extends React.Component {
  state = {
    isOpenNotificationPopover: false,
    isNotificationConfirmed: false,
    isOpenUserCardPopover: false,
  };

  componentDidMount() {
    this.props.getCurrentUser();
  }

  toggleNotificationPopover = () => {
    this.setState(prevState => ({
      isOpenNotificationPopover: !prevState.isOpenNotificationPopover,
    }));

    if (!this.state.isNotificationConfirmed) {
      this.setState({ isNotificationConfirmed: true });
    }
  };

  toggleUserCardPopover = () => {
    this.setState(prevState => ({
      isOpenUserCardPopover: !prevState.isOpenUserCardPopover,
    }));
  };

  handleSidebarControlButton = (event) => {
    event.preventDefault();
    event.stopPropagation();

    document.querySelector('.cr-sidebar').classList.toggle('cr-sidebar--open');
  };

  render() {
    if (this.props.currentUser == null) return null;
    return (
      <Navbar light expand className={bem.b('bg-white')}>
        <Nav navbar className="mr-2">
          <Button outline onClick={this.handleSidebarControlButton}>
            <MdClearAll size={25} />
          </Button>
        </Nav>
        <Nav navbar>
          <SearchInput />
        </Nav>

        <Nav navbar className={bem.e('nav-right')}>

          <NavItem>
            <NavLink id="Popover2">
              <Avatar
                onClick={this.toggleUserCardPopover}
                className="can-click"
              />
            </NavLink>
            <Popover
              placement="bottom-end"
              isOpen={this.state.isOpenUserCardPopover}
              toggle={this.toggleUserCardPopover}
              target="Popover2"
              className="p-0 border-0"
              style={{ minWidth: 250 }}
            >
              <PopoverBody className="p-0 border-light">
                <UserCard
                  title={this.props.currentUser.UserName}
                  subtitle={this.props.currentUser.Role}
                  className="border-light"
                >
                  <ListGroup flush>
                    <ListGroupItem tag="button" action className="border-light can-click" onClick={() => { window.location.href = '/account/logout'; }}>
                      <MdExitToApp />
                      {' Odhlásit'}
                    </ListGroupItem>
                  </ListGroup>
                </UserCard>
              </PopoverBody>
            </Popover>
          </NavItem>
        </Nav>
      </Navbar>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    getCurrentUser: bindActionCreators(getCurrentUser, dispatch),
  };
}

function mapStateToProps(state) {
  return {
    currentUser: state.root.currentUser,
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);
