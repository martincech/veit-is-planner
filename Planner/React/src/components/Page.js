import React from 'react';
import { Link } from 'react-router-dom';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import bn from '../utils/bemnames';
import PropTypes from '../utils/propTypes';

const bem = bn.create('page');

const Page = ({
  title,
  breadcrumbs,
  tag: Tag,
  className,
  children,
  submenu,
  middleTitle,
  ...restProps
}) => {
  const classes = bem.b('px-3', className);

  const submenuStyle = {
    alignSelf: 'flex-start',
    marginTop: 0,
    marginBottom: '1rem',
  };

  return (
    <Tag className={classes} {...restProps}>
      <div className={bem.e('header')}>
        {breadcrumbs && (
          <Breadcrumb className={bem.e('breadcrumb')}>
            {breadcrumbs.length
              && breadcrumbs.map(({ name, active, link }) => (
                <BreadcrumbItem key={name} active={active}>
                  {link == null ? name : <Link to={link}>{name}</Link>}
                </BreadcrumbItem>
              ))}
          </Breadcrumb>
        )}
        <div style={{ flex: 1 }}></div>
        {middleTitle}
        <div style={{ flex: 1 }}></div>
        <div style={submenuStyle}>{submenu}</div>
      </div>
      {children}
    </Tag>
  );
};

Page.propTypes = {
  tag: PropTypes.component,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  className: PropTypes.string,
  children: PropTypes.node,
  breadcrumbs: PropTypes.arrayOf(
    PropTypes.shape({
      link: PropTypes.string,
      name: PropTypes.string,
      active: PropTypes.bool,
    }),
  ),
};

Page.defaultProps = {
  tag: 'div',
  title: '',
  className: null,
  children: null,
  breadcrumbs: [],
};

export default Page;
