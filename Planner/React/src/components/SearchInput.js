import React from 'react';
import { connect } from 'react-redux';
import { Form, Input } from 'reactstrap';
import { MdSearch } from 'react-icons/lib/md';

const SearchInput = (props) => {
  const handleInputChange = (event) => {
    props.search(event.target.value);
  };

  return (
    <Form inline className="cr-search-form" onSubmit={e => e.preventDefault()}>
      <MdSearch
        size="20"
        className="cr-search-form__icon-search text-secondary"
      />
      <Input
        type="search"
        className="cr-search-form__input"
        placeholder="Hledat..."
        value={props.searchValue}
        onChange={handleInputChange}
      />
    </Form>
  );
};


function mapStateToProps(state) {
  return {
    searchValue: state.root.searchValue,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    search: (text) => { dispatch({ type: 'SET_SEARCH', payload: { searchValue: text } }); },
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SearchInput);
