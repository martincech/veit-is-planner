import React from 'react';
import FaPlay from 'react-icons/lib/fa/play';
import FaStop from 'react-icons/lib/fa/stop';
import FaPause from 'react-icons/lib/fa/pause';
import FaQuestion from 'react-icons/lib/fa/question';

export default function AssignmentStatusIcon({ assignment, className }) {
  if (assignment == null) return null;
  if (assignment.Stop != null) return <FaStop className={className} />;
  if (assignment.StartRun != null) return <FaPlay className={className} />;
  if (assignment.Start != null) return <FaPause className={className} />;
  return <FaQuestion className={className} />;
}
