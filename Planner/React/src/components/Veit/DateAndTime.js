import React from 'react';

export function getDate(date) {
  return (date instanceof Date) ? date : new Date(Date.parse(date));
}

function getHours(duration) {
  return Math.floor(Math.abs(duration) / (1000 * 60 * 60));
}

function getMinutes(duration) {
  return Math.floor((Math.abs(duration) % (1000 * 60 * 60)) / (1000 * 60));
}

export function actualDuration(assignment) {
  if (assignment == null || (assignment.Start == null && assignment.Duration === 0)) return null;
  let duration = assignment.Duration;
  if (assignment.StartRun != null) duration += ((new Date()) - getDate(assignment.StartRun));
  return duration;
}

export function totalDuration(assignment) {
  if (assignment == null || (assignment.Start == null && assignment.Duration === 0)) return null;
  let diff = -assignment.ExpectedDuration + assignment.Duration;
  if (assignment.StartRun != null) diff += ((new Date()) - getDate(assignment.StartRun));
  return diff;
}

export function totalDurationPercent(assignment) {
  const duration = totalDuration(assignment);
  if (duration == null || assignment.ExpectedDuration === 0) return '';
  return Math.round(duration / assignment.ExpectedDuration * 100) + 100;
}

export const TimeDuration = ({ duration }) => {
  if (duration == null) return '-';
  return (
    <React.Fragment>
      {getHours(duration)}
      {' '}
      h
      {' '}
      {getMinutes(duration)}
      {' '}
      min
    </React.Fragment>
  );
};

export const SignedTimeDuration = ({ duration }) => {
  if (duration == null) return '-';
  return (
    <React.Fragment>
      {duration < 0 ? '- ' : '+ '}
      <TimeDuration duration={duration} />
    </React.Fragment>
  );
};

export const TimeDiff = ({ from, to }) => {
  if (from == null || to == null) return '-';
  const diff = (getDate(to) - getDate(from));
  return <TimeDuration duration={diff} />;
};

export default ({ date }) => {
  if (date == null) return '-';
  const d = getDate(date);
  return (
    <React.Fragment>
      {d.toLocaleString()}
    </React.Fragment>
  );
};
