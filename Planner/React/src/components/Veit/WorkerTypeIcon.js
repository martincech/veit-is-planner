import React from 'react';
import FaWrench from 'react-icons/lib/fa/wrench';
import FaBolt from 'react-icons/lib/fa/bolt';
import FaPaperPlane from 'react-icons/lib/fa/paper-plane';

export default function WorkerTypeIcon({ type, className, ...otherProps }) {
  let Icon = FaPaperPlane;
  if (type === 1) Icon = FaWrench;
  if (type === 2) Icon = FaBolt;
  return <Icon className={className} {...otherProps} />;
}
