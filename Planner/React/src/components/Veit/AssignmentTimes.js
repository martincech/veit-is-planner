import React from 'react';

function actualDuration(assignment) {
  if (assignment == null || (assignment.Start == null && assignment.Duration === 0)) return null;
  let diff = -assignment.ExpectedDuration + assignment.Duration;
  if (assignment.StartRun != null) diff += (new Date() - Date.parse(assignment.StartRun));
  return diff;
}

function durationTime(duration) {
  const dur = duration || 0;
  return `${Math.floor(dur / (1000 * 60 * 60))}h ${Math.floor((dur % (1000 * 60 * 60)) / (1000 * 60))}min`;
}

export default function AssignmentTimes({ assignment }) {
  if (assignment == null) return '-';
  const duration = actualDuration(assignment);
  return duration == null ? null : (
    <div>
      {' '}
      {duration > 0 ? '+' : '-'}
      {' '}
      {durationTime(Math.abs(duration))}
      {' '}
    </div>
  );
}
