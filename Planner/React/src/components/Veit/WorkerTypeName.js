export default function WorkerTypeName({ type }) {
  if (type === 1) return 'Zámečníni';
  if (type === 2) return 'Elektro';
  return 'Ostatní';
}
