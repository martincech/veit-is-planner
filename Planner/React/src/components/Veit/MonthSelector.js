import React, { Component } from 'react';

import {
  ButtonGroup,
  Button,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

import { connect } from 'react-redux';

class MonthSelector extends Component {
  months = ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'].reverse();

  constructor(props) {
    super(props);

    const from = new Date(2017, 1);
    const to = new Date();
    const currentYear = to.getFullYear();
    const currentMonth = to.getMonth();
    const yearsDiff = currentYear - from.getFullYear();
    const years = Array.from(new Array(yearsDiff + 1), (val, index) => index).map(m => currentYear - m);

    this.state = {
      dropdownOpen: false,
      year: null,
      years,
      currentYear,
      currentMonth,
    };
  }

  toggle = () => {
    this.setState(prevState => ({ dropdownOpen: !prevState.dropdownOpen, year: null }));
  }

  selectMonth = (year) => {
    this.setState({ year });
  }

  monthChanged = (monthName) => {
    const index = this.months.indexOf(monthName);
    const month = 11 - index;
    const to = new Date();
    const from = new Date(this.state.year, month);
    const months = to.getMonth() - from.getMonth() + (12 * (to.getFullYear() - from.getFullYear()));
    this.setMonth(months);
  }

  setMonth = (month) => {
    this.props.setMonth(month);
  }

  filterMonths = (month, index) => {
    return !(this.state.currentYear === this.state.year && (11 - this.state.currentMonth) > index);
  }

  render() {
    const date = new Date();
    date.setDate(1);
    date.setMonth(date.getMonth() - this.props.month);
    const monthIndex = 11 - date.getMonth();
    const monthName = this.months[monthIndex];
    const activeYear = date.getFullYear();

    return (
      <ButtonGroup>
        <Button onClick={() => this.setMonth(this.props.month + 1)}>&lt;</Button>
        <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
          <DropdownToggle style={{ width: '150px' }} active>
            {monthName}
            {' '}
            {activeYear}
          </DropdownToggle>
          <DropdownMenu>
            {
              this.state.year
                ? this.months.filter(this.filterMonths).map(month => <DropdownItem active={activeYear === this.state.year && monthName === month} onClick={() => this.monthChanged(month)}>{month}</DropdownItem>)
                : this.state.years.map(year => <DropdownItem active={activeYear === year} onClick={() => this.selectMonth(year)} toggle={false}>{year}</DropdownItem>)
            }
          </DropdownMenu>
        </ButtonDropdown>
        <Button onClick={() => this.setMonth(this.props.month - 1)} disabled={this.props.month < -6}>&gt;</Button>
      </ButtonGroup>
    );
  }
}

function mapStateToProps(state) {
  return {
    month: state.root.month,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    setMonth: (month) => { dispatch({ type: 'SET_MONTH', payload: { month } }); },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MonthSelector);
