import React from 'react';
import { Link } from 'react-router-dom';

import { Table } from 'reactstrap';

import WorkerTypeIcon from './WorkerTypeIcon';
import AssignmentStatusIcon from './AssignmentStatusIcon';

import DataAndTime, { TimeDiff } from './DateAndTime';

import {
  FaGroup,
} from 'react-icons/lib/fa';

function mapAssignment(workTime, assignments) {
  const assignment = assignments.find(f => f.Id === workTime.AssignmentId);
  return { assignment: assignment || {}, ...workTime };
}

const WorkTimes = ({ workTimes, assignments }) => {
  return (
    <div>
      <Table striped>
        <thead>
          <tr>
            <th>Úkol ID</th>
            <th>Úkol</th>
            <th>Start</th>
            <th>Stop</th>
            <th>Celkem</th>
          </tr>
        </thead>
        <tbody>
          {
            workTimes && workTimes.slice().reverse().map(m => mapAssignment(m, assignments)).map(a => (
              <tr key={a.Id}>
                <td>
                  <AssignmentStatusIcon assignment={a.assignment} />
                  {' '}
                  {a.AssignmentId}
                  {a.assignment.UserId === a.UserId ? null : <FaGroup style={{ float: 'right', height: '24px' }} />}
                </td>
                <td>
                  <WorkerTypeIcon type={a.assignment.WorkType} />
                  <Link to={`/task/${a.AssignmentId}`}>
                    {' '}
                    {a.assignment.BlockName}
                    {' '}
                    ||
                    {' '}
                    {a.assignment.Name}
                  </Link>
                </td>
                <td>
                  <DataAndTime date={a.From} />
                </td>
                <td>
                  <DataAndTime date={a.To} />
                </td>
                <td>
                  <TimeDiff from={a.From} to={a.To} />
                </td>
              </tr>
            ))
          }
        </tbody>
      </Table>
    </div>
  );
};

export default WorkTimes;
