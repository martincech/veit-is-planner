import React from 'react';
import { Link } from 'react-router-dom';

import { Table } from 'reactstrap';

import {
  FaGroup,
} from 'react-icons/lib/fa';

import { getColor } from '../../utils/assignment';

import WorkerTypeIcon from './WorkerTypeIcon';
import AssignmentStatusIcon from './AssignmentStatusIcon';
import AssignmentTimes from './AssignmentTimes';

function orderByProgress(assignments) {
  const running = [];
  const stopped = [];
  const planned = [];
  const paused = [];
  assignments.forEach((item) => {
    if (item.StartRun != null) {
      running.push(item);
    } else if (item.Stop != null) {
      stopped.push(item);
    } else if (item.Start == null) {
      planned.push(item);
    } else {
      paused.push(item);
    }
  });
  return [...running, ...paused, ...planned, ...stopped];
}

export default function Assignments({ assignments, userId }) {
  return (
    <div>
      <Table striped>
        <thead>
          <tr>
            <th style={{ width: '1px' }}>Úkol ID</th>
            <th>Úkol</th>
            <th>Stav</th>
          </tr>
        </thead>
        <tbody>
          {
            assignments && orderByProgress(assignments).map(a => (
              <tr key={a.Id}>
                <td>{a.Id}</td>
                <td>
                  <AssignmentStatusIcon assignment={a} />
                  {' '}
                  {userId === a.UserId ? null : <FaGroup style={{ height: '24px' }} />}
                  {' '}
                  <WorkerTypeIcon type={a.WorkType} />
                  <Link to={`/task/${a.Id}`}>
                    {' '}
                    {a.BlockName}
                    {' '}
                    |
                    {' '}
                    {a.Name}
                    {' '}
                    |
                    {' '}
                    {a.Description}
                    {' '}
                  </Link>
                </td>
                <td style={{ backgroundColor: getColor(a), whiteSpace: 'nowrap' }}>
                  <AssignmentTimes assignment={a} />
                </td>
              </tr>
            ))
          }
        </tbody>
      </Table>
    </div>
  );
}
