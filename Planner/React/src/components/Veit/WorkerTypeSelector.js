import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

import { filterByWorkType } from '../../actions/filter.actions';
import WorkerTypeName from './WorkerTypeName';

class WorkerTypeSelector extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
    };
  }

  filter = (workType) => {
    this.setState({ dropdownOpen: false });
    this.props.filterByWorkType(workType);
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen,
    }));
  }

  render() {
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret active={this.props.workType}>
          {this.props.workType ? <WorkerTypeName type={this.props.workType} /> : 'Odbornost'}
        </DropdownToggle>
        <DropdownMenu right>
          {
            [1, 2].map(type => (
              <DropdownItem key={type} active={this.props.workType === type} toggle={false} onClick={() => this.filter(type)}>
                <WorkerTypeName type={type} />
              </DropdownItem>
            ))
          }
          {
            this.props.workType
            && (
              <React.Fragment>
                <DropdownItem divider />
                <DropdownItem toggle={false} onClick={() => this.filter(null)}>Zrušit</DropdownItem>
              </React.Fragment>
            )
          }
        </DropdownMenu>
      </Dropdown>
    );
  }
}


function mapStateToProps(state) {
  return {
    workType: state.root.filter.workType,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    filterByWorkType: bindActionCreators(filterByWorkType, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkerTypeSelector);
