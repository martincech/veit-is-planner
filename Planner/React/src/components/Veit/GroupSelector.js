import React, { Component } from 'react';

import { connect } from 'react-redux';

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';

import { bindActionCreators } from 'redux';

import { filterByGroup } from '../../actions/filter.actions';


class GroupSelector extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false,
    };
  }

  filter = (group) => {
    this.props.filterByGroup(group != null ? group.Id : null);
    this.setState({ dropdownOpen: false });
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen,
    }));
  }

  render() {
    const group = this.props.groups.find(f => f.Id === this.props.groupId);
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret active={this.props.groupId}>
          {group ? group.Name : 'Skupina'}
        </DropdownToggle>
        <DropdownMenu right>
          {
            this.props.groups.map(gr => (
              <DropdownItem key={gr.Id} active={this.props.groupId === gr.Id} toggle={false} onClick={() => this.filter(gr)}>
                {gr.Name}
              </DropdownItem>
            ))
          }
          {
            this.props.groupId
            && (
              <React.Fragment>
                <DropdownItem divider />
                <DropdownItem toggle={false} onClick={() => this.filter(null)}>Zrušit</DropdownItem>
              </React.Fragment>
            )
          }
        </DropdownMenu>
      </Dropdown>
    );
  }
}


function mapStateToProps(state) {
  return {
    groups: state.root.groups,
    groupId: state.root.filter.groupId,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    filterByGroup: bindActionCreators(filterByGroup, dispatch),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupSelector);
