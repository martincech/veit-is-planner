import React from 'react';
import { connect } from 'react-redux';
import AssignmentEditModal from './components/Modal/AssignmentEditModal';
import AssignmentTimesModal from './components/Modal/AssignmentTimesModal';

import { EDIT_ASSIGNMENT_MODAL, EDIT_WORK_TIME_MODAL } from './actions/modal.actions';

const getModal = (modal) => {
  if (modal == null || modal.type == null) return null;
  switch (modal.type) {
    case EDIT_ASSIGNMENT_MODAL:
      return AssignmentEditModal;
    case EDIT_WORK_TIME_MODAL:
      return AssignmentTimesModal;
    default:
      return null;
  }
};

const ModalContainer = ({ modal }) => {
  const ModalWindow = getModal(modal);
  return ModalWindow == null ? null : <ModalWindow data={modal.payload} />;
};

const mapStateToProps = (state) => {
  return {
    modal: state.modal,
  };
};

export default connect(mapStateToProps)(ModalContainer);
