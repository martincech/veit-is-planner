import React from 'react';
import ReactDOM from 'react-dom';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import { Provider } from 'react-redux';
import * as moment from 'moment';
import 'moment/locale/cs';

import App from './App';
import configureStore from './store';

moment.locale('cs');
const history = createBrowserHistory({ basename: `/${process.env.PUBLIC_URL.split('/').pop()}` });
const initialState = window.initialReduxState;
const store = configureStore(history, initialState);

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'),
);
