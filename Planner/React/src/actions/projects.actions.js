import axios from 'axios';

export const FETCH_PROJECTS = 'FETCH_PROJECTS';

export const getAllProjects = () => {
  return (dispatch) => {
    axios.get('/project/get').then((result) => {
      dispatch({
        type: FETCH_PROJECTS,
        payload: {
          users: result.data.Users,
          groups: result.data.Groups,
          projects: result.data.Projects,
        },
      });
    });
  };
};
