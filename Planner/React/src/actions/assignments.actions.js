import axios from 'axios';

export const FETCH_ASSIGNMENTS = 'FETCH_ASSIGNMENTS';
export const FETCH_LAST_ASSIGNMENTS = 'FETCH_LAST_ASSIGNMENTS';
export const FETCH_ASSIGNMENT = 'FETCH_ASSIGNMENT';
export const SET_LOADER = 'SET_LOADER';

export const getAssignments = ({ userId, month }) => {
  return (dispatch) => {
    dispatch({ type: SET_LOADER, payload: true });
    axios.get('/assignment/getby', {
      params: {
        userId,
        month,
      },
    }).then((result) => {
      dispatch({
        type: FETCH_ASSIGNMENTS,
        payload: {
          absences: result.data.Absences,
          assignments: result.data.Assignments,
          workTimes: result.data.WorkTimes,
          attendance: result.data.Attendance,
          holiday: result.data.Holiday,
          isFetching: false,
        },
      });
    });
  };
};

export const getLastAssignments = ({ month }) => {
  return (dispatch) => {
    dispatch({ type: SET_LOADER, payload: true });
    axios.get('/assignment/getlastby', {
      params: {
        month,
      },
    }).then((result) => {
      dispatch({
        type: FETCH_LAST_ASSIGNMENTS,
        payload: result.data,
      });
      dispatch({ type: SET_LOADER, payload: false });
    });
  };
};

export const getAssignment = (assignmentId) => {
  return (dispatch) => {
    axios.get('/assignment/getbyId', {
      params: {
        assignmentId,
      },
    }).then((result) => {
      dispatch({
        type: FETCH_ASSIGNMENT,
        payload: {
          assignment: result.data.Assignment,
          times: result.data.Times,
          users: result.data.Users,
          project: result.data.Project,
          role: result.data.Role,
        },
      });
    });
  };
};

export const startAssignment = (assignment) => {
  return (dispatch) => {
    return axios.post('/assignment/start', assignment).then((result) => {
      dispatch({ type: 'ASSIGNMENT_START', payload: result.data });
      getAssignment(assignment.Id)(dispatch);
    });
  };
};

export const pauseAssignment = (assignment) => {
  return (dispatch) => {
    return axios.post('/assignment/pause', assignment).then((result) => {
      dispatch({ type: 'ASSIGNMENT_PAUSE', payload: result.data });
      getAssignment(assignment.Id)(dispatch);
    });
  };
};

export const stopAssignment = (assignment) => {
  return (dispatch) => {
    return axios.post('/assignment/stop', assignment).then((result) => {
      dispatch({ type: 'ASSIGNMENT_STOP', payload: result.data });
      getAssignment(assignment.Id)(dispatch);
    });
  };
};

export const archiveAssignment = (assignment) => {
  return (dispatch) => {
    return axios.post('/assignment/archiveassignment', assignment).then((result) => {
      dispatch({ type: 'ASSIGNMENT_ARCHIVE', payload: result.data });
      getAssignment(assignment.Id)(dispatch);
    });
  };
};

export const editAssignment = (assignment) => {
  return (dispatch) => {
    return axios.post('/assignment/update', assignment).then((result) => {
      dispatch({ type: 'ASSIGNMENT_EDIT', payload: result.data });
      getAssignment(assignment.Id)(dispatch);
    });
  };
};

export const getAssignmentModalData = () => {
  return (dispatch) => {
    return axios.get('/project/get').then((result) => {
      dispatch({ type: 'MODAL_ASSIGNMENT', payload: result.data });
    });
  };
};
