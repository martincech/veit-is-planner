export const SET_FILTER_TEXT = 'SET_FILTER_TEXT';
export const SET_FILTER_GROUP = 'SET_FILTER_GROUP';
export const SET_FILTER_WORK_TYPE = 'SET_FILTER_WORK_TYPE';

export const CLEAR_FILTER = 'CLEAR_FILTER';

export const filterByText = (text) => {
  return (dispatch) => {
    dispatch({ type: SET_FILTER_TEXT, payload: text });
  };
};

export const filterByGroup = (groupId) => {
  return (dispatch) => {
    dispatch({ type: SET_FILTER_GROUP, payload: groupId });
  };
};

export const filterByWorkType = (workType) => {
  return (dispatch) => {
    dispatch({ type: SET_FILTER_WORK_TYPE, payload: workType });
  };
};

export const clearFilters = () => {
  return (dispatch) => {
    dispatch({ type: CLEAR_FILTER });
  };
};
