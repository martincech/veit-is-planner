import axios from 'axios';
import { getAssignment } from './assignments.actions';

export const UPDATE_WORK_TIME = 'UPDATE_WORK_TIME';
export const DELETE_WORK_TIME = 'DELETE_WORK_TIME';
export const UPDATE_DURATION = 'UPDATE_DURATION';

export const updateWorkTime = (id, from, to) => {
  return (dispatch) => {
    return axios.post('/worktime/update', { Id: id, From: from, To: to }).then((result) => {
      dispatch({ type: UPDATE_WORK_TIME, payload: result.data });
      getAssignment(result.data.AssignmentId)(dispatch);
    });
  };
};

export const deleteWorkTime = (workTime) => {
  return (dispatch) => {
    return axios.post('/worktime/delete', { id: workTime.Id }).then(() => {
      dispatch({ type: DELETE_WORK_TIME, payload: workTime.Id });
      getAssignment(workTime.AssignmentId)(dispatch);
    });
  };
};

export const updateDuration = (assignment) => {
  return (dispatch) => {
    axios.post('/worktime/updateduration', assignment).then((result) => {
      dispatch({ type: UPDATE_DURATION, payload: result.data });
    });
  };
};
