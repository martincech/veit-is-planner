export const EDIT_ASSIGNMENT_MODAL = 'EDIT_ASSIGNMENT_MODAL';
export const EDIT_ATTENDANCE_MODAL = 'EDIT_ATTENDANCE_MODAL';
export const EDIT_WORK_TIME_MODAL = 'EDIT_WORK_TIME_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';

export const editAssignmentModal = () => (dispatch) => {
  dispatch({ type: EDIT_ASSIGNMENT_MODAL });
};

export const editWorkTimeModal = data => (dispatch) => {
  dispatch({ type: EDIT_WORK_TIME_MODAL, payload: data });
};

export const editAttendanceModal = (userId, month, attendance) => (dispatch) => {
  dispatch({ type: EDIT_ATTENDANCE_MODAL, payload: { userId, month, attendance } });
};

export const closeModal = () => (dispatch) => {
  dispatch({ type: CLOSE_MODAL });
};

const defaultState = {
};

export const modalReducer = (state = defaultState, action) => {
  switch (action.type) {
    case EDIT_ASSIGNMENT_MODAL:
    case EDIT_WORK_TIME_MODAL:
    case EDIT_ATTENDANCE_MODAL:
      return Object.assign({}, state, { type: action.type, payload: action.payload });
    case CLOSE_MODAL:
      return Object.assign({}, state, { type: null, payload: null });
    default:
      return state;
  }
};
