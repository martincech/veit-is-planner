import axios from 'axios';

export const FETCH_USERS = 'FETCH_USERS';
export const FETCH_CURRENT_USER = 'FETCH_CURRENT_USER';

export const getAllUsers = () => {
  return (dispatch) => {
    axios.get('/user/get').then((result) => {
      dispatch({
        type: FETCH_USERS,
        payload: {
          users: result.data.Users,
          groups: result.data.Groups,
        },
      });
    });
  };
};

export const getUser = (userId) => {
  return (dispatch) => {
    axios.get('/user/getById', { params: { userId } }).then((result) => {
      dispatch({
        type: FETCH_USERS,
        payload: {
          user: result.data.User,
          group: result.data.Group,
        },
      });
    });
  };
};

export const getCurrentUser = () => {
  return (dispatch) => {
    axios.get('/account/CurrentUser').then((result) => {
      dispatch({
        type: FETCH_CURRENT_USER,
        payload: {
          currentUser: result.data,
          isAdmin: result.data.Role === 'Admin',
        },
      });
    });
  };
};
