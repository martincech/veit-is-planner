export const startOfMonth = (month) => {
  const today = new Date();
  const monthFrom = new Date(today.getFullYear(), today.getMonth(), 1);
  monthFrom.setMonth(monthFrom.getMonth() - month);
  return monthFrom;
};

export const endOfMonth = (month) => {
  const monthTo = startOfMonth(month);
  monthTo.setMonth(monthTo.getMonth() + 1);
  return monthTo;
};

export const getDate = (date) => {
  if (date == null) return null;
  return (date instanceof Date) ? date : new Date(Date.parse(date));
};

export const diffDate = (from, to) => {
  if (from == null || to == null) return null;
  const fromDate = getDate(from);
  const toDate = getDate(to);
  return toDate - fromDate;
};
