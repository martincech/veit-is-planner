const getDuration = (duration) => {
  if (duration == null || Number.isNaN(duration)) return null;
  const minutes = Math.round(Math.abs(duration) / (1000 * 60));
  return { hours: Math.floor(minutes / 60), minutes: minutes % 60, duration };
};

const getTime = ({ Duration, ExpectedDuration, StartRun }) => {
  let totalTime = Duration;
  if (StartRun != null) {
    const diff = Date.now() - Date.parse(StartRun);
    totalTime += diff;
  }

  const result = { total: getDuration(totalTime) };
  if (ExpectedDuration == null || ExpectedDuration === 0 || Number.isNaN(ExpectedDuration)) return result;

  result.expected = getDuration(ExpectedDuration);
  result.diff = getDuration(totalTime - ExpectedDuration);
  result.percent = Math.round(totalTime / ExpectedDuration * 100);
  return result;
};

export const getColorByPercent = (percent, correction = 0) => {
  return percent > 110 + correction ? (percent > 125 + correction ? 'salmon' : 'darkorange') : 'darkseagreen';
};

export const getColor = ({ Duration, ExpectedDuration, StartRun }) => {
  if (ExpectedDuration == null || ExpectedDuration === 0) return 'initial';
  let totalTime = Duration;
  if (StartRun != null) {
    const diff = Date.now() - Date.parse(StartRun);
    totalTime += diff;
  }
  if (totalTime === 0) return 'initial';
  const percent = Math.round(totalTime / ExpectedDuration * 100);
  return getColorByPercent(percent);
};

export const filterClosedBetween = (assignment, from, to) => {
  if (assignment == null || from == null || to == null || assignment.Stop == null) return false;
  const stop = new Date(assignment.Stop);
  return stop > from && stop < to;
};

export default {
  getTime,
};
