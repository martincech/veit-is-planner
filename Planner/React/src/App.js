import React from 'react';
import componentQueries from 'react-component-queries';
import NotificationSystem from 'react-notification-system';
import { Route, Switch, Redirect } from 'react-router-dom';

import { NOTIFICATION_SYSTEM_STYLE } from './utils/constants';
// layouts
import { Header, Sidebar, Content } from './components/Layout';
import Loader from './components/Loader';

import Users from './pages/users';
import User from './pages/user';

import Task from './pages/task';

import Projects from './pages/projects/Projects';
import Project from './pages/projects/Project';
import ModalContainer from './ModalContainer';

import './styles/reduction.css';

class App extends React.Component {
  static isSidebarOpen() {
    return document
      .querySelector('.cr-sidebar')
      .classList.contains('cr-sidebar--open');
  }

  componentDidMount() {
    this.checkBreakpoint(this.props.breakpoint);
  }

  componentWillReceiveProps({ breakpoint }) {
    if (breakpoint !== this.props.breakpoint) {
      this.checkBreakpoint(breakpoint);
    }
  }

  // close sidebar when
  handleContentClick = () => {
    // close sidebar if sidebar is open and screen size is less than `md`
    if (
      App.isSidebarOpen()
      && (this.props.breakpoint === 'xs'
        || this.props.breakpoint === 'sm'
        || this.props.breakpoint === 'md')
    ) {
      this.openSidebar('close');
    }
  };

  openSidebar = (openOrClose) => {
    if (openOrClose === 'open') {
      document
        .querySelector('.cr-sidebar')
        .classList.add('cr-sidebar--open');
    }

    document.querySelector('.cr-sidebar').classList.remove('cr-sidebar--open');
  }

  checkBreakpoint(breakpoint) {
    switch (breakpoint) {
      case 'xs':
      case 'sm':
      case 'md':
        return this.openSidebar('close');

      case 'lg':
      case 'xl':
      default:
        return this.openSidebar('open');
    }
  }

  render() {
    return (
      <main className="cr-app bg-light">
        <Sidebar />
        <Content fluid onClick={this.handleContentClick}>
          <Header />
          <Loader style={{ marginTop: '10px' }}>
            <Switch>
              <Route exact path="/" component={Users} />
              <Route path="/users" component={Users} />
              <Route path="/user/:id" component={User} />
              <Route path="/task/:id" component={Task} />
              <Route path="/projects" component={Projects} />
              <Route path="/project/:id" component={Project} />
              <Redirect to="/users" />
            </Switch>
          </Loader>
        </Content>
        <ModalContainer />
        <NotificationSystem
          dismissible={false}
          ref={(notificationSystem) => { this.notificationSystem = notificationSystem; }}
          style={NOTIFICATION_SYSTEM_STYLE}
        />
      </main>
    );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (width > 576 && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (width > 768 && width < 991) {
    return { breakpoint: 'md' };
  }

  if (width > 992 && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);
