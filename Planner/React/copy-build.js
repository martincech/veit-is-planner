const path = require('path');
const fs = require('fs-extra');

const source = path.resolve(__dirname, 'build');
const destination = path.resolve(__dirname, '../Web/Content/dist');

fs.emptyDir(destination).then(() => {
  fs.copy(source, destination)
    .then(() => console.log('Copy completed!'))
    .catch((err) => {
      console.log('An error occured while deleting dist folder.');
      return console.error(err);
    });
}).catch((err) => {
  console.log('An error occured while copying the folder.');
  return console.error(err);
});
